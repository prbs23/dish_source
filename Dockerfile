FROM nginx:alpine

WORKDIR /usr/src/dish_source

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV DEBIAN_FRONTEND noninteractive

# Install python, node, and the build utils required to install pip packages
RUN apk add --update build-base linux-headers python3-dev py3-pip nodejs-current npm

# Install dependencies
COPY ./requirements.txt /usr/src/dish_source
RUN pip3 install -r requirements.txt --ignore-installed six
RUN npm install -g yuglify postcss postcss-cli autoprefixer

# Copy source files into image
COPY --chown=101:101 . /usr/src/dish_source

# Build static files
RUN DISH_SOURCE_DEBUG=true python3 manage.py collectstatic --noinput

# Configure nginx
RUN rm /etc/nginx/conf.d/default.conf
RUN ln -s /usr/src/dish_source/nginx/dish_source.conf /etc/nginx/conf.d/dish_source.conf

# Change all directories to nginx user
RUN chown -R 101:101 .

EXPOSE 80

CMD ["nginx/run_server.sh"]

