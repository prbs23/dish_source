[![pipeline status](https://gitlab.com/prbs23/dish_source/badges/master/pipeline.svg)](https://gitlab.com/prbs23/dish_source/-/commits/master)  [![coverage report](https://gitlab.com/prbs23/dish_source/badges/master/coverage.svg)](https://gitlab.com/prbs23/dish_source/-/commits/master)

# Dish Source

This is Dish Source, a django-app for implementing a self hosted recipe manager site.
