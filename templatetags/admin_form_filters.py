"""
Template tags for Django auth app templates
"""

from django import template
from django.utils.html import format_html, format_html_join

register = template.Library()


@register.filter(name='add_field_class')
def add_field_class(value, class_name):
    """Template tag to add class to form field"""
    return value.as_widget(attrs={'class': class_name})


@register.filter(name='label_with_class')
def label_with_class(value, class_name):
    """Template tag to get a field label with custom fields"""
    return value.label_tag(attrs={'class': class_name})


@register.filter(name='format_field_errors')
def format_field_errors(value, class_name):
    """Template tag to format list error messages for a field"""
    return format_html(
        '<ul class="{} {}" style="list-style: none; padding-left: 1rem">{}</ul>',
        value.error_class, class_name,
        format_html_join('', '<li>{}</li>', ((e,) for e in value))
    )
