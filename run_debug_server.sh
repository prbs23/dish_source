#!/bin/sh

source ./debug_server.env

python3 manage.py livereload &
LIVERELOAD_PID=$!
python3 manage.py runserver $DISH_SOURCE_HOST_NAME:8000
kill $LIVERELOAD_PID
wait $LIVERELOAD_PID
