# Generated by Django 3.0.2 on 2020-07-04 19:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dish_source', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='recipeingredient',
            options={'ordering': ['order'], 'verbose_name': 'Recipe Ingredient', 'verbose_name_plural': 'Recipe Ingredients'},
        ),
        migrations.AlterModelOptions(
            name='recipeingredientquantity',
            options={'ordering': ['order'], 'verbose_name': 'Recipe Ingredient Quantity', 'verbose_name_plural': 'Recipe Ingredient Quantities'},
        ),
        migrations.AlterModelOptions(
            name='recipeingredientsection',
            options={'ordering': ['order'], 'verbose_name': 'Recipe Ingredient Section', 'verbose_name_plural': 'Recipe Ingredient Sections'},
        ),
        migrations.AlterModelOptions(
            name='recipeinstruction',
            options={'ordering': ['order'], 'verbose_name': 'Recipe Instruction', 'verbose_name_plural': 'Recipe Instructions'},
        ),
        migrations.AlterModelOptions(
            name='recipeinstructionsection',
            options={'ordering': ['order'], 'verbose_name': 'Recipe Instruction Section', 'verbose_name_plural': 'Recipe Instruction Sections'},
        ),
        migrations.AlterModelOptions(
            name='recipevariation',
            options={'ordering': ['order'], 'verbose_name': 'Recipe Variation', 'verbose_name_plural': 'Recipe Variations'},
        ),
        migrations.AlterModelOptions(
            name='recipeyield',
            options={'ordering': ['order'], 'verbose_name': 'Recipe Yield', 'verbose_name_plural': 'Recipe Yields'},
        ),
    ]
