"""
Models for Dish Source app

See dish_source.mwb for full database diagram.
"""

from django.contrib.auth import get_user_model
from django.contrib.auth.models import AnonymousUser
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
import inflect

from dish_source.utils import format_decimal, get_slug

User = get_user_model()

_QUANTITY_VALUE_MAX_DIGITS = 8
_QUANTITY_VALUE_DECIMAL_PLACES = 4


# =========================================================
# User and configuration tables
# =========================================================
class UserConfig(models.Model):
    """
    Model to configurable user settings
    """
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    global_default = models.BooleanField(default=False)
    default_yield_unit = models.ForeignKey("Unit", on_delete=models.PROTECT)

    def __str__(self):
        return f"User Settings: default_yield_unit='{self.default_yield_unit.name}'"

    @classmethod
    def get_config_dict(cls, user):
        """
        Get dictionary of user configuration settings. Defaulting to global_default config if user
        has no UserConfig object or user is not logged in.
        :param user: User model object or None if no user is logged in.
        :return: Dictionary of configuration options.
        """
        try:
            if isinstance(user, AnonymousUser):
                raise cls.DoesNotExist
            config_obj = cls.objects.get(user=user)
        except cls.DoesNotExist:
            try:
                config_obj = cls.objects.get(global_default=True)
            except cls.DoesNotExist:
                return {"default_yield_unit": None}
        return {"default_yield_unit": config_obj.default_yield_unit.id}

    class Meta:
        constraints = [
            models.UniqueConstraint(name="global_default_unique_true", fields=["global_default"],
                                    condition=Q(global_default=True))
        ]


# =========================================================
# Source tables
# =========================================================
class Source(models.Model):
    """
    Model to store recipe sources
    Could be a website, cookbook, or something else.
    """
    name = models.CharField(verbose_name="Name", max_length=255, blank=True)
    authors = models.ManyToManyField("SourceAuthor", related_name="sources",
                                     verbose_name="Authors", blank=True)
    isbn = models.CharField(verbose_name="ISBN", max_length=15, blank=True)
    url = models.URLField(verbose_name="URL", max_length=2048, blank=True)
    notes = models.TextField(verbose_name="Notes", blank=True)

    def __str__(self):
        result = ""
        if self.name:
            result += str(self.name)
            if self.authors.all():
                result += " (" + ", ".join([str(a) for a in self.authors.all()]) + ")"
        if self.url:
            if str(self.name):
                result += " - "
            result += str(self.url)
            if self.authors.all() and not self.name:
                result += " (" + ", ".join([str(a) for a in self.authors.all()]) + ")"
        return result

    class Meta:
        verbose_name = "Source"
        verbose_name_plural = "Sources"
        ordering = ['name', 'url']
        constraints = [
            models.CheckConstraint(check=~(Q(name__exact='') & Q(url__exact='')),
                                   name="name_and_url_not_empty")
        ]


class SourceAuthor(models.Model):
    """
    Recipe author model
    Many to Many relationship with recipe source model.
    """
    name = models.CharField(verbose_name="Name", max_length=255, blank=False)
    slug = models.SlugField(unique=True)

    @property
    def recipes(self):
        """Returns a list of all recipes from this author"""
        return list(Recipe.objects.filter(source__authors=self).all())

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Source Author"
        verbose_name_plural = "Source Authors"
        ordering = ['name']

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Auto-generate slug field if it's not provided
        if not self.slug:
            self.slug = get_slug(self.name, self.__class__)
        super().save(force_insert, force_update, using, update_fields)


class UnimportedRecipe(models.Model):
    """
    Unimported recipe model
    Used to allow quick noting of recipes without formatting into true recipe entries.
    """
    source = models.ForeignKey("Source", related_name="unimported_recipes", verbose_name="Source",
                               on_delete=models.PROTECT)
    text = models.TextField(verbose_name="Text", blank=True)

    def __str__(self):
        return str(self.source)

    class Meta:
        verbose_name = "Unimported Recipe"
        verbose_name_plural = "Unimported Recipes"
        ordering = ['source']


# =========================================================
# Unit tables
# =========================================================
class Unit(models.Model):
    """
    Unit model
    Represents the unit for a value.
    """
    name = models.CharField(verbose_name="Name", max_length=127, blank=True)
    plural = models.CharField(verbose_name="Plural", max_length=127, blank=True)
    abbrev = models.CharField(verbose_name="Abbreviation", max_length=32, blank=True, null=True)

    def __str__(self):
        return str(self.abbrev) + "." if self.abbrev else str(self.name)

    class Meta:
        verbose_name = "Unit"
        verbose_name_plural = "Units"
        ordering = ['name', 'abbrev']

    # Declare class inflection engine instance
    inflect_engine = inflect.engine()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        """
        Overrides default save behavior of unit names to normalize them. The following rules are
        applied.
        - Name and Plural must be lower case
        - Name or Plural field must be non-empty/missing
        - If the name or plural are missing, compute the missing from the other
        - Abbreviation must be lower case
        - Abbreviation must not end in a period
        """
        if not self.name and not self.plural:
            raise ValidationError("Either the Name or Plural field are required")
        if not self.name:
            singular = self.inflect_engine.singular_noun(self.plural)
            self.name = (singular if singular else self.plural).lower()
        else:
            self.name = self.name.lower()

        if not self.plural:
            plural = self.inflect_engine.plural_noun(self.name)
            self.plural = (plural if plural else self.name).lower()
        else:
            self.plural = self.plural.lower()

        if self.abbrev:
            self.abbrev = self.abbrev.lower().strip(".")
        super().save(force_insert, force_update, using, update_fields)


# =========================================================
# Ingredient Tables
# =========================================================
class Ingredient(models.Model):
    """
    Ingredient model
    Represents an ingredient that is used in multiple different recipes.
    """
    name = models.CharField(verbose_name="Name", max_length=255, blank=True)
    plural = models.CharField(verbose_name="Plural", max_length=255, blank=True)
    slug = models.SlugField(unique=True)
    description = models.TextField(verbose_name="Description", blank=True)
    usda_number = models.IntegerField(verbose_name="USDA Number", blank=True, null=True)
    tags = models.ManyToManyField("IngredientTag", related_name="ingredients", verbose_name="Tags",
                                  blank=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Ingredient"
        verbose_name_plural = "Ingredients"
        ordering = ['name']

    # Declare class inflection engine instance
    inflect_engine = inflect.engine()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        """
        Overrides default save behavior of ingredient object to normalize them. The following rules
        are applied.
        - Name or Plural field must be non-empty/missing
        - If the name or plural are missing, compute the missing from the other
        - Auto-generate a slug if it's not provided
        """
        if not self.name and not self.plural:
            raise ValidationError("Either the Name or Plural field are required")
        if not self.name:
            singular = self.inflect_engine.singular_noun(self.plural)
            self.name = singular if singular else self.plural
        if not self.plural:
            plural = self.inflect_engine.plural_noun(self.name)
            self.plural = plural if plural else self.name

        # Auto-generate slug field if it's not provided
        if not self.slug:
            self.slug = get_slug(self.name, self.__class__)
        super().save(force_insert, force_update, using, update_fields)


class IngredientTag(models.Model):
    """
    Ingredient tag model
    A tag that can be applied to ingredients. To be used to categorize ingredients.
    """
    name = models.CharField(verbose_name="Name", max_length=255)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Ingredient Tag"
        verbose_name_plural = "Ingredient Tags"
        ordering = ['name']

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Auto-generate slug field if it's not provided
        if not self.slug:
            self.slug = get_slug(self.name, self.__class__)
        super().save(force_insert, force_update, using, update_fields)


class IngredientCalories(models.Model):
    """
    Ingredient calorizes model
    Store the number of calorize for a given quantity of an ingredient.
    """
    ingredient = models.ForeignKey("Ingredient", related_name="calories", on_delete=models.CASCADE)
    unit = models.ForeignKey("Unit", related_name="+", on_delete=models.PROTECT)
    calories = models.FloatField()

    def __str__(self):
        return str(self.calories) + "/" + str(self.unit) + " " + str(self.ingredient)

    class Meta:
        verbose_name = "Ingredient Calories"
        verbose_name_plural = "Ingredient Calories"


# =========================================================
# Recipe tables
# =========================================================
class Category(models.Model):
    """
    Category model
    Top level categorization for recipes
    """
    name = models.CharField(verbose_name="Name", max_length=255)
    slug = models.SlugField(unique=True)
    description = models.TextField(verbose_name="Description", blank=True)

    def __str__(self):
        return str(self.name)

    @property
    def unsubcategorized_recipes(self):
        """Get all recipes mapped to this category that don't have a subcategory"""
        return self.recipes.all().filter(subcategory__isnull=True)

    def get_all_recipes(self, include_empty_subcategories=False):
        """Get an ordered list of unsubcategorized recipe and subcategory objects sorted by name"""
        return sorted(list(self.unsubcategorized_recipes) +
                      list(self.subcategories.all().annotate(
                          recipe_count=models.Count('recipes')).filter(
                              recipe_count__gte=1 if not include_empty_subcategories else 0)),
                      key=lambda item: item.name)

    @property
    def all_recipes(self):
        """Helper property to call get_all_recipes with include_empty_subcategories = False"""
        return self.get_all_recipes(False)

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"
        ordering = ['name']

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Auto-generate slug field if it's not provided
        if not self.slug:
            self.slug = get_slug(self.name, self.__class__)
        super().save(force_insert, force_update, using, update_fields)


class Subcategory(models.Model):
    """
    Subcategory model
    Second level categorization of recipes inside a category.
    """
    category = models.ForeignKey("Category", related_name="subcategories", verbose_name="Category",
                                 on_delete=models.CASCADE)
    name = models.CharField(verbose_name="Name", max_length=255)
    slug = models.SlugField(unique=False)
    description = models.TextField(verbose_name="Description", blank=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Subcategory"
        verbose_name_plural = "Subcategories"
        ordering = ['name']
        constraints = [
            models.UniqueConstraint(fields=['category', 'slug'], name='unique_slug')
        ]

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Auto-generate slug field if it's not provided
        if not self.slug:
            self.slug = get_slug(self.name, self.__class__)
        super().save(force_insert, force_update, using, update_fields)


class RecipeTag(models.Model):
    """
    Recipe tag model
    Used for arbitrary categorization of recipes.
    """
    name = models.CharField(verbose_name="Name", max_length=255)
    slug = models.SlugField(unique=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Recipe Tag"
        verbose_name_plural = "Recipe Tags"
        ordering = ['name']

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Auto-generate slug field if it's not provided
        if not self.slug:
            self.slug = get_slug(self.name, self.__class__)
        super().save(force_insert, force_update, using, update_fields)


class RecipeYield(models.Model):
    """
    Recipe yield model
    Single yield quantity for a recipe. There could be multiple yield specifiers per recipe.
    """
    recipe = models.ForeignKey("Recipe", related_name="yields", verbose_name="Recipe",
                               on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField(verbose_name="Order")
    approx = models.BooleanField(verbose_name="Approximate Yield")
    min_quantity = models.DecimalField(verbose_name="(Minimum) Quantity",
                                       max_digits=_QUANTITY_VALUE_MAX_DIGITS,
                                       decimal_places=_QUANTITY_VALUE_DECIMAL_PLACES)
    max_quantity = models.DecimalField(verbose_name="Maximum Quantity",
                                       max_digits=_QUANTITY_VALUE_MAX_DIGITS,
                                       decimal_places=_QUANTITY_VALUE_DECIMAL_PLACES,
                                       blank=True, null=True)
    unit = models.ForeignKey("Unit", related_name="+", verbose_name="Unit",
                             on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        result = ""
        if self.approx:
            result += "~"
        result += format_decimal(self.min_quantity)
        if self.max_quantity:
            result += "-" + format_decimal(self.max_quantity)
        result += " " + str(self.unit)
        return result.lower() + ("s" if self.min_quantity > 1 else "")

    class Meta:
        verbose_name = "Recipe Yield"
        verbose_name_plural = "Recipe Yields"
        ordering = ['order']
        constraints = [
            models.UniqueConstraint(fields=["order", "recipe"], name="unique_yield_order")
        ]


class RecipeIngredientSection(models.Model):
    """
    Recipe ingredient section recipe
    Section of recipe ingredients.
    """
    recipe = models.ForeignKey("recipe", related_name="ingredient_sections", verbose_name="Recipe",
                               on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField(verbose_name="Order")
    name = models.CharField(verbose_name="Section Name", max_length=255, blank=True, null=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Recipe Ingredient Section"
        verbose_name_plural = "Recipe Ingredient Sections"
        ordering = ['order']
        constraints = [
            models.UniqueConstraint(fields=["name", "recipe"],
                                    name="unique_ingredient_section_name"),
            models.UniqueConstraint(fields=["order", "recipe"],
                                    name="unique_ingredient_section_order"),
            models.CheckConstraint(check=(Q(name__isnull=False) | Q(order=0)),
                                   name="null_ingredient_section_first")
        ]


class RecipeIngredientSubstitution(models.Model):
    """
    Recipe ingredient substitution model
    Points to a recipe ingredient entry, pointed to by recipe ingredients that substitute the
    parent ingredient.
    """
    substituted_ingredient = models.ForeignKey("RecipeIngredient", related_name="substitutions",
                                               verbose_name="Recipe Ingredient",
                                               on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField(verbose_name="Order")

    def __str__(self):
        return ("Substitute " + str(self.substituted_ingredient) + " with " +
                ", ".join([str(i) for i in self.ingredients.all()]))

    class Meta:
        verbose_name = "Recipe Ingredient Substitution"
        verbose_name_plural = "Recipe Ingredient Substitutions"
        ordering = ['order']
        constraints = [
            models.UniqueConstraint(fields=["order", "substituted_ingredient"],
                                    name="unique_substitution_order")
        ]


class RecipeIngredient(models.Model):
    """
    Recipe ingredient model
    Represents an ingredient of a recipe. Includes the ingredient, quantity, and other ingredient
    notes.
    """
    section = models.ForeignKey("RecipeIngredientSection", related_name="ingredients",
                                verbose_name="Ingredient Section", on_delete=models.CASCADE,
                                blank=True, null=True)
    substitution = models.ForeignKey("RecipeIngredientSubstitution", related_name="ingredients",
                                     verbose_name="Substitution", on_delete=models.CASCADE,
                                     blank=True, null=True)
    order = models.PositiveSmallIntegerField(verbose_name="Order")
    qualifier = models.CharField(verbose_name="Ingredient Qualifier", max_length=512, blank=True)
    ingredient = models.ForeignKey("Ingredient", related_name="recipe_ingredients",
                                   on_delete=models.PROTECT)
    processing = models.CharField(verbose_name="Ingredient Processing Notes", max_length=512,
                                  blank=True)

    def __str__(self):
        result = ""
        if self.quantities.all():
            result += str(self.quantities.all()[0])
            if len(self.quantities.all()) > 1:
                result += "(" + ", ".join([str(q) for q in self.quantities.all()[1:]]) + ")"
        if self.qualifier:
            result += " " + str(self.qualifier)
        result += " " + str(self.ingredient).lower()
        if self.processing:
            result += ", " + str(self.processing)
        return result

    class Meta:
        verbose_name = "Recipe Ingredient"
        verbose_name_plural = "Recipe Ingredients"
        ordering = ['order']
        constraints = [
            models.UniqueConstraint(fields=["order", "section", "substitution"],
                                    name="unique_ingredient_order"),
            models.CheckConstraint(check=((Q(section__isnull=True) &
                                           Q(substitution__isnull=False)) |
                                          (Q(section__isnull=False) &
                                           Q(substitution__isnull=True))),
                                   name="ingredient_as_section_xor_substitution")
        ]


class RecipeIngredientQuantity(models.Model):
    """
    Recipe ingredient quantity model
    Represents a quantity or quantity range of a given recipe ingredient.
    """
    recipe_ingredient = models.ForeignKey("RecipeIngredient", related_name="quantities",
                                          verbose_name="Recipe Ingredient",
                                          on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField(verbose_name="Order")
    approx = models.BooleanField(verbose_name="Approximate Quantity")
    min_quantity = models.DecimalField(verbose_name="(Minimum) Quantity",
                                       max_digits=_QUANTITY_VALUE_MAX_DIGITS,
                                       decimal_places=_QUANTITY_VALUE_DECIMAL_PLACES)
    max_quantity = models.DecimalField(verbose_name="Maximum Quantity",
                                       max_digits=_QUANTITY_VALUE_MAX_DIGITS,
                                       decimal_places=_QUANTITY_VALUE_DECIMAL_PLACES,
                                       blank=True, null=True)
    unit = models.ForeignKey("Unit", related_name="+", verbose_name="Unit",
                             on_delete=models.PROTECT, blank=True, null=True)

    def __str__(self):
        result = ""
        if self.approx:
            result += "~"
        result += format_decimal(self.min_quantity)
        if self.max_quantity:
            result += "-" + format_decimal(self.max_quantity)
        if self.unit:
            result += " " + str(self.unit)
        return result.lower() + ("s" if self.min_quantity > 1 and self.unit else "")

    class Meta:
        verbose_name = "Recipe Ingredient Quantity"
        verbose_name_plural = "Recipe Ingredient Quantities"
        ordering = ['order']
        constraints = [
            models.UniqueConstraint(fields=["order", "recipe_ingredient"],
                                    name="unique_quantity_order")
        ]


class RecipeInstructionSection(models.Model):
    """
    Recipe instruction section model
    """
    recipe = models.ForeignKey("recipe", related_name="instruction_sections", verbose_name="Recipe",
                               on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField(verbose_name="Order")
    name = models.CharField(verbose_name="Section Name", max_length=255, blank=True, null=True)

    def __str__(self):
        return str(self.name)

    def numbering_start(self):
        """
        Get the starting number of the instructions in this section

        All instructions should have consecutive incrementing numbers across sections. This function
        returns the number that would follow the instruction from the previous section.
        """
        return sum([len(section.instructions.all()) for section in
                    self.recipe.instruction_sections.filter(order__lt=self.order).all()]) + 1

    class Meta:
        verbose_name = "Recipe Instruction Section"
        verbose_name_plural = "Recipe Instruction Sections"
        ordering = ['order']
        constraints = [
            models.UniqueConstraint(fields=["name", "recipe"],
                                    name="unique_instruction_section_name"),
            models.UniqueConstraint(fields=["order", "recipe"],
                                    name="unique_instruction_section_order"),
            models.CheckConstraint(check=Q(name__isnull=False) | Q(order=0),
                                   name="null_instruction_section_first")
        ]


class RecipeInstruction(models.Model):
    """
    Recipe instruction model
    Single instruction of a recipe, html formatted.
    """
    section = models.ForeignKey("RecipeInstructionSection", related_name="instructions",
                                verbose_name="Section", on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField(verbose_name="Order")
    instruction = models.TextField(verbose_name="Instruction", blank=True)

    def __str__(self):
        result = str(self.instruction)
        if len(result) > 100:
            return result[0:97] + "..."
        return result

    class Meta:
        verbose_name = "Recipe Instruction"
        verbose_name_plural = "Recipe Instructions"
        ordering = ['order']
        constraints = [
            models.UniqueConstraint(fields=["order", "section"], name="unique_instruction_order")
        ]


class RecipeVariation(models.Model):
    """
    Recipe variation model
    Variation description of a recipe, html formatted.
    """
    recipe = models.ForeignKey("recipe", related_name="variations", verbose_name="Recipe",
                               on_delete=models.CASCADE)
    order = models.PositiveSmallIntegerField(verbose_name="Order")
    name = models.CharField(verbose_name="Variation Name", max_length=255)
    variation = models.TextField(verbose_name="Variation", blank=True)

    def __str__(self):
        return str(self.name)

    class Meta:
        verbose_name = "Recipe Variation"
        verbose_name_plural = "Recipe Variations"
        ordering = ['order']
        constraints = [
            models.UniqueConstraint(fields=["order", "recipe"], name="unique_variation_order")
        ]


class Recipe(models.Model):
    """
    Recipe model
    Represents an entire recipe
    """
    category = models.ForeignKey("Category", related_name="recipes", verbose_name="Category",
                                 on_delete=models.PROTECT)
    subcategory = models.ForeignKey("Subcategory", related_name="recipes",
                                    verbose_name="Subcategory", on_delete=models.PROTECT,
                                    blank=True, null=True)
    source = models.ForeignKey("Source", related_name="recipes", verbose_name="Recipe Source",
                               on_delete=models.PROTECT, blank=True, null=True)
    name = models.CharField(verbose_name="Recipe Name", max_length=255)
    slug = models.SlugField(unique=True)
    prep_duration_min = models.IntegerField(verbose_name="(Minimum) Preparation Duration",
                                            blank=True, null=True)
    prep_duration_max = models.IntegerField(verbose_name="Maximum Preparation Duration",
                                            blank=True, null=True)
    cook_duration_min = models.IntegerField(verbose_name="(Minimum) Cooking Duration",
                                            blank=True, null=True)
    cook_duration_max = models.IntegerField(verbose_name="Maximum Cooking Duration",
                                            blank=True, null=True)
    description = models.TextField(verbose_name="Description", blank=True)
    image = models.FileField(upload_to="recipe_images", blank=True)
    notes = models.TextField(verbose_name="Notes", blank=True)
    tags = models.ManyToManyField("RecipeTag", related_name="recipes", verbose_name="Tags",
                                  blank=True)

    def __str__(self):
        result = str(self.name) + " (" + str(self.category)
        if self.subcategory:
            result += ", " + str(self.subcategory)
        result += ")"
        return result

    class Meta:
        verbose_name = "Recipe"
        verbose_name_plural = "Recipes"
        ordering = ['name']

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        # Auto-generate slug field if it's not provided
        if not self.slug:
            self.slug = get_slug(self.name, self.__class__)
        super().save(force_insert, force_update, using, update_fields)
