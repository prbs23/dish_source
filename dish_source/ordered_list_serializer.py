"""
Extend NestedModelSerializer types to implement a model that includes an ordered list of
child items. This assumes that the database models the child items as a separate model type which
as a foreign key relationship with the parent class, and a integer order column.
"""

from rest_framework.utils.model_meta import get_field_info
from dish_source.nested_model_serializer import NestedListSerializer, NestedModelSerializer


class OrderedListSerializer(NestedListSerializer):
    """Extends NestedListSerializer to create/update a list of ordered items under a parent item"""
    deferred_create = True

    def __init__(self, *args, **kwargs):
        assert isinstance(kwargs["child"], OrderedListItemSerializer), (
            "Child of OrderedListSerializer must inherit from OrderedListItemSerializer."
        )
        super().__init__(self, *args, **kwargs)

    def create_or_update_child(self, method_name, parent_serializer, validated_data,
                               parent_instance=None, **kwargs):
        assert isinstance(validated_data, list), (
            "validated_data argument to create_or_update_child method of NestedListSerializer "
            "must be a list."
        )

        assert (parent_instance is not None), (
            "create_or_update_child() method of OrderedListSerializer should never be called "
            "without a parent_instance because deferred_create should be True."
        )

        # Get the model class for our child item
        # pylint: disable=invalid-name
        ModelClass = self.child.Meta.model

        # Find all the child items of the parent_instance and delete them
        # pylint: disable=protected-access
        query_args = {self.child.order_parent_field: parent_instance}
        existing_list_items = ModelClass._default_manager.filter(**query_args)
        existing_list_items.delete()

        # Iterate through the validated_data list creating each item
        child_items = []
        for order_position, validated_item in enumerate(validated_data):
            child_items.append(self.child.create_or_update_child(
                method_name, parent_serializer, validated_item, parent_instance,
                order_position=order_position))

        return child_items

    def update(self, instance, validated_data):
        """The update function should never be called, so provide a no-op implementation"""


class OrderedListItemSerializer(NestedModelSerializer):
    """
    Extend NestedModelSerializer for modeling a ordered list child item.
    A class that inherits from this class must set the name of the model field used to store the
    order index. Additionally the constructor must be passed the order_parent_field to indicate
    the name for foreign key field that relates to the parent model.
    """
    order_field = "order"
    deferred_create = True

    def __new__(cls, *args, **kwargs):
        """
        Override the default list_serializer_class field of the Meta class to instance a
        OrderedListSerializer instead of the default NestedListSerializer.
        """
        meta = getattr(cls, 'Meta', None)
        setattr(meta, 'list_serializer_class',
                getattr(meta, 'list_serializer_class', OrderedListSerializer))
        return super().__new__(cls, *args, **kwargs)

    def __init__(self, *args, **kwargs):
        self.order_parent_field = kwargs.pop('order_parent_field', None)

        # Add checks for the OrderedListItem class variables
        assert (self.order_field is not None), (
            "order_field must not be None"
        )

        assert (self.order_parent_field is not None), (
            "order_parent_field must not be None"
        )

        # Check that the the order and order parent fields are valid
        field_info = get_field_info(getattr(self, "Meta", None).model)
        assert (self.order_field in field_info.fields), (
            f"order_field '{self.order_field}' for '{type(self)}' serializer is not a valid field "
            f"for the model.\nValid fields for the model are {list(field_info.fields.keys())}"
        )
        assert (self.order_parent_field in field_info.forward_relations), (
            f"order_field '{self.order_parent_field}' for '{type(self)}' serializer is not a valid "
            f"field for the model.\nValid fields for the model are "
            f"{list(field_info.forward_relations.keys())}"
        )

        super().__init__(*args, **kwargs)

    def bind(self, field_name, parent):
        assert isinstance(parent, OrderedListSerializer), (
            f"OrderedListItemSerializer may only be bound as a child serializer under a "
            f"OrderedListSerializer object\ntype(parent): {type(parent)}\nparent: {parent}\n"
            f"type(self): {type(self)}\nself: {self}\nfield_name: {field_name}"
        )
        super().bind(field_name, parent)

    def save(self, **kwargs):
        order_position = kwargs.pop('order_position', None)
        parent_instance = kwargs.pop('parent_instance', None)

        assert (order_position is not None or self.instance is not None), (
            "save() method of OrderedListItemSerializer must be called with an instance or a valid "
            "order position."
        )

        assert (parent_instance is not None or self.instance is not None), (
            "save() method of OrderedListItemSerializer must be called with an instance or a "
            "parent instance."
        )

        if order_position is not None:
            kwargs[self.order_field] = order_position

        if parent_instance is not None:
            kwargs[self.order_parent_field] = parent_instance

        return super().save(**kwargs)

    def create_or_update_child(self, method_name, parent_serializer, validated_data,
                               parent_instance=None, **kwargs):
        order_position = kwargs.pop('order_position', None)

        assert (parent_instance is not None), (
            "create_or_update_child() method of OrderedListItemSerializer should never be called "
            "without a parent_instance value, because deferred_create is True."
        )

        assert (order_position is not None), (
            "create_or_update_child() method of OrderedListItemSerializer must be called with a "
            "valid order_position argument."
        )

        validated_data[self.order_field] = order_position
        validated_data[self.order_parent_field] = parent_instance
        return self.create(validated_data)
