const {EditorState, TextSelection} = require("prosemirror-state")
const {EditorView, Decoration, DecorationSet} = require("prosemirror-view")
const {DOMParser, Slice, Fragment} = require("prosemirror-model")
const {baseKeymap, chainCommands, deleteSelection, joinBackward, toggleMark, setBlockType, wrapIn, lift} = require("prosemirror-commands")
const {insertPoint} = require("prosemirror-transform")
const {keymap} = require("prosemirror-keymap")
const {history, undo, redo} = require("prosemirror-history")
const {wrapInList, splitListItem, liftListItem, sinkListItem} = require("prosemirror-schema-list")

function unitItemMatchesString(item, str) {
    return ((item.name && item.name.localeCompare(str.trim(), undefined, {sensitivity: 'accent', ignorePunctuation: true}) == 0) ||
            (item.plural && item.plural.localeCompare(str.trim(), undefined, {sensitivity: 'accent', ignorePunctuation: true}) == 0) ||
            (item.abbrev && (item.abbrev + ".").localeCompare(str.trim(), undefined, {sensitivity: 'accent', ignorePunctuation: true}) == 0) ||
            (item.abbrev && item.abbrev.localeCompare(str.trim(), undefined, {sensitivity: 'accent', ignorePunctuation: true}) == 0));
}

function ingredientItemMatchesString(item, str) {
    return ((item.name && item.name.localeCompare(str.trim(), undefined, {sensitivity: 'accent'}) == 0) ||
            (item.plural && item.plural.localeCompare(str.trim(), undefined, {sensitivity: 'accent'}) == 0));
}

function formatRecipeIngredient(recipe_ingredient, nodeType = "recipeIngredient") {
    nodes = []
    recipe_ingredient.quantities.forEach((quantity, index) => {
        nodes.push(ingredientSchema.text((index == 1 ? " (" : (index > 0 ? ", " : "")) + formatQuantityValue(quantity) + (quantity.unit ? " " : "")));
        if (quantity.unit) {
            nodes.push(ingredientSchema.text(formatQuantityUnit(quantity), ingredientSchema.mark("unitName", {item: quantity.unit})));
        }
    });
    nodes.push(ingredientSchema.text(
        (recipe_ingredient.quantities.length > 1 ? ") " : " ") +
        (recipe_ingredient.qualifier ? (recipe_ingredient.qualifier + " ") : "")));
    nodes.push(ingredientSchema.text(recipe_ingredient.ingredient.name, ingredientSchema.mark("ingredientName", {item: recipe_ingredient.ingredient})));
    if (recipe_ingredient.processing) nodes.push(ingredientSchema.text(", " + recipe_ingredient.processing));
    return ingredientSchema.node(
        nodeType,
        {item: recipe_ingredient},
        nodes
    );
}

function parseRecipeYields(node, base_pos) {
    const quantityValueAndProcessingRegExp = RegExp(/(?:(.*?)(?:((?:~\s*|about\s*|approx(?:\.?|imately)\s*)?)((?:\d+\s*)?(?:[⅛¼⅓⅜½⅝⅔¾⅞]|(?:\d+\s*\/\s*\d+)|(?<!\d+\s+)\.?\d+))(?:\s*-\s*((?:\d+\s*)?(?:[⅛¼⅓⅜½⅝⅔¾⅞]|(?:\d+\s*\/\s*\d+)|(?<!\d+\s+)\.?\d+)))?))|(.+)/gmi);

    let errors = [];
    let warnings = [];
    let last_yield = null;
    yields = []
    let paren_depth = 0;
    node.descendants(function(child_node, pos) {
        if (child_node.marks.length == 0) {
            let matches = child_node.textContent.matchAll(quantityValueAndProcessingRegExp);
            let local_pos = base_pos + pos;
            for (const match of matches) {
                if (match[3]) {
                    if (last_yield != null) {
                        let msg = "Yield value '" + formatQuantityValue(last_yield) + "' has no unit.";
                        let default_unit = units.find(unit => unit.id === user_config.default_yield_unit);
                        if (default_unit != null) {
                            last_yield.unit = default_unit;
                            msg += " Using default unit '" + default_unit.name + "'.";
                        }
                        warnings.push({from: local_pos, to: local_pos + match[0].length, message: msg});
                        yields.push(last_yield);
                    }
                    last_yield = {
                        approx: Boolean(match[2]),
                        min_quantity: encodeDecimal(match[3]),
                        max_quantity: (match[4] != undefined) ? encodeDecimal(match[4]) : null,
                        unit: null
                    };
                }

                // Track parenthesis depth
                for (c of match[0]) {
                    if (c == "(") paren_depth += 1;
                    else if (c == ")" && paren_depth > 0) paren_depth -= 1;
                };
                local_pos += match[0].length;
            }
        } else if (child_node.marks[0].type == ingredientSchema.marks.unitName) {
            // Track parenthesis depth
            for (c of child_node.textContent) {
                if (c == "(") paren_depth += 1;
                else if (c == ")") paren_depth -= 1;
            };
            if (last_yield == null) {
                errors.push({from: base_pos + pos, to: base_pos + pos + child_node.nodeSize, message: "Yield unit without a value."})
            } else if (child_node.marks[0].attrs.item != null) {
                if (child_node.marks[0].attrs.item.id < 0) {
                    warnings.push({from: base_pos + pos, to: base_pos + pos + child_node.nodeSize, message: "New unit '"+child_node.marks[0].attrs.item.name+"' will be created."});
                }
                last_yield.unit = child_node.marks[0].attrs.item;
                yields.push(last_yield);
                last_yield = null;
            }
        }
    });
    if (last_yield != null) {
        if (last_yield.unit == null) {
            let msg = "Yield value '" + formatQuantityValue(last_yield) + "' has no unit.";
            let default_unit = units.find(unit => unit.id === user_config.default_yield_unit);
            if (default_unit != null) {
                last_yield.unit = default_unit;
                msg += " Using default unit '" + default_unit.name + "'.";
            }
            warnings.push({from: base_pos, to: base_pos + node.nodeSize, message: msg});
        }
        yields.push(last_yield);
    }

    if (errors.length > 0) {
        node.attrs.item = null;
    } else {
        node.attrs.item = yields;
    }

    return {
        obj: yields,
        errors: errors,
        warnings: warnings
    };
}

function parseDuration(node) {
    let text = node.textContent;
    if (text.length == 0) return [null, null];
    let durations = text.split("-", 2).map(t => {
        let match = t.match(/^\s*(?:(\d+)\s*(?:|:|hrs?\.?|hours?))??\s*(?:(\d+)\s*(?:|:|min\.?|minutes?))?\s*(?:(?<!^[^:]+:[^:]*)(\d+)\s*(?:|sec\.?|seconds?))??\s*$/i);
        if (match == null) return undefined;
        let duration = 0;
        if (match[1] != undefined) duration += Number(match[1]) * 60 * 60;
        if (match[2] != undefined) duration += Number(match[2]) * 60;
        if (match[3] != undefined) duration += Number(match[3]);
        return duration;
    });
    if (durations.length == 1) {
        if (durations[0] === undefined) return undefined;
        return [durations[0], null];
    } else if (durations.length == 2) {
        if (durations[0] === undefined || durations[0] === null || durations[1] === undefined) return undefined;
        if (durations[1] !== null && durations[0] > durations[1]) return undefined;
        return durations;
    } else {
        return [null, null];
    }
}

function parseRecipeIngredient(node, base_pos = 0) {
    const quantityValueAndProcessingRegExp = RegExp(/(?:(.*?)(?:((?:~\s*|about\s*|approx(?:\.?|imately)\s*)?)((?:\d+\s*)?(?:[⅛¼⅓⅜½⅝⅔¾⅞]|(?:\d+\s*\/\s*\d+)|(?<!\d+\s+)\.?\d+))(?:\s*(?:-|to)\s*((?:\d+\s*)?(?:[⅛¼⅓⅜½⅝⅔¾⅞]|(?:\d+\s*\/\s*\d+)|(?<!\d+\s+)\.?\d+)))?))|(.+)/gmi);
    let errors = [];
    let warnings = [];
    let last_quantity = null;
    let recipe_ingredient = {quantities: [], qualifier: "", ingredient: null, "": null, substitutions: []};
    let paren_depth = 0;
    node.descendants(function(child_node, pos) {
        if (recipe_ingredient.ingredient == null && child_node.marks.length == 0) {
            let matches = child_node.textContent.matchAll(quantityValueAndProcessingRegExp);
            let local_pos = base_pos + pos;
            for (const match of matches) {
                if (match[5]) {
                    let trimmed = match[5]
                    while (paren_depth > 0 && trimmed.length > 0) {
                        let idx = trimmed.indexOf(")");
                        if (idx >= 0) {
                            trimmed = trimmed.slice(idx+1);
                            paren_depth -= 1;
                        }
                        else trimmed = "";
                    }
                    trimmed = trimmed.trim();
                    recipe_ingredient.qualifier = trimmed;
                }
                if (match[3]) {
                    if (last_quantity != null) {
                        warnings.push({from: local_pos, to: local_pos + match[0].length, message: "Quantity value has no unit"});
                        recipe_ingredient.quantities.push(last_quantity);
                    }
                    last_quantity = {
                        approx: Boolean(match[2]),
                        min_quantity: encodeDecimal(match[3]),
                        max_quantity: (match[4] != undefined) ? encodeDecimal(match[4]) : null,
                        unit: null
                    };
                }

                // Track parenthesis depth
                for (c of match[0]) {
                    if (c == "(") paren_depth += 1;
                    else if (c == ")" && paren_depth > 0) paren_depth -= 1;
                };
                local_pos += match[0].length;
            }
        } else if (recipe_ingredient.ingredient != null && child_node.marks.length == 0) {
            let trimmed = child_node.textContent;
            trimmed = trimmed.replace(/^,+/, "");
            trimmed = trimmed.trim();
            recipe_ingredient.processing = trimmed;
        } else if (child_node.marks[0].type == ingredientSchema.marks.unitName) {
            // Track parenthesis depth
            for (c of child_node.textContent) {
                if (c == "(") paren_depth += 1;
                else if (c == ")") paren_depth -= 1;
            };
            if (last_quantity == null) {
                errors.push({from: base_pos + pos, to: base_pos + pos + child_node.nodeSize, message: "Quantity unit without a value."})
            } else if (child_node.marks[0].attrs.item != null) {
                if (child_node.marks[0].attrs.item.id < 0) {
                    warnings.push({from: base_pos + pos, to: base_pos + pos + child_node.nodeSize, message: "New unit '"+child_node.marks[0].attrs.item.name+"' will be created."});
                }
                last_quantity.unit = child_node.marks[0].attrs.item;
                recipe_ingredient.quantities.push(last_quantity);
                last_quantity = null;
            }
        } else if (child_node.marks[0].type == ingredientSchema.marks.ingredientName) {
            if (recipe_ingredient.ingredient != null) {
                errors.push({from: base_pos + pos, to: base_pos + pos + child_node.nodeSize, message: "Only one ingredient is allowed"});
            } else if (child_node.marks[0].attrs.item != null) {
                if (child_node.marks[0].attrs.item.id < 0) {
                        warnings.push({from: base_pos + pos, to: base_pos + pos + child_node.nodeSize, message: "New ingredient '"+child_node.marks[0].attrs.item.name+"' will be created."});
                }
                recipe_ingredient.ingredient = child_node.marks[0].attrs.item;
            }
        }
    })
    if (last_quantity != null) {
        recipe_ingredient.quantities.push(last_quantity);
    }

    if (recipe_ingredient.ingredient == null) {
        errors.push({from: base_pos, to: base_pos + node.nodeSize, message: "No ingredient found."});
    }

    if (errors.length > 0) {
        node.attrs.item = null;
    } else {
        node.attrs.item = recipe_ingredient;
    }

    return {
        obj: recipe_ingredient,
        errors: errors,
        warnings: warnings
    };
}

function formatIngredientSection(section) {
    return ingredientSchema.node(
        "recipeIngredientSection",
        undefined,
        [
            ingredientSchema.node("recipeIngredientSectionHeader", null, section.name ? ingredientSchema.text(section.name) : null),
            ingredientSchema.node("recipeIngredientList", null, section.ingredients.reduce(function(nodes, recipe_ingredient) {
                nodes.push(formatRecipeIngredient(recipe_ingredient));
                recipe_ingredient.substitutions.forEach(function(sub) {
                    sub.forEach(function(item, index) {nodes.push(formatRecipeIngredient(item, index == 0 ? "recipeIngredientFirstSubItem" : "recipeIngredientSubItem"))})
                });
                return nodes;
            }, []))
        ]
    );
}

function renderIngredientNotificationIcons(type, items) {
    let dom = document.createElement("div");
    dom.className = "recipe-edit-notification-icon recipe-edit-"+type+"-icon";
    if (type == "warning") {
        dom.innerHTML = '<svg width="1.0625em" height="1em" viewBox="0 0 17 16" class="bi bi-exclamation-triangle-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8.982 1.566a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767L8.982 1.566zM8 5a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 5zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/></svg>';
    } else if (type == "error") {
        dom.innerHTML = '<svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-exclamation-octagon-fill" fill="currentColor" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M11.46.146A.5.5 0 0 0 11.107 0H4.893a.5.5 0 0 0-.353.146L.146 4.54A.5.5 0 0 0 0 4.893v6.214a.5.5 0 0 0 .146.353l4.394 4.394a.5.5 0 0 0 .353.146h6.214a.5.5 0 0 0 .353-.146l4.394-4.394a.5.5 0 0 0 .146-.353V4.893a.5.5 0 0 0-.146-.353L11.46.146zM8 4a.905.905 0 0 0-.9.995l.35 3.507a.552.552 0 0 0 1.1 0l.35-3.507A.905.905 0 0 0 8 4zm.002 6a1 1 0 1 0 0 2 1 1 0 0 0 0-2z"/></svg>';
    }
    let item_list = document.createElement("ul");
    item_list.className = "recipe-edit-notification-box";
    items.forEach(item => {
        let dom_item = document.createElement("li");
        dom_item.textContent = item.message;
        item_list.append(dom_item);
    });
    dom.append(item_list);
    return dom;
}

var recipe_errors = [], recipe_warnings = [];

function decorateErrors(pos, node, errors, warnings) {
    let decorations = [];
    if (errors.length > 0) {
        decorations.push(Decoration.node(pos, pos + node.nodeSize, {class: "recipe-edit-line-error"}));
        decorations.push(Decoration.widget(pos + node.nodeSize-1, renderIngredientNotificationIcons("error", errors), {ignoreSelection: true}));
    }
    if (warnings.length > 0) {
        if (errors.length == 0) decorations.push(Decoration.node(pos, pos + node.nodeSize, {class: "recipe-edit-line-warning"}));
        decorations.push(Decoration.widget(pos + node.nodeSize-1, renderIngredientNotificationIcons("warning", warnings), {ignoreSelection: true}));
    }
    recipe_errors.push(...errors);
    recipe_warnings.push(...warnings);
    return decorations
}

function updateRecipeObj(node) {
    let decorations = [];
    let lastRecipeIngredient = null;
    let category_errors = [];
    let category_warnings = [];
    let tag_warnings = [];
    recipe_errors = [];
    recipe_warnings = [];
    recipe.ingredient_sections = [];
    recipe.instruction_sections = [];
    recipe.variations = [];
    recipe.tags = [];
    serializer = DOMSerializer.fromSchema(ingredientSchema);
    node.descendants(function(child_node, pos, parent) {
        if (child_node.type == ingredientSchema.nodes.recipeTitle) {
            recipe.name = child_node.textContent;
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeCategory) {
            if (child_node.attrs.item) {
                recipe.category = child_node.attrs.item;
                if (recipe.category.id < 0) {
                    category_warnings.push({from: pos, to: pos + child_node.nodeSize, message: "New category '"+recipe.category.name+"' will be created'"});
                }
            } else {
                category_errors.push({from: pos, to: pos + child_node.nodeSize, message: "A recipe category is required"});
            }
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeSubcategory) {
            recipe.subcategory = child_node.attrs.item;
            if (recipe.subcategory != null && recipe.subcategory.id < 0) {
                category_warnings.push({from: pos, to: pos + child_node.nodeSize, message: "New subcategory '"+recipe.subcategory.name+"' will be created'"});
            }
            if (category_errors.length > 0 || category_warnings.length > 0) {
                let resolved_pos = node.resolve(pos);
                decorations.push(...decorateErrors(resolved_pos.before(2), resolved_pos.node(2), category_errors, category_warnings));
            }
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeSource) {
            recipe.source = child_node.attrs.item;
            if (recipe.source != null && recipe.source.id < 0) {
                decorations.push(...decorateErrors(pos, child_node, [], [{from: pos, to: pos + child_node.nodeSize, message: "New recipe source will be added"}]));
            }
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeTag) {
            recipe.tags.push(child_node.attrs.item);
            if (child_node.attrs.item != null && child_node.attrs.item.id < 0) {
                tag_warnings.push({from: pos, to: pos + child_node.node_size, message: "New recipe tag '"+child_node.attrs.item.name+"' will be created"});
            }
            let resolved = node.resolve(pos);
            if (resolved.index(2) == resolved.node(2).childCount-2) {
                decorations.push(...decorateErrors(resolved.before(2), resolved.node(2), [], tag_warnings));
            }
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeYields) {
            let {obj, errors, warnings} = parseRecipeYields(child_node, pos);
            recipe.yields = obj;
            decorations.push(...decorateErrors(node.resolve(pos).before(), parent, errors, warnings));
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipePrepTimeContainer) {
            if (recipe.prep_duration_min === undefined) {
                decorations.push(...decorateErrors(pos, child_node, [{from: pos, to: pos + child_node.nodeSize, message: "Error parsing prep time."}], []));
            }
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeCookTimeContainer) {
            if (recipe.cook_duration_min === undefined) {
                decorations.push(...decorateErrors(pos, child_node, [{from: pos, to: pos + child_node.nodeSize, message: "Error parsing cook time."}], []));
            }
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeDescription) {
            if (child_node.textContent.length == 0) {
                recipe.description = "";
            } else {
                recipe.description = serializer.serializeNode(child_node).innerHTML;
            }
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeIngredientSectionHeader) {
            recipe.ingredient_sections.push({
                name: child_node.textContent.length == 0 ? null : child_node.textContent,
                ingredients: [],
            });
            if (recipe.ingredient_sections.length > 1 && !recipe.ingredient_sections[recipe.ingredient_sections.length-1].name) {
                decorations.push(...decorateErrors(pos, child_node, [{from: pos, to: pos + child_node.nodeSize, message: "Only the first ingredient section doesn't need a name"}], []));
            }
            return false;
        } else if (child_node.type.spec.group == "recipeIngredientType") {
            let {obj, errors, warnings} = parseRecipeIngredient(child_node, pos);
            let lastSection = recipe.ingredient_sections[recipe.ingredient_sections.length-1];
            if (child_node.type == ingredientSchema.nodes.recipeIngredient) {
                lastSection.ingredients.push(obj);
            } else if (child_node.type == ingredientSchema.nodes.recipeIngredientFirstSubItem) {
                lastSection.ingredients[lastSection.ingredients.length - 1].substitutions.push([obj]);
            } else {
                let lastIngredient = lastSection.ingredients[lastSection.ingredients.length - 1];
                lastIngredient.substitutions[lastIngredient.substitutions.length - 1].push(obj);
            }
            decorations.push(...decorateErrors(pos, child_node, errors, warnings));
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeInstructionSectionHeader) {
            recipe.instruction_sections.push({
                name: child_node.textContent.length == 0 ? null : child_node.textContent,
                instructions: [],
            });
            if (recipe.instruction_sections.length > 1 && !recipe.instruction_sections[recipe.instruction_sections.length-1].name) {
                decorations.push(...decorateErrors(pos, child_node, [{from: pos, to: pos + child_node.nodeSize, message: "Only the first instruction section doesn't need a name"}], []));
            }
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeInstruction) {
            recipe.instruction_sections[recipe.instruction_sections.length - 1].instructions.push(serializer.serializeNode(child_node).innerHTML);
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeVariationName) {
            recipe.variations.push({
                name: child_node.textContent,
                variation: "",
            });
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeVariationContent) {
            if (child_node.textContent.length > 0) {
                recipe.variations[recipe.variations.length - 1].variation = serializer.serializeNode(child_node).innerHTML;
            }
            return false;
        } else if (child_node.type == ingredientSchema.nodes.recipeNotes) {
            if (child_node.textContent.length == 0) {
                recipe.notes = "";
            } else {
                recipe.notes = serializer.serializeNode(child_node).innerHTML;
            }
            return false;
        }
        return true;
    });
    recipe.variations = recipe.variations.filter(v => (v.name.length > 0) || (v.variation.length > 0));
    return DecorationSet.create(node, decorations);
}

function inYieldNode(view) {
    let {$head} = view.state.selection;
    for (let depth = $head.depth; depth >= 0; depth--) {
        if ($head.node(depth).type.name == "recipeYields") {
            return true;
        }
    }
    return false;
}
function inIngredientNode(view) {
    let {$head} = view.state.selection;
    for (let depth = $head.depth; depth >= 0; depth--) {
        if ($head.node(depth).type.name == "recipeIngredientsBlock") {
            return true;
        }
    }
    return false;
}
function inInstructionNode(view) {
    let {$head} = view.state.selection;
    for (let depth = $head.depth; depth >= 0; depth--) {
        if ($head.node(depth).type.name == "recipeInstructionBlock") {
            return true;
        }
    }
    return false;
}
function inVariationNode(view) {
    let {$head} = view.state.selection;
    for (let depth = $head.depth; depth >= 0; depth--) {
        if ($head.node(depth).type.name == "recipeVariationsBlock") {
            return true;
        }
    }
    return false;
}
function inRichTextNode(view) {
    let {$head} = view.state.selection;
    for (let depth = $head.depth; depth >= 0; depth--) {
        if ($head.node(depth).type.spec.content.startsWith("richText")) {
            return true;
        }
    }
    return false;
}

function richTextMarksAllowed(view) {
    let {$head} = view.state.selection;
    for (let depth = $head.depth; depth >= 0; depth--) {
        if ($head.node(depth).type.spec.marks == "richTextMarks") {
            return true;
        }
    }
    return false;
}

var ingredientUnitRegExp;
function stringToRecipeIngredient(text) {
    let result = Fragment.empty;
    text.split("\n").forEach(child_text => {
        child_text = child_text.trimStart().trimEnd();
        if (!child_text) return;

        let newNode = ingredientSchema.node(ingredientSchema.nodes.recipeIngredient);

        // Try to find units and ingredients
        let ingredientFound = false;
        let ingredient_unit_split = [child_text];
        if (ingredients.length > 0 || units.length > 0) ingredient_unit_split = child_text.split(ingredientUnitRegExp);
        for (let ingr_unit_split_idx = 0; ingr_unit_split_idx < ingredient_unit_split.length; ingr_unit_split_idx++) {
            if (!ingredient_unit_split[ingr_unit_split_idx]) continue;
            let mark = null;

            if (ingr_unit_split_idx % 2 == 1 && !ingredientFound) {
                // Odd indices were matched a unit or ingredient
                let item = ingredients.find(ingr => ingredientItemMatchesString(ingr, ingredient_unit_split[ingr_unit_split_idx]))
                if (item !== undefined) {
                    mark = ingredientSchema.mark(ingredientSchema.marks.ingredientName, item);
                    ingredientFound = true;
                } else if (item == undefined) {
                    item = units.find(unit => unitItemMatchesString(unit, ingredient_unit_split[ingr_unit_split_idx]))
                    mark = ingredientSchema.mark(ingredientSchema.marks.unitName, item);
                }
            }

            newNode = newNode.copy(newNode.content.append(Fragment.from(ingredientSchema.text(ingredient_unit_split[ingr_unit_split_idx], mark))));
        }
        result = result.append(Fragment.from(newNode));
    });
    return result;
}

function handleIngredientPaste(view, event, pastedSlice) {
    let slice = undefined;
    let tr = view.state.tr;
    if (inIngredientNode(view)) {
        // Create regex that match ingredients and units
        ingredientUnitRegExp = RegExp(ingredients.reduce((strs, ing) => {
            if (ing.name) strs.push(escapeRegExp(ing.name));
            if (ing.plural) strs.push(escapeRegExp(ing.plural));
            return strs;
        }, []).concat(units.reduce((strs, unit) => {
            if (unit.name) strs.push(escapeRegExp(unit.name));
            if (unit.plural) strs.push(escapeRegExp(unit.plural));
            if (unit.abbrev) strs.push(escapeRegExp(unit.abbrev) + "\\.?");
            return strs;
        }, [])).sort((a, b) => b.length - a.length).reduce((res, item, idx) => res + (idx > 0 ? "|" : "") + item, "\\b(") + ")(?=\\b|\\s|$)", "i");

        let pastedFragment = Fragment.empty;
        pastedFragment = stringToRecipeIngredient(event.clipboardData.getData("text/plain"))
        if (pastedFragment.childCount > 0) {
            if (view.state.selection.$head.node().type == ingredientSchema.nodes.recipeIngredientSectionHeader) {
                // Make a full ingredient section if pasting into the header
                slice = new Slice(Fragment.from(
                    ingredientSchema.node(ingredientSchema.nodes.recipeIngredientSection, null, [
                        ingredientSchema.node(ingredientSchema.nodes.recipeIngredientSectionHeader, null, ingredientSchema.text(pastedFragment.child(0).textContent)),
                        ingredientSchema.node(ingredientSchema.nodes.recipeIngredientList, null, pastedFragment.content.slice(1))
                    ])
                ), 3, 4);
                // If at the end of the header line, and there is not a current selection, then select from cursor into
                // beginning of following ingredient so a new ingredient section isn't created
                if (view.state.selection.empty && view.state.selection.head == view.state.selection.$head.end()) {
                    tr = tr.setSelection(TextSelection.create(tr.doc, view.state.selection.head, view.state.selection.head+3));
                }
            } else {
                slice = new Slice(pastedFragment, 1, 1);
            }
        }
    } else if (inInstructionNode(view)) {
        let modifiedFragment = Fragment.empty;
        let lastNodeWasText = false;
        pastedSlice.content.forEach(function(top_node, offset, index) {
            if (top_node.inlineContent) {
                modifiedFragment = modifiedFragment.append(Fragment.from(ingredientSchema.node(ingredientSchema.nodes.recipeInstruction, undefined, top_node.content)));
                lastNodeWasText = false;
            } else if (top_node.isText) {
                if (lastNodeWasText) {
                    modifiedFragment = modifiedFragment.replaceChild(modifiedFragment.size-1,
                        ingredientSchema.node(ingredientSchema.nodes.recipeInstruction, undefined, modifiedFragment.child(modifiedFragment.size-1).content.append(top_node))
                    );
                } else {
                    modifiedFragment = modifiedFragment.append(Fragment.from(ingredientSchema.node(ingredientSchema.nodes.recipeInstruction, undefined, top_node)));
                    lastNodeWasText = true;
                }
            } else {
                top_node.descendants(function(node, pos, parent) {
                    if (node.inlineContent) {
                        modifiedFragment = modifiedFragment.append(Fragment.from(ingredientSchema.node(ingredientSchema.nodes.recipeInstruction, undefined, node.content)));
                        lastNodeWasText = false;
                    } else if (node.isText) {
                        if (lastNodeWasText) {
                            modifiedFragment = modifiedFragment.replaceChild(modifiedFragment.size-1,
                                ingredientSchema.node(ingredientSchema.nodes.recipeInstruction, undefined, modifiedFragment.child(modifiedFragment.size-1).content.append(node))
                            );
                        } else {
                            modifiedFragment = modifiedFragment.append(Fragment.from(ingredientSchema.node(ingredientSchema.nodes.recipeInstruction, undefined, node)));
                            lastNodeWasText = true;
                        }
                    } else {
                        // Continue into the node if not an inline node
                        return true;
                    }
                    return false; // All the content under this node has been parsed, don't go deeper.
                });
            }
        });
        if (modifiedFragment.childCount > 0) {
            if (view.state.selection.$head.node().type == ingredientSchema.nodes.recipeInstructionSectionHeader) {
                // Make a full ingredient section if pasting into the header
                slice = new Slice(Fragment.from(
                    ingredientSchema.node(ingredientSchema.nodes.recipeInstructionSection, null, [
                        ingredientSchema.node(ingredientSchema.nodes.recipeInstructionSectionHeader, null, ingredientSchema.text(modifiedFragment.child(0).textContent)),
                        ingredientSchema.node(ingredientSchema.nodes.recipeInstructionList, null, modifiedFragment.content.slice(1))
                    ])
                ), 3, 4);
                // If at the end of the header line, and there is not a current selection, then select from cursor into
                // beginning of following ingredient so a new ingredient section isn't created
                if (view.state.selection.empty && view.state.selection.head == view.state.selection.$head.end()) {
                    tr = tr.setSelection(TextSelection.create(tr.doc, view.state.selection.head, view.state.selection.head+3));
                }
            } else {
                slice = new Slice(modifiedFragment, 1, 1);
            }
        }
    }
    if (slice != undefined) {
        let singleNode = slice.openStart == 0 && slice.openEnd == 0 && slice.content.childCount == 1 ? slice.content.firstChild : null;
        tr = singleNode ? tr.replaceSelectionWith(singleNode, view.shiftKey) : tr.replaceSelection(slice);
        view.dispatch(tr.scrollIntoView().setMeta("paste", true).setMeta("uiEvent", "paste"));
        return true;
    } else {
        return false;
    }
}

class recipeEditorSaveButtonView {
    constructor(plugin, editorView) {
        this.plugin = plugin;
        this.editorView = editorView;

        this.button_dom = document.createElement("button");
        this.button_dom.id = "recipe-save-button";
        this.button_dom.className = "btn btn-dark btn-sm mt-2";
        this.button_dom.setAttribute("type", "button");
        this.button_dom.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-save2" viewBox="0 0 16 16"><path d="M2 1a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V2a1 1 0 0 0-1-1H9.5a1 1 0 0 0-1 1v4.5h2a.5.5 0 0 1 .354.854l-2.5 2.5a.5.5 0 0 1-.708 0l-2.5-2.5A.5.5 0 0 1 5.5 6.5h2V2a2 2 0 0 1 2-2H14a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h2.5a.5.5 0 0 1 0 1H2z"/></svg> Save Recipe';
        this.button_dom.plugin = this;
        this.button_dom.addEventListener("click", function(e) {
            if (recipe_errors.length > 0 || recipe_warnings.length > 0) {
                let modal_body = document.createElement("div");
                modal_body.append(document.createElement("p"));
                if (recipe_errors.length > 0) {
                    modal_body.lastChild.textContent = "Recipe cannot be saved because there are errors";
                    modal_body.append(document.createTextNode("Errors:"));
                    modal_body.append(document.createElement("ul"));
                    recipe_errors.forEach(e => {
                        modal_body.lastChild.append(document.createElement("li"));
                        modal_body.lastChild.lastChild.textContent = e.message;
                    });
                } else {
                    modal_body.lastChild.textContent = "Recipe has warnings, are you sure you want to save changes";
                }
                if (recipe_warnings.length > 0) {
                    modal_body.append(document.createTextNode("Warnings:"));
                    modal_body.append(document.createElement("ul"));
                    recipe_warnings.forEach(w => {
                        modal_body.lastChild.append(document.createElement("li"));
                        modal_body.lastChild.lastChild.textContent = w.message;
                    });
                }

                showModal(this.plugin.editorView.dom.parentNode, {
                    header_class: recipe_errors.length > 0 ? "bg-danger" : "bg-warning",
                    title: recipe_errors.length > 0 ? "Errors in Recipe" : "Warnings in Recipe",
                    body_node: modal_body,
                    ok_btn_text: "Save",
                    ok_en: recipe_errors.length == 0,
                    ok_cb: function() {this.save_recipe()}.bind(this.plugin),
                });
            } else {
                this.plugin.save_recipe();
            }
        });
        editorView.dom.parentNode.insertBefore(this.button_dom, editorView.dom.parentNode.firstChild);
    }

    save_recipe() {
            let csrftoken = getCookie("csrftoken");
            fetch(location.origin+'/api/recipes/' + (recipe.id === undefined ? '' : (recipe.id + '/')), {
                method: (recipe.id === undefined ? 'POST' : 'PUT'),
                body: JSON.stringify(recipe),
                headers: {
                    'X-Csrftoken': csrftoken,
                    'Content-Type': 'application/json'
               }
            }).then(response => {
                return {ok: response.ok, data: response.json()}
            }).then(res => {
                // Return to the recipe view page if the recipe was saved successfully
                if (res.ok) {
                    let body_node = document.createElement("p");
                    body_node.textContent = "Recipe saved successfully.";
                    showModal(this.editorView.dom.parentNode, {
                        header_class: "bg-success",
                        title: "Success",
                        body_node: body_node,
                        ok_cb: function(res) {return function() {
                            res.data.then(data => window.location.href = [window.location.protocol, "//", window.location.host, "/recipe/" + data.id + "/" + data.slug].join(""));
                        }}(res),
                        cancel_en: false,
                    });
                } else {
                    let body_node = document.createElement("p");
                    body_node.textContent = "Error saving recipe.";
                    showModal(this.editorView.dom.parentNode, {
                        header_class: "bg-danger",
                        title: "Error!",
                        body_node: body_node,
                        ok_en: false,
                        ok_cb() {},
                    });
                }
            });
    }

    destroy() {this.button_dom.remove()};
}

let recipeEditorSaveButtonPlugin = new Plugin({
    view(editorView) {
        this.toolbarView = new recipeEditorSaveButtonView(this, editorView);
        return this.toolbarView;
    },
});

class ingredientInputToolbarView {
    constructor(commandGroups, plugin, editorView) {
        this.plugin = plugin;
        this.editorView = editorView;
        this.commandGroups = commandGroups
        this.dom = document.createElement("div");
        this.dom.className = "recipe-edit-toolbar btn-toolbar";
        this.commandGroups.forEach(group => {
            let groupDiv = document.createElement("div");
            groupDiv.className = "btn-group btn-group-sm mr-2";
            if (group.showGroup !== undefined && !group.showGroup(this.editorView)) {
                groupDiv.style.display = "none";
            }
            let button_dropdown_div = undefined;
            if (group.dropdownLabelHTML !== undefined) {
                let dropdown_div = document.createElement("div");
                dropdown_div.style.display = "none";
                dropdown_div.className = "btn-group btn-group-sm mr-2";
                let dropdown_button = document.createElement("button");
                dropdown_button.className = "btn btn-secondary dropdown-toggle";
                dropdown_button.setAttribute("type", "button");
                dropdown_button.setAttribute("data-toggle", "dropdown");
                dropdown_button.setAttribute("aria-haspopup", "true");
                dropdown_button.setAttribute("aria-expanded", "true");
                dropdown_button.innerHTML = group.dropdownLabelHTML;
                groupDiv.append(dropdown_button);
                let button_dropdown = document.createElement("div");
                button_dropdown.className = "dropdown-menu";
                button_dropdown_div = document.createElement("div");
                button_dropdown_div.className = "btn-group-vertical"
                button_dropdown.append(button_dropdown_div);
                dropdown_div.append(dropdown_button);
                dropdown_div.append(button_dropdown);
                group.dropdown_dom = dropdown_div;
                this.dom.append(dropdown_div);
            }
            group.commands.forEach(cmdSpec => {
                if (!('label' in cmdSpec || 'labelHTML' in cmdSpec)) return;
                let button = document.createElement("button");
                button.className = "btn btn-secondary recipe-edit-toolbar-btn";
                button.setAttribute("type", "button");
                button.setAttribute("data-toggle", "tooltip");
                button.setAttribute("data-placement", "bottom");
                button.setAttribute("title", cmdSpec.tooltip +
                                            (('shortcuts' in cmdSpec && cmdSpec.shortcuts) ?
                                                (" (" + cmdSpec.shortcuts.join(", ") + ")") :
                                                ""));
                if (cmdSpec.label !== undefined) {
                    button.textContent = cmdSpec.label;
                } else {
                    button.innerHTML = cmdSpec.labelHTML;
                }
                cmdSpec.dom = button;
                button.toolbarBtnSpec = cmdSpec;
                if (group.dropdownLabelHTML !== undefined) {
                    cmdSpec.dropdown_dom = button.cloneNode(true);
                    cmdSpec.dropdown_dom.toolbarBtnSpec = cmdSpec;
                    button_dropdown_div.append(cmdSpec.dropdown_dom);
                }
                groupDiv.append(button);
            });
            if (groupDiv.childElementCount > 0) {
                group.dom = groupDiv;
                this.dom.append(groupDiv);
            }
        })
        this.dom.addEventListener("click", e => {
            let btn_node = e.target.closest('.recipe-edit-toolbar-btn');
            if (btn_node == null) return;
            if (btn_node.toolbarBtnSpec.command == null) return;
            btn_node.toolbarBtnSpec.command(editorView.state, editorView.dispatch, editorView);
            editorView.focus();
        })
        this.resize_obs = new ResizeObserver(function(entries) {this.update(editorView, null)}.bind(this));
        this.resize_obs.observe(this.dom);
        editorView.dom.parentNode.insertBefore(this.dom, editorView.dom.parentNode.firstChild);
        this.update(editorView, null);
    }

    update(view, lastState) {
        let state = view.state
        // Don't do anything if the document/selection didn't change
        if (lastState && lastState.doc.eq(state.doc) &&
            lastState.selection.eq(state.selection)) return


        this.commandGroups.forEach(group => {
            if (!('dom' in group)) return;
            if (group.dropdownLabelHTML !== undefined) group.dropdown_dom.style.display = "none";
            if (group.showGroup !== undefined && !group.showGroup(this.editorView)) {
                group.dom.style.display = "none";
            } else {
                group.dom.style.display = "";
            }
            group.commands.forEach(cmdSpec => {
                if (!('dom' in cmdSpec)) return;
                cmdSpec.dom.disabled = (cmdSpec.command == null || !cmdSpec.command(state, null, view));
                if (cmdSpec.dropdown_dom !== undefined) cmdSpec.dropdown_dom.disabled = cmdSpec.dom.disabled;
            });
        });

        if (this.dom.offsetWidth < this.dom.scrollWidth) {
            this.commandGroups.forEach(group => {
                if (group.dropdown_dom === undefined) return;
                group.dropdown_dom.style.display = group.dom.style.display;
                group.dom.style.display = "none";
            })
        }
    }
    destroy() {this.dom.remove()}
}

let commandGroups = [
    {
        commands: [
            {
                command: undo,
                shortcuts: ["Ctrl-z"],
                tooltip: "Undo",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-counterclockwise" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M8 3a5 5 0 1 1-4.546 2.914.5.5 0 0 0-.908-.417A6 6 0 1 0 8 2v1z"/><path d="M8 4.466V.534a.25.25 0 0 0-.41-.192L5.23 2.308a.25.25 0 0 0 0 .384l2.36 1.966A.25.25 0 0 0 8 4.466z"/></svg>'
            },
            {
                command: redo,
                shortcuts: ["Shift-Ctrl-z", "Ctrl-y"],
                tooltip: "Redo",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-arrow-clockwise" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2v1z"/><path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466z"/></svg>'
            },
        ]
    },
    {
        showGroup: richTextMarksAllowed,
        dropdownLabelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-fonts" viewBox="0 0 16 16"><path d="M12.258 3H3.747l-.082 2.46h.478c.26-1.544.76-1.783 2.694-1.845l.424-.013v7.827c0 .663-.144.82-1.3.923v.52h4.082v-.52c-1.162-.103-1.306-.26-1.306-.923V3.602l.43.013c1.935.062 2.434.301 2.694 1.846h.479L12.258 3z"/></svg>',
        commands: [
            {
                command: toggleMark(ingredientSchema.marks.strong),
                shortcuts: ["Ctrl-b"],
                tooltip: "Bold",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-type-bold" viewBox="0 0 16 16"><path d="M8.21 13c2.106 0 3.412-1.087 3.412-2.823 0-1.306-.984-2.283-2.324-2.386v-.055a2.176 2.176 0 0 0 1.852-2.14c0-1.51-1.162-2.46-3.014-2.46H3.843V13H8.21zM5.908 4.674h1.696c.963 0 1.517.451 1.517 1.244 0 .834-.629 1.32-1.73 1.32H5.908V4.673zm0 6.788V8.598h1.73c1.217 0 1.88.492 1.88 1.415 0 .943-.643 1.449-1.832 1.449H5.907z"/></svg>'
            },
            {
                command: toggleMark(ingredientSchema.marks.em),
                shortcuts: ["Ctrl-i"],
                tooltip: "Italic",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-type-italic" viewBox="0 0 16 16"><path d="M7.991 11.674L9.53 4.455c.123-.595.246-.71 1.347-.807l.11-.52H7.211l-.11.52c1.06.096 1.128.212 1.005.807L6.57 11.674c-.123.595-.246.71-1.346.806l-.11.52h3.774l.11-.52c-1.06-.095-1.129-.211-1.006-.806z"/></svg>'
            },
            {
                command: toggleMark(ingredientSchema.marks.underline),
                shortcuts: ["Ctrl-u"],
                tooltip: "Underline",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-type-underline" viewBox="0 0 16 16"><path d="M5.313 3.136h-1.23V9.54c0 2.105 1.47 3.623 3.917 3.623s3.917-1.518 3.917-3.623V3.136h-1.23v6.323c0 1.49-.978 2.57-2.687 2.57-1.709 0-2.687-1.08-2.687-2.57V3.136zM12.5 15h-9v-1h9v1z"/></svg>'
            },
            {
                command: toggleMark(ingredientSchema.marks.strikethrough),
                tooltip: "Strikethrough",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-type-strikethrough" viewBox="0 0 16 16"><path d="M6.333 5.686c0 .31.083.581.27.814H5.166a2.776 2.776 0 0 1-.099-.76c0-1.627 1.436-2.768 3.48-2.768 1.969 0 3.39 1.175 3.445 2.85h-1.23c-.11-1.08-.964-1.743-2.25-1.743-1.23 0-2.18.602-2.18 1.607zm2.194 7.478c-2.153 0-3.589-1.107-3.705-2.81h1.23c.144 1.06 1.129 1.703 2.544 1.703 1.34 0 2.31-.705 2.31-1.675 0-.827-.547-1.374-1.914-1.675L8.046 8.5H1v-1h14v1h-3.504c.468.437.675.994.675 1.697 0 1.826-1.436 2.967-3.644 2.967z"/></svg>'
            },
            {
                command: toggleMark(ingredientSchema.marks.link),
                tooltip: "Link",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-link" viewBox="0 0 16 16"><path d="M6.354 5.5H4a3 3 0 0 0 0 6h3a3 3 0 0 0 2.83-4H9c-.086 0-.17.01-.25.031A2 2 0 0 1 7 10.5H4a2 2 0 1 1 0-4h1.535c.218-.376.495-.714.82-1z"/><path d="M9 5.5a3 3 0 0 0-2.83 4h1.098A2 2 0 0 1 9 6.5h3a2 2 0 1 1 0 4h-1.535a4.02 4.02 0 0 1-.82 1H12a3 3 0 1 0 0-6H9z"/></svg>'
            },
        ]
    },
    {
        showGroup: inRichTextNode,
        dropdownLabelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-type" viewBox="0 0 16 16"><path d="M2.244 13.081l.943-2.803H6.66l.944 2.803H8.86L5.54 3.75H4.322L1 13.081h1.244zm2.7-7.923L6.34 9.314H3.51l1.4-4.156h.034zm9.146 7.027h.035v.896h1.128V8.125c0-1.51-1.114-2.345-2.646-2.345-1.736 0-2.59.916-2.666 2.174h1.108c.068-.718.595-1.19 1.517-1.19.971 0 1.518.52 1.518 1.464v.731H12.19c-1.647.007-2.522.8-2.522 2.058 0 1.319.957 2.18 2.345 2.18 1.06 0 1.716-.43 2.078-1.011zm-1.763.035c-.752 0-1.456-.397-1.456-1.244 0-.65.424-1.115 1.408-1.115h1.805v.834c0 .896-.752 1.525-1.757 1.525z"/></svg>',
        commands: [
            {
                command: setBlockType(ingredientSchema.nodes.paragraph),
                shortcuts: ["Shift-Ctrl-0"],
                tooltip: "Normal Text",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-type" viewBox="0 0 16 16"><path d="M2.244 13.081l.943-2.803H6.66l.944 2.803H8.86L5.54 3.75H4.322L1 13.081h1.244zm2.7-7.923L6.34 9.314H3.51l1.4-4.156h.034zm9.146 7.027h.035v.896h1.128V8.125c0-1.51-1.114-2.345-2.646-2.345-1.736 0-2.59.916-2.666 2.174h1.108c.068-.718.595-1.19 1.517-1.19.971 0 1.518.52 1.518 1.464v.731H12.19c-1.647.007-2.522.8-2.522 2.058 0 1.319.957 2.18 2.345 2.18 1.06 0 1.716-.43 2.078-1.011zm-1.763.035c-.752 0-1.456-.397-1.456-1.244 0-.65.424-1.115 1.408-1.115h1.805v.834c0 .896-.752 1.525-1.757 1.525z"/></svg>'
            },
            {
                command: setBlockType(ingredientSchema.nodes.heading, {level: 1}),
                shortcuts: ["Shift-Ctrl-1"],
                tooltip: "Header 1",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-type-h1" viewBox="0 0 16 16"><path d="M8.637 13V3.669H7.379V7.62H2.758V3.67H1.5V13h1.258V8.728h4.62V13h1.259zm5.329 0V3.669h-1.244L10.5 5.316v1.265l2.16-1.565h.062V13h1.244z"/></svg>'
            },
            {
                command: setBlockType(ingredientSchema.nodes.heading, {level: 2}),
                shortcuts: ["Shift-Ctrl-2"],
                tooltip: "Header 2",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-type-h2" viewBox="0 0 16 16"><path d="M7.638 13V3.669H6.38V7.62H1.759V3.67H.5V13h1.258V8.728h4.62V13h1.259zm3.022-6.733v-.048c0-.889.63-1.668 1.716-1.668.957 0 1.675.608 1.675 1.572 0 .855-.554 1.504-1.067 2.085l-3.513 3.999V13H15.5v-1.094h-4.245v-.075l2.481-2.844c.875-.998 1.586-1.784 1.586-2.953 0-1.463-1.155-2.556-2.919-2.556-1.941 0-2.966 1.326-2.966 2.74v.049h1.223z"/></svg>'
            },
            {
                command: setBlockType(ingredientSchema.nodes.heading, {level: 3}),
                shortcuts: ["Shift-Ctrl-3"],
                tooltip: "Header 3",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-type-h3" viewBox="0 0 16 16"><path d="M7.637 13V3.669H6.379V7.62H1.758V3.67H.5V13h1.258V8.728h4.62V13h1.259zm3.625-4.272h1.018c1.142 0 1.935.67 1.949 1.674.013 1.005-.78 1.737-2.01 1.73-1.08-.007-1.853-.588-1.935-1.32H9.108c.069 1.327 1.224 2.386 3.083 2.386 1.935 0 3.343-1.155 3.309-2.789-.027-1.51-1.251-2.16-2.037-2.249v-.068c.704-.123 1.764-.91 1.723-2.229-.035-1.353-1.176-2.4-2.954-2.385-1.873.006-2.857 1.162-2.898 2.358h1.196c.062-.69.711-1.299 1.696-1.299.998 0 1.695.622 1.695 1.525.007.922-.718 1.592-1.695 1.592h-.964v1.074z"/></svg>'
            },
            {
                command: setBlockType(ingredientSchema.nodes.heading, {level: 4}),
                shortcuts: ["Shift-Ctrl-4"]
            },
            {
                command: setBlockType(ingredientSchema.nodes.heading, {level: 5}),
                shortcuts: ["Shift-Ctrl-5"]
            },
            {
                command: setBlockType(ingredientSchema.nodes.heading, {level: 6}),
                shortcuts: ["Shift-Ctrl-6"]
            }
        ]
    },
    {
        showGroup: inRichTextNode,
        dropdownLabelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-text-paragraph" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M2 12.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm4-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5z"/></svg>',
        commands: [
            {
                command: changeNodeAttrValue(ingredientSchema.nodes.paragraph, "textAlign", "left"),
                shortcuts: ["Shift-Ctrl-l"],
                tooltip: "Left Align",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-text-left" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M2 12.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/></svg>'
            },
            {
                command: changeNodeAttrValue(ingredientSchema.nodes.paragraph, "textAlign", "center"),
                shortcuts: ["Shift-Ctrl-c"],
                tooltip: "Center Align",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-text-center" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M4 12.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm2-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-2-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/></svg>'
            },
            {
                command: changeNodeAttrValue(ingredientSchema.nodes.paragraph, "textAlign", "right"),
                shortcuts: ["Shift-Ctrl-r"],
                tooltip: "Right Align",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-text-right" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M6 12.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-4-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm4-3a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5zm-4-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/></svg>'
            },
            {
                command: changeNodeAttrValue(ingredientSchema.nodes.paragraph, "textAlign", "justify"),
                tooltip: "Justiy",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-justify" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M2 12.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm0-3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/></svg>'
            },
        ]
    },
    {
        showGroup: inRichTextNode,
        commands: [
            {
                command: insertHorizontalRule,
                shortcuts: ["Ctrl-_"],
                tooltip: "Horizontal Rule",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-hr" viewBox="0 0 16 16"><path d="M12 3H4a1 1 0 0 0-1 1v2.5H2V4a2 2 0 0 1 2-2h8a2 2 0 0 1 2 2v2.5h-1V4a1 1 0 0 0-1-1zM2 9.5h1V12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V9.5h1V12a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V9.5zm-1.5-2a.5.5 0 0 0 0 1h15a.5.5 0 0 0 0-1H.5z"/></svg>'
            },
            {
                command: wrapIn(ingredientSchema.nodes.blockquote),
                shortcuts: ["Ctrl-'"],
                tooltip: "Block Quote",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-blockquote-left" viewBox="0 0 16 16"><path d="M2.5 3a.5.5 0 0 0 0 1h11a.5.5 0 0 0 0-1h-11zm5 3a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1h-6zm0 3a.5.5 0 0 0 0 1h6a.5.5 0 0 0 0-1h-6zm-5 3a.5.5 0 0 0 0 1h11a.5.5 0 0 0 0-1h-11zm.79-5.373c.112-.078.26-.17.444-.275L3.524 6c-.122.074-.272.17-.452.287-.18.117-.35.26-.51.428a2.425 2.425 0 0 0-.398.562c-.11.207-.164.438-.164.692 0 .36.072.65.217.873.144.219.385.328.72.328.215 0 .383-.07.504-.211a.697.697 0 0 0 .188-.463c0-.23-.07-.404-.211-.521-.137-.121-.326-.182-.568-.182h-.282c.024-.203.065-.37.123-.498a1.38 1.38 0 0 1 .252-.37 1.94 1.94 0 0 1 .346-.298zm2.167 0c.113-.078.262-.17.445-.275L5.692 6c-.122.074-.272.17-.452.287-.18.117-.35.26-.51.428a2.425 2.425 0 0 0-.398.562c-.11.207-.164.438-.164.692 0 .36.072.65.217.873.144.219.385.328.72.328.215 0 .383-.07.504-.211a.697.697 0 0 0 .188-.463c0-.23-.07-.404-.211-.521-.137-.121-.326-.182-.568-.182h-.282a1.75 1.75 0 0 1 .118-.492c.058-.13.144-.254.257-.375a1.94 1.94 0 0 1 .346-.3z"/></svg>'
            }
        ]
    },
    {
        showGroup: inRichTextNode,
        dropdownLabelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-nested" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M4.5 11.5A.5.5 0 0 1 5 11h10a.5.5 0 0 1 0 1H5a.5.5 0 0 1-.5-.5zm-2-4A.5.5 0 0 1 3 7h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm-2-4A.5.5 0 0 1 1 3h10a.5.5 0 0 1 0 1H1a.5.5 0 0 1-.5-.5z"/></svg>',
        commands: [
            {
                command: wrapInList(ingredientSchema.nodes.bulletList),
                shortcuts: ["Shift-Ctrl-8"],
                tooltip: "Bulleted List",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/></svg>'
            },
            {
                command: wrapInList(ingredientSchema.nodes.orderedList),
                shortcuts: ["Shift-Ctrl-9"],
                tooltip: "Numbered List",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-ol" viewBox="0 0 16 16"><path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5z"/><path d="M1.713 11.865v-.474H2c.217 0 .363-.137.363-.317 0-.185-.158-.31-.361-.31-.223 0-.367.152-.373.31h-.59c.016-.467.373-.787.986-.787.588-.002.954.291.957.703a.595.595 0 0 1-.492.594v.033a.615.615 0 0 1 .569.631c.003.533-.502.8-1.051.8-.656 0-1-.37-1.008-.794h.582c.008.178.186.306.422.309.254 0 .424-.145.422-.35-.002-.195-.155-.348-.414-.348h-.3zm-.004-4.699h-.604v-.035c0-.408.295-.844.958-.844.583 0 .96.326.96.756 0 .389-.257.617-.476.848l-.537.572v.03h1.054V9H1.143v-.395l.957-.99c.138-.142.293-.304.293-.508 0-.18-.147-.32-.342-.32a.33.33 0 0 0-.342.338v.041zM2.564 5h-.635V2.924h-.031l-.598.42v-.567l.629-.443h.635V5z"/></svg>'
            },
            {
                command: sinkListItem(ingredientSchema.nodes.listItem),
                shortcuts: ["Tab"],
                tooltip: "Increase Indent",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-text-indent-left" viewBox="0 0 16 16"><path d="M2 3.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm.646 2.146a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1 0 .708l-2 2a.5.5 0 0 1-.708-.708L4.293 8 2.646 6.354a.5.5 0 0 1 0-.708zM7 6.5a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm-5 3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/></svg>'
            },
            {
                command: chainCommands(liftListItem(ingredientSchema.nodes.listItem), lift),
                shortcuts: ["Shift-Tab"],
                tooltip: "Decrease Indent",
                labelHTML: '<svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-text-indent-right" viewBox="0 0 16 16"><path d="M2 3.5a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5zm10.646 2.146a.5.5 0 0 1 .708.708L11.707 8l1.647 1.646a.5.5 0 0 1-.708.708l-2-2a.5.5 0 0 1 0-.708l2-2zM2 6.5a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h6a.5.5 0 0 1 0 1h-6a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h11a.5.5 0 0 1 0 1h-11a.5.5 0 0 1-.5-.5z"/></svg>'
            }
        ]
    },
    {
        showGroup: inIngredientNode,
        commands: [
            {label: "Header", tooltip: "Make current line header", command: makeIngredientHeader},
            {label: "Ingredient", tooltip: "Make current line ingredient", command: makeIngredient},
            {label: "Substitution", tooltip: "Make current line ingredient substitution", command: demoteSubstitution},
        ]
    },
    {
        showGroup: inInstructionNode,
        commands: [
            {label: "Header", tooltip: "Make current line header", command: makeInstructionHeader},
            {label: "Instruction", tooltip: "Make current line instruction", command: makeInstruction},
        ]
    },
    {
        showGroup: inIngredientNode,
        commands: [
            {label: "Unit", tooltip: "Mark selection as unit", command: markUnit},
            {label: "Ingredient", tooltip: "Mark selection as an ingredient", command: markIngredient},
            {label: "Clear", tooltip: "Clear annotation of selection", command: clearMarks},
        ]
    },
    {
        showGroup: inVariationNode,
        commands: [
            {label: "Insert Variation", tooltip: "Insert new variation", command: insertVariation},
        ]
    },
    {
        showGroup: inYieldNode,
        commands: [
            {label: "Unit", tooltip: "Mark selection as unit", command: markUnit},
            {label: "Clear", tooltip: "Clear annotation of selection", command: clearMarks},
        ],
    },
    {
        commands: [
            {
                command: chainCommands(insertIngredient, insertInstruction, moveFromVariationNameToContent, splitListItem(ingredientSchema.nodes.listItem)),
                shortcuts: ["Enter"]
            },
            {
                command: chainCommands(insertHardBreak, insertIngredientHeader, insertInstructionHeader, insertVariation),
                shortcuts: ["Ctrl-Enter"]
            },
            {
                command: chainCommands(insertIngredientHeader, insertInstructionHeader, insertVariation),
                shortcuts: ["Shift-Enter"]
            },
            {
                command: chainCommands(demoteSubstitution, tabToNode(1)),
                shortcuts: ["Tab"]
            },
            {
                command: chainCommands(promoteSubstitution, tabToNode(-1)),
                shortcuts: ["Shift-Tab"]
            },
            {
                command: chainCommands(
                    deleteSelection,
                    mergeSubBackwardsToIngredient,
                    mergeIngredientBackwardsToHeader,
                    mergeHeaderBackwardsToIngredient,
                    mergeInstructionBackwardsToHeader,
                    mergeHeaderBackwardsToInstruction,
                    mergeVariationBackwardsToName,
                    mergeNameBackwardsToVariation,
                    joinBackward,
                ),
                shortcuts: ["Backspace"]
            }
        ]
    }
];

let ingredientEditorPlugin = new Plugin({
    state: {
        init(_, {doc}) { return updateRecipeObj(doc) },
        apply(tr, old) { return tr.docChanged ? updateRecipeObj(tr.doc) : old }
    },
    props: {
        decorations(state) { return this.getState(state) },
        handlePaste: handleIngredientPaste,
    },
    view(editorView) {
        this.toolbarView = new ingredientInputToolbarView(commandGroups, this, editorView);
        return this.toolbarView;
    },
    appendTransaction(transactions, oldState, newState) {
        let durations_changed = false;
        let total_duration_pos = 0;
        let total_duration_node = undefined;
        newState.doc.descendants(function(child_node, pos, parent) {
            if ((child_node.type == ingredientSchema.nodes.recipeHeader) ||
                (child_node.type == ingredientSchema.nodes.recipePrepTimeContainer) ||
                (child_node.type == ingredientSchema.nodes.recipeCookTimeContainer)) {
                return true;
            } else if (child_node.type == ingredientSchema.nodes.recipePrepTime) {
                let prep_times = parseDuration(child_node);
                if (prep_times === undefined) {
                    durations_changed = durations_changed || (recipe.prep_duration_min !== undefined);
                    recipe.prep_duration_min = undefined;
                    durations_changed = durations_changed || (recipe.prep_duration_max !== undefined);
                    recipe.prep_duration_max = undefined;
                } else {
                    durations_changed = durations_changed || (recipe.prep_duration_min != prep_times[0]) || (recipe.prep_duration_max != prep_times[0]);
                    recipe.prep_duration_min = prep_times[0];
                    recipe.prep_duration_max = prep_times[1];
                }
                return false;
            } else if (child_node.type == ingredientSchema.nodes.recipeCookTime) {
                let cook_times = parseDuration(child_node);
                if (cook_times === undefined) {
                    durations_changed = durations_changed || (recipe.cook_duration_min !== undefined);
                    recipe.cook_duration_min = undefined;
                    durations_changed = durations_changed || (recipe.cook_duration_max !== undefined);
                    recipe.cook_duration_max = undefined;
                } else {
                    durations_changed = durations_changed || (recipe.cook_duration_min != cook_times[0]) || (recipe.cook_duration_max != cook_times[1]);
                    recipe.cook_duration_min = cook_times[0];
                    recipe.cook_duration_max = cook_times[1];
                }
                return false;
            } else if (child_node.type == ingredientSchema.nodes.recipeTotalTime) {
                total_duration_pos = pos;
                total_duration_node = child_node;
                return false;
            } else {
                return false;
            }
        });

        if (durations_changed) {
            total_duration_text = format_duration_range(
                ((recipe.prep_duration_min ? recipe.prep_duration_min : 0) + (recipe.cook_duration_min ? recipe.cook_duration_min : 0)),
                ((recipe.prep_duration_max ? recipe.prep_duration_max : (recipe.prep_duration_min ? recipe.prep_duration_min : 0)) +
                 (recipe.cook_duration_max ? recipe.cook_duration_max : (recipe.cook_duration_min ? recipe.cook_duration_min : 0))));
            let tr = newState.tr;
            tr = tr.replaceWith(total_duration_pos + 1, total_duration_pos + total_duration_node.nodeSize - 1, total_duration_text.length > 0 ? ingredientSchema.text(total_duration_text) : null);
            return tr;
        }
        return undefined;
    },
});

let ingredientEditorDropdownPlugin = editorDropdown([
    {
        nodeName: "recipeSource",
        title: "Source",
        labelField: "name",
        idField: "id",
        editFields: [
            {field: "name", label: "Name"},
            {field: "url", label: "URL"},
        ],
        searchFields: ["name", "url"],
        renderItem(item) {return item.name ? item.name : item.url},
        formatText(item) {return item.name ? item.name : item.url},
        detailsLink: item => "/source/" + item.id,
        items: sources,
        updateItem(orig_item, new_item) { $.extend(true, orig_item, new_item) },
        itemMatchesString(item, str) {
            return (((item.name.length > 0) && (item.name.localeCompare(str.trim(), undefined, {sensitivity: 'accent'}) == 0)) ||
                   ((item.url.length > 0) && (item.url == str.trim())));
        },
        retireItem(item, doc) {
            // If this is not a new item, then just return, we never remove these items
            if (item.id >= 0) return

            // Remove the node if there's less than two references to it in the doc
            if (nodesWithAttr(ingredientSchema.nodes.recipeSource, "item", item, doc) < 2) {
                let item_index = sources.findIndex(i => item == i);
                if (item_index >= 0) sources.splice(item_index, 1);
            }
        },
        addItem(text){
            if (text.trim().length == 0) return null;
            let isURL = text.match(/((([A-Za-z]{3,9}:(?:\/\/)?)(?:[\-;:&=\+\$,\w]+@)?[A-Za-z0-9\.\-]+|(?:www\.|[\-;:&=\+\$,\w]+@)[A-Za-z0-9\.\-]+)((?:\/[\+~%\/\.\w\-_]*)?\??(?:[\-\+=&;%@\.\w_]*)#?(?:[\.\!\/\\\w]*))?)/);
            item = {
                id: getUnusedId(sources),
                name: isURL ? "" : text.trim(),
                url: isURL ? text.trim() : "",
            };
            sources.push(item);
            return item;
        }
    },
    {
        nodeName: "newRecipeTag",
        title: "Tag",
        labelField: "name",
        idField: "id",
        editFields: [
            {field: "name", label: "Name"},
        ],
        searchFields: ["name"],
        detailsLink: item => "/tags/recipe/" + item.id,
        items: recipe_tags,
        updateItem(orig_item, new_item) { $.extend(true, orig_item, new_item) },
        updateNode(pos, node, item, view, tr) {
            if (recipe_tags.find(i => i == item) == undefined) recipe_tags.push(item);
            tr = tr.insert(tr.mapping.map(pos)-1, ingredientSchema.node(ingredientSchema.nodes.recipeTag, {item: item}));
            tr = tr.delete(tr.mapping.map(pos), tr.mapping.map(pos + node.nodeSize)-2);
            tr = tr.setSelection(TextSelection.create(tr.doc, tr.mapping.map(pos)));
            return tr;
        },
        itemMatchesString(item, str) {
            return item.name.localeCompare(str.trim(), undefined, {sensitivity: 'accent'}) == 0;
        },
        retireItem(item, doc) {
            // If this is not a new item, then just return, we never remove these items
            if (item.id >= 0) return

            // Remove the node if there's less than two references to it in the doc
            if (nodesWithAttr(ingredientSchema.nodes.recipeTag, "item", item, doc) == 0) {
                let item_index = recipe_tags.findIndex(i => item == i);
                if (item_index >= 0) recipe_tags.splice(item_index, 1);
            }
        },
        addItem(text){
            if (text.length == 0) return null;
            item = {
                id: getUnusedId(recipe_tags),
                name: text,
            };
            recipe_tags.push(item);
            return item;
        }
    },
    {
        nodeName: "recipeCategory",
        title: "Category",
        labelField: "name",
        idField: "id",
        editFields: [
            {field: "name", label: "Name"},
            //{field: "abbrev", label: "Abbreviation"},
        ],
        searchFields: ["name"],
        detailsLink: item => "/category/" + item.id,
        items: categories,
        updateItem(orig_item, new_item) { $.extend(true, orig_item, new_item) },
        updateNode(pos, node, item, view, tr) {
            if (node.attrs.item != item) {
                let resolved_pos = view.state.doc.resolve(pos)
                let subcategory_node_pos = resolved_pos.posAtIndex(2, 2)+1;
                let subcategory_node = view.state.doc.resolve(subcategory_node_pos).node();
                tr = tr.replaceWith(tr.mapping.map(subcategory_node_pos), tr.mapping.map(subcategory_node_pos + subcategory_node.nodeSize),
                                    ingredientSchema.node(ingredientSchema.nodes.recipeSubcategory, {item: null}, null));
            }
            tr.replaceWith(tr.mapping.map(pos), tr.mapping.map(pos + node.nodeSize),
                           ingredientSchema.node(ingredientSchema.nodes.recipeCategory, {item: item}, ingredientSchema.text(item.name)));
            return tr;
        },
        nodeItemUpdated(pos, node, item, tr) {
            if (node.attrs.item == null) return tr;
            let resolved_pos = tr.doc.resolve(pos)
            let subcategory_node_pos = resolved_pos.posAtIndex(2, 2)+1;
            let subcategory_node = tr.doc.resolve(subcategory_node_pos).node();
            tr = tr.replaceWith(tr.mapping.map(subcategory_node_pos), tr.mapping.map(subcategory_node_pos + subcategory_node.nodeSize),
                                ingredientSchema.node(ingredientSchema.nodes.recipeSubcategory, {item: null}, null));
            return tr;
        },
        itemMatchesString(item, str) { return item.name.localeCompare(str.trim(), undefined, {sensitivity: 'accent'}) == 0; },
        retireItem(item, doc) {
            // If this is not a new item, then just return, we never remove these items
            if (item.id >= 0) return

            // Remove the node if there's less than two references to it in the doc
            if (nodesWithAttr(ingredientSchema.nodes.recipeCategory, "item", item, doc) < 2) {
                let item_index = categories.findIndex(i => item == i);
                if (item_index >= 0) categories.splice(item_index, 1);
            }
        },
        addItem(text){
            if (text.trim().length == 0) return null;
            item = {
                id: getUnusedId(categories),
                name: text.trim(),
                subcategories: [],
            };
            categories.push(item);
            return item;
        }
    },
    {
        nodeName: "recipeSubcategory",
        title: "Subcategory",
        labelField: "name",
        idField: "id",
        editFields: [
            {field: "name", label: "Name"},
        ],
        searchFields: ["name"],
        //detailsLink: item => "/subcategory/" + item.id,
        updateItemList() {
            let cat_obj = categories.find(cat => cat.id == recipe.category.id);
            return (cat_obj == undefined) ? [] : cat_obj.subcategories;
        },
        updateItem(orig_item, new_item) { $.extend(true, orig_item, new_item) },
        itemMatchesString(item, str) { return item.name.localeCompare(str.trim(), undefined, {sensitivity: 'accent'}) == 0; },
        retireItem(item, doc) {
            // If this is not a new item, then just return, we never remove these items
            if (item.id >= 0) return

            // Remove the node if there's less than two references to it in the doc
            if (nodesWithAttr(ingredientSchema.nodes.recipeSubategory, "item", item, doc) < 2) {
                let selected_category = categories.find(cat => cat.id == recipe.category.id);
                if (selected_category != undefined) {
                    let item_index = selected_category.subcategories.findIndex(i => item == i);
                    if (item_index >= 0) selected_category.subcategories.splice(item_index, 1);
                }
            }
        },
        addItem(text){
            // If the text is then don't create a new item;
            if (text.length == 0) return null;

            let selected_category = categories.find(cat => cat.id == recipe.category.id);
            item = {
                id: (selected_category != undefined) ? getUnusedId(selected_category.subcategories) : -1,
                name: text.trim(),
            };
            if (selected_category != undefined) selected_category.subcategories.push(item);
            return item;
        }
    },
    {
        markName: "unitName",
        title: "Unit",
        labelField: "name",
        idField: "id",
        editFields: [
            {field: "name", label: "Name"},
            {field: "plural", label: "Plural Name"},
            {field: "abbrev", label: "Abbreviation"},
        ],
        searchFields: ["name", "plural", "abbrev"],
        renderItem(item) {return item.name + (item.abbrev ? " (" + item.abbrev + ".)" : "")},
        formatText(item) {return item.abbrev ? (item.abbrev + ".") : item.name},
        detailsLink: item => "/units/" + item.id,
        items: units,
        updateItem(orig_item, new_item) { $.extend(true, orig_item, new_item) },
        itemMatchesString: unitItemMatchesString,
        retireItem(item, doc) {
            // If this is not a new item, then just return, we never remove these items
            if (item.id >= 0) return

            // Remove the node if there's less than two references to it in the doc
            if (marksWithAttr(ingredientSchema.marks.unitName, "item", item, doc) < 2) {
                let item_index = units.findIndex(i => item == i);
                if (item_index >= 0) units.splice(item_index, 1);
            }
        },
        addItem(text){
            item = {
                id: getUnusedId(units),
                name: text.trim(),
                abbrev: null,
                plural: null,

            };
            units.push(item);
            return item;
        }
    },
    {
        markName: "ingredientName",
        title: "Ingredient",
        labelField: "name",
        editFields: [
            {field: "name", label: "Name"},
            {field: "plural", label: "Plural Name"},
        ],
        searchFields: ["name", "plural"],
        renderItem(item) {return item.name ? item.name : item.plural},
        formatText(item) {return item.name ? item.name : item.plural},
        idField: "id",
        detailsLink: item =>  "/ingredient/" + item.id + "/" + item.slug,
        items: ingredients,
        updateItem(orig_item, new_item) { $.extend(true, orig_item, new_item) },
        itemMatchesString: ingredientItemMatchesString,
        retireItem(item, doc) {
            // If this is not a new item, then just return, we never remove these items
            if (item.id >= 0) return

            // Remove the node if there's less than two references to it in the doc
            if (marksWithAttr(ingredientSchema.marks.ingredientName, "item", item, doc) < 2) {
                let item_index = ingredients.findIndex(i => item == i);
                if (item_index >= 0) ingredients.splice(item_index, 1);
            }
        },
        addItem(text){
            item = {
                id: getUnusedId(ingredients),
                name: text.trim(),
                plural: null,
            };
            ingredients.push(item);
            return item;
        }
    }
]);

let linkEditorPlugin = markAttrEditor([{
    markName: "link",
    editAttrs: [
        {
            attr: "href",
            label: "URL",
            previewNode(attr_value) {
                let node = document.createElement("a");
                node.href = attr_value;
                node.textContent = attr_value;
                return node;
            }
        }
    ],
    validate(attrs) {
        if (attrs.href.match(/^\w+:\/\/.*/) == null) {
            attrs.href = "https://" + attrs.href;
        }
        return attrs;
    }
}]);

let editorPlaceholderPlugin = editorPlaceholder({
    recipeTitle: {textContent: "Add Recipe Name"},
    recipeCategory: {textContent: "Add Category"},
    recipeSubcategory: {textContent: "Add Subcategory"},
    recipeSource: {textContent: "Add Recipe Source"},
    newRecipeTag: {
        textContent: "Add Tags",
        extraCondition(node, pos, parent) {
            return node == parent.child(parent.childCount - 1);
        }
    },
    recipeYields: {textContent: "Add Recipe Yield"},
    recipePrepTime: {textContent: "Add Prep Time"},
    recipeCookTime: {textContent: "Add Cook Time"},
    recipeDescription: {textContent: "Add Description"},
    recipeIngredientSectionHeader: {textContent: "Add Section Header"},
    recipeInstructionSectionHeader: {textContent: "Add Section Header"},
    recipeVariationName: {textContent: "Add Variation Name"},
    recipeNotes: {textContent: "Add Notes"},
});

let ingredientEditorKeymap = keymap(commandGroups.reduce((shortcutMap, group) => {
    return group.commands.reduce((shortcutMap, cmdSpec) => {
        if ('shortcuts' in cmdSpec)  cmdSpec.shortcuts.forEach(shortcut => {
            if (shortcut in shortcutMap) {
                shortcutMap[shortcut] = chainCommands(shortcutMap[shortcut], cmdSpec.command);
            } else {
                shortcutMap[shortcut] = cmdSpec.command;
            }
        });
        return shortcutMap;
    }, shortcutMap);
}, {}));

let container = $("#recipe-edit-container")[0]
let state = EditorState.create({
  //doc: ingredientSchema.node("doc", null, recipe.ingredient_sections.map(section => formatIngredientSection(section))),
  doc: DOMParser.fromSchema(ingredientSchema).parse(container),
  plugins: [
    editorPlaceholderPlugin,
    ingredientEditorDropdownPlugin,
    recipeEditorSaveButtonPlugin,
    ingredientEditorPlugin,
    linkEditorPlugin,
    ingredientEditorKeymap,
    keymap(baseKeymap),
    history()
  ]
})
container.querySelectorAll('*').forEach(n => n.remove());
window.view = new EditorView(container, {state})
