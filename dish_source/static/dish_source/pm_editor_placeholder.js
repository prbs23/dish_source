const {DOMSerializer} = require("prosemirror-model")

function editorPlaceholder(placeholder_specs) {
    return new Plugin({
        props: {
            decorations(state) {
                let decos = [];
                let head = state.selection.head;
                state.doc.descendants(function(node, pos, parent) {
                    if (!(node.type.name in placeholder_specs)) return true; // If node is not in placeholder_specs then we don't ever need to decorate it
                    if ((head >= pos) && (head <= (pos + node.nodeSize))) return false;
                    if  (!((node.content.size == 0) ||
                           ((node.childCount == 1) && (node.content.content[0].isText ?
                                                       node.content.content[0].text.length == 0 :
                                                       node.content.content[0].content.size == 0)))) return false; // Don't decorate a node that isn't empty
                    if (placeholder_specs[node.type.name].extraCondition != undefined && !placeholder_specs[node.type.name].extraCondition(node, pos, parent)) return false;
                    decos.push(Decoration.widget(pos+1, function(view, getPos) {
                        let dom_node = document.createElement("span");
                        dom_node.textContent = placeholder_specs[node.type.name].textContent;
                        dom_node.className = "placeholder";
                        return dom_node;
                    }, {side: +1}));
                    return false;
                });
                return DecorationSet.create(state.doc, decos);
            },
        },
    });
};

