//const {Plugin} = require("prosemirror-state")
//const {Mark} = require("prosemirror-model")


class MarkAttrView {
    constructor(editorSpec, plugin, editorView) {
        // Define instance variables
        this.spec = editorSpec;
        this.visible = false;
        this.current_spec = null;
        this.current_mark = undefined;

        // Create editor DOM
        this.plugin = plugin;
        this.editorView = editorView;
        this.dom = document.createElement("div");
        this.dom.className = "editor-dropdown editor-dropdown-edit-container bg-dark";
        this.dom.style.display = "none";
        this.dom.style.position = "absolute";
        this.dom.contentEditable = "false";
        this.dom.pm_mark_attr_view = this
        this.dom.addEventListener('keydown', function(e) {
            if (e.key == "Escape" || e.key == "Enter") {
                this.pm_mark_attr_view.hide(this.pm_mark_attr_view.editorView);
            }
        })
        editorView.dom.parentNode.appendChild(this.dom);
        document.addEventListener('mousedown', function(event) {
            if (this.visible && !(this.dom.contains(event.target) || this.editorView.dom.contains(event.target))) {
                this.hide();
            }
        }.bind(this));

        // Update the current editor to populate the dropdown if needed.
        this.update(editorView, null);
    }

    update(view, lastState) {
        let state = view.state
        // Don't do anything if the document/selection didn't change
        if (lastState && lastState.doc.eq(state.doc) &&
            lastState.selection.eq(state.selection)) return

        // Check if the dropdown needs to be re-rendered
        let $head = state.selection.$head;
        let marks = $head.marks();
        if (lastState == null || $head.node() != lastState.selection.$head.node() || !Mark.sameSet(marks, lastState.selection.$head.marks())) {
            let new_visible = (this.spec.find(spec => marks.map(mark => mark.type.name).includes(spec.markName)) != undefined);
            if (this.visible && !new_visible) {
                this.hide(view);
                return;
            }
            this.visible = new_visible;
        }

        // Only re-render dropdown if it's also visible
        if (this.visible) {
            // Figure out which dropdown type to render based on the marks of the current node.
            // Update the current selected node and spec fields based on our current mark type.
            this.current_mark = marks.find(mark => this.spec.map(spec => spec.markName).includes(mark.type.name), this);
            let spec = this.spec.find(spec => spec.markName == this.current_mark.type.name);

            if (spec !== this.current_spec) {
                // Re-render the editor if the mark spec had changed
                this.current_spec = spec;

                this.dom.querySelectorAll('*').forEach(n => n.remove());
                this.current_spec.editAttrs.forEach(function(attrSpec) {
                    let dom_node = document.createElement("div");
                    dom_node.className = "editor-dropdown-edit-item input-group input-group-sm mb-1";
                    let label = document.createElement("span");
                    label.className = "input-group-text";
                    label.textContent = attrSpec.label;
                    let label_container = document.createElement("div");
                    label_container.className = "input-group-prepend";
                    label_container.append(label);
                    let input = document.createElement("input");
                    input.className = "form-control";
                    input.setAttribute("type", "text");
                    input.value = this.current_mark.attrs[attrSpec.attr];
                    input.pm_mark_attr_view = this;
                    input.pm_mark_attr_spec = attrSpec;
                    input.addEventListener("input", function(event) {
                        this.pm_mark_attr_view.updateCurrentMarkAttr(this.pm_mark_attr_spec, this.value);
                    });
                    dom_node.append(label_container);
                    dom_node.append(input);
                    this.dom.append(dom_node);
                    if ('previewNode' in attrSpec) {
                        let preview_node = document.createElement("div");
                        preview_node.className = "attr-preview";
                        preview_node.pm_mark_attr_spec = attrSpec;
                        preview_node.append(attrSpec.previewNode(this.current_mark.attrs[attrSpec.attr]));
                        this.dom.append(preview_node);
                    }
                }, this);
                this.dom.style.width = "300px";
            }
        }
        this.updateView(view);
    }

    updateView(view) {
        if (this.visible) {
            let $head = view.state.selection.$head;

            // Update the editor from the document state
            this.dom.querySelectorAll("input").forEach(input => {
                if (!("pm_mark_attr_spec" in input)) return;
                input.value = this.current_mark.attrs[input.pm_mark_attr_spec.attr];
            })
            this.dom.querySelectorAll(".attr-preview").forEach(preview_node => {
                if (!("pm_mark_attr_spec" in preview_node)) return;
                preview_node.querySelectorAll('*').forEach(n => n.remove());
                preview_node.append(preview_node.pm_mark_attr_spec.previewNode(this.current_mark.attrs[preview_node.pm_mark_attr_spec.attr]));
            })

            this.dom.style.display = "";
            let mark_start = view.coordsAtPos(getMarkRange(this.current_mark, this.editorView.state.doc).from, -1);
            let container_box = this.dom.offsetParent.getBoundingClientRect()
            let dropdown_box = this.dom.getBoundingClientRect()
            this.dom.style.left = (mark_start.left - container_box.left) + "px";
            this.dom.style.top = (mark_start.bottom - container_box.top) + "px";
            this.dom.scrollIntoView({behavior: "auto", block: "nearest"});
        } else {
            this.dom.style.display = "none";
        }
    }

    updateCurrentMarkAttr(attrSpec, attr_value) {
        this.updateCurrentMark(Object.assign({}, this.current_mark.attrs, {[attrSpec.attr]: attr_value}));
    }

    updateCurrentMark(attrs) {
        // Replace the mark
        let {from, to, found} = getMarkRange(this.current_mark, this.editorView.state.doc);
        if (found) {
            let new_mark = this.editorView.state.schema.mark(this.current_mark.type, attrs);
            this.editorView.dispatch(this.editorView.state.tr.removeMark(from, to, this.current_mark).addMark(from, to, new_mark));
        }
    }

    hide(view) {
        this.visible = false;
        this.updateView(view);
        this.updateCurrentMark(this.current_spec.validate(Object.assign({}, this.current_mark.attrs)));
    }

    destroy() {this.dom.remove()}
}

function markAttrEditor(editorSpec) {
    return new Plugin({
        props: {
            handleDOMEvents: {
                focus(view, e) {
                    this.spec.view_obj.update(view, null);
                    return false;
                }
            },
            handleKeyDown(view, e) {
                if (this.spec.view_obj.visible == false) return false;
                if (e.key == "Escape" || e.key == "Enter") this.spec.view_obj.hide(view);
                else return false;
            }
        },
        view(EditorView) {
            this.view_obj = new MarkAttrView(editorSpec, this, EditorView);
            return this.view_obj;
        },
    })
}
