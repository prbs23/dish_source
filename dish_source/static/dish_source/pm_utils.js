

// Find the start and end point of a mark (assumes that the mark is continuous)
function getMarkRange(mark, doc) {
    let from = -1;
    let to = -1;
    let done = false;
    doc.descendants((node, pos) => {
        if (done) return false;
        if (node.marks.find(m => m == mark)) {
            if (from == -1) from = pos;
            to = pos + node.nodeSize;
            return false;
        } else if (from > -1) {
            done = true;
        }
        return true;
    });
    return {from, to, found: to != -1};
}

// Find the number of nodes with the given attribute item
function nodesWithAttr(nodeType, attrName, attrValue, doc) {
    let node_count = 0;
    doc.descendants(function(node, pos, parent) {
        if (node.type == nodeType && node.attrs[attrName] == attrValue) {
            node_count += 1;
            return false;
        } else {
            return true;
        }
    });
    return node_count;
}


// Find the number of marks with the given attribute item
function marksWithAttr(markType, attrName, attrValue, doc) {
    let mark_count = 0;
    doc.descendants(function(node, pos, parent) {
        if (node.marks.find(mark => mark.type == markType && mark.attrs[attrName] == attrValue) != null) {
            mark_count += 1;
            return false;
        } else {
            return true;
        }
    });
    return mark_count;
}

