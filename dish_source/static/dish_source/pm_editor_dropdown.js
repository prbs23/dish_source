const {Plugin} = require("prosemirror-state")
const {Mark} = require("prosemirror-model")


class EditorDropdown {
    constructor(dropdown_specs, plugin, editorView) {
        // Define instance variables
        this.dropdown_specs = dropdown_specs;
        this.dropdown_specs.forEach(item => {
            if (item.updateItemList) {
                item.items = item.updateItemList();
            }
            item.sifter = new Sifter(item.items)
        });
        this.visible = false;
        this.current_spec = null;
        this.current_selection = null;
        this.current_applied = null;

        // Create dropdown DOM
        this.plugin = plugin;
        this.editorView = editorView;
        this.dom = document.createElement("div");
        this.dom.className = "editor-dropdown";
        this.dom.style.display = "none";
        this.dom.style.position = "absolute";
        this.dom.contentEditable = "false";
        this.dropdown_edit_container = document.createElement("div")
        this.dropdown_edit_container.className = "editor-dropdown-edit-container bg-dark";
        this.dropdown_edit_title = document.createElement("div");
        this.dropdown_edit_title.className = "editor-dropdown-edit-title";
        this.dropdown_edit_container.append(this.dropdown_edit_title);
        this.dropdown_edit = document.createElement("div");
        this.dropdown_edit.className = "editor-dropdown-edit-list";
        this.dropdown_edit_container.append(this.dropdown_edit);
        this.dropdown_edit_details_link = document.createElement("a");
        this.dropdown_edit_details_link.className = "text-primary";
        this.dropdown_edit_details_link.textContent = "Edit Details";
        this.dropdown_edit_details_link.href = "#";
        this.dropdown_edit_container.append(this.dropdown_edit_details_link);
        this.dropdown_edit_apply = document.createElement("button");
        this.dropdown_edit_apply.className = "btn btn-outline-primary btn-sm";
        this.dropdown_edit_apply.setAttribute("type", "button");
        this.dropdown_edit_apply.textContent = "Apply";
        this.dropdown_edit_apply.pm_dropdown_parent = this;
        this.dropdown_edit_apply.addEventListener('click', function() {
            this.pm_dropdown_parent.applyEdits(this.pm_dropdown_parent.editorView);
        })
        this.dropdown_edit_container.append(this.dropdown_edit_apply);
        this.dom.append(this.dropdown_edit_container);
        this.dropdown_list = document.createElement("div");
        this.dropdown_list.className = "editor-dropdown-list";
        this.dom.append(this.dropdown_list)
        editorView.dom.parentNode.appendChild(this.dom);
        document.addEventListener('mousedown', function(event) {
            if (this.visible && !(this.dom.contains(event.target) || this.editorView.dom.contains(event.target))) {
                this.hide();
            }
        }.bind(this));

        // Update the current editor to populate the dropdown if needed.
        let update_tr = EditorDropdown.updateAttributes(editorView.state, dropdown_specs);
        if (update_tr) editorView.dispatch(update_tr);
        this.update(editorView, null);
    }

    static updateAttributes(state, dropdown_specs) {
        // Update marks/nodes to ensure items match
        let tr = state.tr;
        state.doc.descendants(function(node, pos, parent) {
            let mark = undefined;

            // Check if there is a spec that matches this node
            let spec = dropdown_specs.find(spec => {
                if (spec.markName == undefined) {
                    return node.type.name == spec.nodeName;
                } else {
                    mark = node.marks.find(mark => mark.type.name == spec.markName);
                    return mark != null;
                }
            });
            if (spec == null) return true;

            // Find the range, text string, and item for the node or mark
            let cur_item = undefined;
            let text = node.textContent;
            if (spec.markName == undefined) {
                cur_item = node.attrs.item;
            } else {
                cur_item = mark.attrs.item;
            }

            // If the node/mark text matches the item, then we don't need to do anything, and don't continue iterating
            // into this node.
            let new_item = spec.items.find(item => spec.itemMatchesString(item, text));
            if (cur_item != null && cur_item == new_item) return true;


            // If we couldn't find a matching item, then retire this item and create a new one
            if (cur_item != null) spec.retireItem(cur_item, tr.doc);
            if (new_item == null) {
                new_item = spec.addItem(text);
                if (new_item != null && !spec.itemMatchesString(new_item, text)) {
                    throw "Error, new item doesn't match string";
                }
            }

            // if new_item is still null and cur_item was also null then don't update the node
            if (cur_item == null && new_item == null) return true;

            // Add transaction to update the node attributes to point to current item
            if (spec.markName == undefined) {
                tr = tr.setNodeMarkup(tr.mapping.map(pos), undefined, Object.assign({}, node.attrs, {item: new_item}));
                if (spec.nodeItemUpdated) {
                    tr = spec.nodeItemUpdated(pos, node, new_item, tr)
                }
            } else {
                tr = tr.removeMark(tr.mapping.map(pos), tr.mapping.map(pos + node.nodeSize), mark).addMark(tr.mapping.map(pos), tr.mapping.map(pos + node.nodeSize), mark.type.create(
                    Object.assign({}, mark.attrs, {item: new_item})
                ));
                if (spec.nodeItemUpdated) {
                    tr = spec.nodeItemUpdated(pos, node, new_item, tr)
                }
            }

            // This node is handled, don't descend any deeper
            return true;
        });
        if (tr.docChanged) {
            return tr;
        } else {
            return undefined;
        }
    }

    update(view, lastState) {
        let state = view.state
        // Don't do anything if the document/selection didn't change
        if (lastState && lastState.doc.eq(state.doc) &&
            lastState.selection.eq(state.selection)) return

        // Check if the dropdown needs to be re-rendered
        let $head = state.selection.$head;
        let marks = $head.marks();
        let node = $head.node();
        if (lastState == null || $head.node() != lastState.selection.$head.node() || !Mark.sameSet(marks, lastState.selection.$head.marks())) {
            this.visible = (this.dropdown_specs.find(spec => {
                return (spec.markName !== undefined) ?
                   marks.map(mark => mark.type.name).includes(spec.markName) :
                   node.type.name == spec.nodeName
                }) != undefined);
        }

        // Only re-render dropdown if it's also visible
        if (this.visible) {
            // Figure out which dropdown type to render based on the marks of the current node.
            // Update the current selected node and spec fields based on our current mark type.
            let mark = marks.find(mark => this.dropdown_specs.map(spec => spec.markName).includes(mark.type.name), this);
            let text = "";
            if (mark != null) {
                this.current_spec = this.dropdown_specs.find(spec => spec.markName == mark.type.name);
                text = $head.node().nodeAt($head.parentOffset > 0 ? $head.parentOffset-1 : 0).textContent;
            } else {
                this.current_spec = this.dropdown_specs.find(spec => spec.nodeName == node.type.name);
                text = node.textContent;
            }

            if (this.current_spec.updateItemList) {
                this.current_spec.items = this.current_spec.updateItemList();
                this.current_spec.sifter = new Sifter(this.current_spec.items)
            }

            // Filter the item list based on the text currently in the node.
            let current_items = this.current_spec.sifter.search(text, {
                fields: ("searchFields" in this.current_spec ? this.current_spec.searchFields : this.current_spec.labelField),
                sort: [{field: this.current_spec.labelField, direction: 'asc'}],
                filter: false,
            }).items.map(item => this.current_spec.items[item[this.current_spec.idField]]);

            // Find the currently applied item index in the current_items, and set the selection to match it.
            let attr_item = undefined;
            if (mark != null) {
                attr_item = mark.attrs.item;
            } else {
                attr_item = node.attrs.item;
            }
            if (attr_item != null) {
                this.current_selection = this.current_applied = current_items.findIndex(item => item[this.current_spec.idField] == attr_item[this.current_spec.idField]);
            } else {
                this.current_selection = 0;
                this.current_applied = undefined;
            }

            this.dropdown_edit_title.textContent = this.current_spec.title;

            // Re-render the dropdown item list.
            //   First, remove all the current items in the dropdown list
            //   Then, iterate through the visible items list creating a new div for each item.
            this.dropdown_list.querySelectorAll('*').forEach(n => n.remove());
            current_items.forEach(function(item, index) {
                // Create the list item div
                let dom_node = document.createElement("div");
                dom_node.pm_dropdown_parent = this;
                dom_node.pm_dropdown_item = item;
                dom_node.className = "editor-dropdown-item";
                if ("renderItem" in this.current_spec) {
                    dom_node.innerHTML = this.current_spec.renderItem(item);
                } else {
                    dom_node.innerHTML = "<span>" + item[this.current_spec.labelField] + "</span>";
                }
                if (item[this.current_spec.idField] < 0) {
                    dom_node.className += " editor-dropdown-item-new";
                    let new_tag = document.createElement("span");
                    new_tag.className = "badge badge-warning ml-2";
                    new_tag.textContent = "New";
                    dom_node.append(new_tag);
                }
                if (index == this.current_selection) dom_node.className += " editor-dropdown-item-selected";
                if (index == this.current_applied) dom_node.className += " editor-dropdown-item-applied";

                // Attach listeners to each list item node to update the state on a mouseover and click.
                dom_node.addEventListener('mouseover', function() {
                    this.pm_dropdown_parent.current_selection = index;
                    this.pm_dropdown_parent.updateView(this.pm_dropdown_parent.editorView);
                })
                dom_node.addEventListener('click', function() {
                    this.pm_dropdown_parent.current_selection = index;
                    this.pm_dropdown_parent.applySelection(this.pm_dropdown_parent.editorView, false);
                })

                // Append the list item to the dropdown list.
                this.dropdown_list.append(dom_node);
            }, this);

            // Re-render the dropdown edit container
            this.dropdown_edit.querySelectorAll('*').forEach(n => n.remove());
            this.current_spec.editFields.forEach(function(fieldSpec) {
                let dom_node = document.createElement("div");
                dom_node.className = "editor-dropdown-edit-item input-group input-group-sm mb-1";
                let label = document.createElement("span");
                label.className = "input-group-text";
                label.textContent = fieldSpec.label;
                let label_container = document.createElement("div");
                label_container.className = "input-group-prepend";
                label_container.append(label);
                let input = document.createElement("input");
                input.className = "form-control";
                input.setAttribute("type", "text");
                if (attr_item == undefined) {
                    input.disabled = true;
                } else {
                    input.value = attr_item[fieldSpec.field];
                }
                dom_node.append(label_container);
                dom_node.append(input);
                this.dropdown_edit.append(dom_node);
            }, this);
            if (this.current_spec.detailsLink != null) {
                this.dropdown_edit_details_link.style.display = "inline";
                if (attr_item == null || attr_item[this.current_spec.idField] < 0) {
                    this.dropdown_edit_details_link.href = "";
                } else {
                    this.dropdown_edit_details_link.href = this.current_spec.detailsLink(attr_item);
                }
            } else {
                this.dropdown_edit_details_link.style.display = "none";
            }

        }
        this.updateView(view);
    }

    updateView(view) {
        if (this.visible) {
            let $head = view.state.selection.$head;
            let mark = $head.marks().find(item => item.type.name == this.current_spec.markName);
            let node = $head.node();

            this.dom.style.display = "";
            let selected_node = null;
            this.dropdown_list.childNodes.forEach(function(item, index) {
                item.className = "editor-dropdown-item";
                if (item.pm_dropdown_item[this.current_spec.idField] < 0) item.className += " editor-dropdown-item-new";
                if (index == this.current_selection) {
                    item.className += " editor-dropdown-item-selected";
                    selected_node = item;
                }
                let attr_item = (this.current_spec.markName == null ? node.attrs : mark.attrs).item;
                if ((attr_item != null) && (item.pm_dropdown_item[this.current_spec.idField] == attr_item[this.current_spec.idField])) item.className += " editor-dropdown-item-applied";
            }, this);
            let left, bottom;
            if (this.current_spec.markName == null) {
                let node_dom = view.nodeDOM($head.before());
                let rect = node_dom.getBoundingClientRect();
                left = rect.left;
                bottom = rect.top + Number(window.getComputedStyle(node_dom).lineHeight.split("px")[0]);
            } else {
                let rect = view.coordsAtPos($head.posAtIndex($head.index() + ($head.textOffset == 0 ? -1 : 0)), 1);
                left = rect.left;
                bottom = rect.bottom;
            }
            let container_box = this.dom.offsetParent.getBoundingClientRect()
            let dropdown_box = this.dom.getBoundingClientRect()
            this.dom.style.left = (left - container_box.left) + "px";
            this.dom.style.top = (bottom - container_box.top) + "px";
            this.dom.style.maxWidth = (document.body.clientWidth - 10 - left) + "px";
            this.dom.scrollIntoView({behavior: "auto", block: "nearest"});
            if (selected_node) selected_node.scrollIntoView({behavior: "auto", block: "nearest"});
        } else {
            this.dom.style.display = "none";
        }
    }

    moveSelection(view, change) {
        let new_selected = this.current_selection + change;
        if (new_selected < 0) new_selected = 0;
        if (new_selected >= this.dropdown_list.children.length)
            new_selected = this.dropdown_list.children.length - 1;
        this.current_selection = new_selected;
        this.updateView(view);
    }

    applySelection(view, hide = false) {
        let text = null;
        if ('formatText' in this.current_spec) {
            text = this.current_spec.formatText(this.dropdown_list.children[this.current_selection].pm_dropdown_item);
        } else {
            text = this.dropdown_list.children[this.current_selection].pm_dropdown_item[this.current_spec.labelField];
        }

        let {$head} = view.state.selection;
        let tr = view.state.tr;
        if (this.current_spec.markName != null) {
            let mark = $head.marks().find(mark => mark.type.name == this.current_spec.markName, this);
            let node = $head.node().nodeAt($head.parentOffset);
            if (node == null || !node.marks.includes(mark)) {
                node = $head.node().nodeAt($head.parentOffset-1)
            }
            this.current_spec.retireItem(mark.attrs.item, view.state.doc);
            if (this.current_spec.updateNode != undefined) {
                tr = this.current_spec.updateNode($head.textOffset == 0 ? $head.pos-node.nodeSize : $head.posAtIndex($head.index()),
                                                  node, this.dropdown_list.children[this.current_selection].pm_dropdown_item,
                                                  view, tr);
            } else {
                mark.attrs.item = this.dropdown_list.children[this.current_selection].pm_dropdown_item;
                tr = tr.replaceWith(
                    $head.textOffset == 0 ? $head.pos-node.nodeSize : $head.posAtIndex($head.index()),
                    $head.posAtIndex($head.indexAfter()),
                    $head.doc.type.schema.text(text, $head.marks())
                );
            }
        } else {
            let node = $head.node();
            if (node.attrs.item != null) this.current_spec.retireItem(node.attrs.item, view.state.doc);
            if (this.current_spec.updateNode != undefined) {
                tr = this.current_spec.updateNode($head.start(), node, this.dropdown_list.children[this.current_selection].pm_dropdown_item, view, tr);
            } else {
                let newNode = node.copy(Fragment.from($head.doc.type.schema.text(text)));
                newNode.attrs.item = this.dropdown_list.children[this.current_selection].pm_dropdown_item;
                tr.replaceWith($head.start(), $head.end(), newNode);
            }
        }
        view.dispatch(tr);
        if (hide) this.hide(view);
    }

    applyEdits(view) {
        let applied_item = this.dropdown_list.children[this.current_applied].pm_dropdown_item;
        let copy_item = $.extend(true, {}, applied_item);

        // Update the model item
        this.dropdown_edit.childNodes.forEach(function(dom_item, index) {
            copy_item[this.current_spec.editFields[index].field] = dom_item.childNodes[1].value;
        }, this);
        this.current_spec.updateItem(applied_item, copy_item);

        // Update all nodes that reference this item
        let tr = view.state.tr;
        view.state.doc.descendants(function(child_node, pos, parent) {
            if (this.current_spec.markName != null) {
                if (!child_node.isText) return true
                let mark = child_node.marks.find(mark => mark.type.name == this.current_spec.markName, this);
                if (mark === undefined || mark.attrs.item[this.current_spec.idField] != applied_item[this.current_spec.idField]) return true;
                if (this.current_spec.updateNode != undefined) {
                    tr = this.current_spec.updateNode(pos, child_node, mark.attrs.item, view, tr);
                } else {
                    tr = tr.replaceWith(
                        tr.mapping.map(pos),
                        tr.mapping.map(pos + child_node.nodeSize),
                        view.state.doc.type.schema.text(this.current_spec.formatText ? this.current_spec.formatText(applied_item) : applied_item[this.current_spec.labelField], child_node.marks)
                    );
                }
            } else {
                if (this.current_spec.nodeName != child_node.type.name) return true;
                if (child_node.attrs.item == null || child_node.attrs.item[this.current_spec.idField] != applied_item[this.current_spec.idField]) return true;
                if (this.current_spec.updateNode != undefined) {
                    tr = this.current_spec.updateNode(pos, child_node, applied_item, view, tr);
                } else {
                    tr = tr.replaceWith(
                        tr.mapping.map(pos),
                        tr.mapping.map(pos + child_node.nodeSize),
                        views.state.doc.type.schema.text(this.current_spec.formatText ? this.current_spec.formatText(applied_item) : applied_item[this.current_spec.labelField], child_node.marks)
                    );
                }
                return false;
            }
        }.bind(this));
        view.dispatch(tr);
    }

    hide(view) {
        this.visible = false;
        this.updateView(view);
    }

    getInputTextRegExp(inputText) {
        let sanitized_text = inputText.replace(/[,\.'"'\s]+$/, "");
        sanitized_text = sanitized_text.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
        let search_str = "(?:" + sanitized_text + ")";
        for (var i=0; i < sanitized_text.length-1; i++) {
            if (sanitized_text[i] === " ") search_str += "|(?:" + sanitized_text.substr(i+1) + "$)";
        }
        return new RegExp(search_str, 'i');
    }

    destroy() {this.dom.remove()}
}

function editorDropdown(dropdown_specs) {
    let editor_plugin_state = {};
    return new Plugin({
        props: {
            handleDOMEvents: {
                focus(view, e) {
                    this.spec.view_obj.update(view, null);
                    return false;
                }
            },
            handleKeyDown(view, e) {
                if (this.spec.view_obj.visible == false) return false;

                if (e.key == "ArrowDown")    this.spec.view_obj.moveSelection(view, 1);
                else if (e.key == "ArrowUp") this.spec.view_obj.moveSelection(view, -1);
                else if (e.key == "Escape") this.spec.view_obj.hide(view);
                else if (e.key == "Enter") this.spec.view_obj.applySelection(view, true);
                else return false;
                return true;
            }
        },
        view(EditorView) {
            this.view_obj = new EditorDropdown(dropdown_specs, this, EditorView);
            return this.view_obj;
        },
        appendTransaction(transactions, oldState, newState) {
            return EditorDropdown.updateAttributes(newState, dropdown_specs);
        }
    })
}
