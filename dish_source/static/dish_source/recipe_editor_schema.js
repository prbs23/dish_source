const {Schema} = require("prosemirror-model")

const ingredientSchemaSpec = {
    nodes: {
        // ============================
        // === Base Text Node Types ===
        // ============================
        text: {
            inline: true,
            group: "inline"
        },
        paragraph: {
            content: "inline*",
            group: "richText",
            attrs: {textAlign: {default: "left"}},
            parseDOM:  [{
                tag: "p",
                getAttrs(dom) {
                    return {textAlign: (dom.style.textAlign) ? dom.style.textAlign : "left"};
                },
                priority: 45,
            }],
            toDOM(node) {return ["p", {style: "text-align: " + node.attrs.textAlign}, 0]},
            marks: "richTextMarks",
        },
        blockquote: {
            content: "richText+",
            group: "richText",
            defining: true,
            parseDOM: [{tag: "blockquote", priority: 45}],
            toDOM() { return ["blockquote", 0]},
            marks: "richTextMarks",
        },
        horizontalRule: {
            group: "richText",
            parseDOM: [{tag: "hr", priority: 45}],
            toDOM() { return ["hr"] },
        },
        heading: {
            attrs: {level: {default: 1}},
            content: "inline*",
            group: "richText",
            defining: true,
            parseDOM: [{tag: "h1", attrs: {level: 1}, priority: 45},
                       {tag: "h2", attrs: {level: 2}, priority: 45},
                       {tag: "h3", attrs: {level: 3}, priority: 45},
                       {tag: "h4", attrs: {level: 4}, priority: 45},
                       {tag: "h5", attrs: {level: 5}, priority: 45},
                       {tag: "h6", attrs: {level: 6}, priority: 45}],
            toDOM(node) { return ["h" + node.attrs.level, 0] },
            marks: "richTextMarks",
        },
        hardBreak: {
            inline: true,
            group: "inline",
            selectable: false,
            parseDOM: [{tag: "br", priority: 45}],
            toDOM() { return ["br"] },
        },
        orderedList: {
            content: "listItem+",
            group: "richText",
            attrs: {order: {default: 1}},
            parseDOM: [{
                tag: "ol",
                getAttrs(dom) {
                    return {order: dom.hasAttribute("start") ? +dom.getAttribute("start") : 1}
                },
                priority: 45
            }],
            toDOM(node) {
                return ["ol", {start: node.attrs.order}, 0]
            }
        },
        bulletList: {
            content: "listItem+",
            group: "richText",
            parseDOM: [{tag: "ul", priority: 45}],
            toDOM() { return ["ul", 0] }
        },
        listItem: {
            content: "paragraph richText*",
            parseDOM: [{tag: "li", priority: 45}],
            toDOM() { return ["li", 0] }
        },

        // ===========================
        // === Recipe Header Nodes ===
        // ===========================
        recipeTitle: {
            content: "text*",
            marks: "",
            toDOM() {return ["div", {id: "recipe-title", class: "text-primary"}, 0]},
            parseDOM: [{tag: "div#recipe-title.text-primary"}],
            defining: true,
            isolating: true,
            selectable: false,
        },
        recipeCategory: {
            content: "text*",
            marks: "",
            attrs: {item: {default: null}},
            toDOM() {return ["span", {id: "category-name"}, 0]},
            parseDOM: [{tag: "span#category-name"}],
            defining: true,
            isolating: true,
            selectable: false,
        },
        recipeCategoryDivider: {
            toDOM() {return ["span", {id: "category-divider"}, " - "]},
            parseDOM: [{tag: "span#category-divider"}],
            defining: true,
            isolating: true,
            selectable: false,
            atom: true,
        },
        recipeSubcategory: {
            content: "text*",
            marks: "",
            attrs: {item: {default: null}},
            toDOM() {return ["span", {id: "subcategory-name"}, 0]},
            parseDOM: [{tag: "span#subcategory-name"}],
            defining: true,
            isolating: true,
            selectable: false,
        },
        recipeCategoryBlock: {
            content: "recipeCategory recipeCategoryDivider recipeSubcategory",
            toDOM() {return ["div", {id: "category", class: "text-secondary"}, 0]},
            parseDOM: [{tag: "div#category.text-secondary"}],
            defining: true,
            isolating: true,
            selectable: false,
            atom: true,
        },
        recipeSource: {
            content: "text*",
            marks: "",
            attrs: {item: {default: null}},
            toDOM() {return ["div", {id: "source"}, ["span", {id: "source-label", contenteditable: "false"}, "Source: "], ["span", {id: "source-value"}, 0]]},
            parseDOM: [{tag: "span#source-label", ignore: true}, {tag: "span#source-value"}],
            defining: true,
            isolating: true,
            selectable: false,
        },
        recipeTag: {
            attrs: {item: {default: null}},
            toDOM(node) {return ["span", {class: "badge badge-primary mr-1", contenteditable: "false"}, node.attrs.item == null ? "ERROR" :node.attrs.item.name]},
            parseDOM: [{
                tag: "span.badge.badge-primary.mr-1",
                getAttrs(dom) {
                    let tag = recipe_tags.find(tag => tag.name.localeCompare(dom.textContent, undefined, {sensitivity: "accent"}) == 0);
                    return {item: tag}
                },
            }],
            draggable: true,
        },
        newRecipeTag: {
            content: "text*",
            marks: "",
            attrs: {item: {default: null}},
            toDOM(node) {return ["span", {id: "new-recipe-tag"}, 0]},
            parseDOM: [{target: "span#new-recipe-tag"}],
        },
        recipeTags: {
            content: "(recipeTag newRecipeTag?)* newRecipeTag",
            toDOM() {return ["div", {id: "tags"}, 0]},
            parseDOM: [{tag: "div#tags"}],
            defining: true,
            isolating: true,
            atom: true,
            selectable: false,
        },
        recipeYields: {
            content: "text*",
            marks: "unitName",
            toDOM() {return ["span", {id: "yields-value"}, 0]},
            parseDOM: [{tag: "span#yields-value"}],
            defining: true,
            isolating: true,
            selectable: false,
            attrs: {item: {default: []}},
        },
        recipeYieldsLabel: {
            toDOM() {return ["span", {id: "yields-label", contenteditable: "false"}, "Yields: "]},
            parseDOM: [{tag: "span#yields-label"}],
            defining: true,
            isolating: true,
            selectable: false,
            atom: true,
        },
        recipeYieldsContainer: {
            content: "recipeYieldsLabel recipeYields",
            toDOM() {return ["div", {id: "yields"}, 0]},
            parseDOM: [{tag: "div#yields"}],
            defining: true,
            isolating: true,
            selectable: false,
        },
        recipePrepTime: {
            content: "text*",
            marks: "",
            toDOM() {return ["span", {id: "prep-duration-value"}, 0]},
            parseDOM: [{tag: "span#prep-duration-value"}],
            defining: true,
            isolating: true,
            selectable: false,
        },
        recipePrepTimeLabel: {
            toDOM() {return ["span", {id: "prep-duration-label", contenteditable: "false"}, "Prep Time: "]},
            parseDOM: [{tag: "span#prep-duration-label"}],
            defining: true,
            isolating: true,
            selectable: false,
            atom: true,
        },
        recipePrepTimeContainer: {
            content: "recipePrepTimeLabel recipePrepTime",
            marks: "",
            toDOM() {return ["div", {id: "prep-duration"}, 0]},
            parseDOM: [{tag: "div#prep-duration"}],
            defining: true,
            isolating: true,
            selectable: false,
        },
        recipeCookTime: {
            content: "text*",
            marks: "",
            toDOM() {return ["span", {id: "cook-duration-value"}, 0]},
            parseDOM: [{tag: "span#cook-duration-value"}],
            defining: true,
            isolating: true,
            selectable: false,
        },
        recipeCookTimeLabel: {
            toDOM() {return ["span", {id: "cook-duration-label", contenteditable: "false"}, "Cook Time: "]},
            parseDOM: [{tag: "span#cook-duration-label"}],
            defining: true,
            isolating: true,
            selectable: false,
            atom: true,
        },
        recipeCookTimeContainer: {
            content: "recipeCookTimeLabel recipeCookTime",
            marks: "",
            toDOM() {return ["div", {id: "cook-duration"}, 0]},
            parseDOM: [{tag: "div#cook-duration"}],
            defining: true,
            isolating: true,
            selectable: false,
        },
        recipeTotalTime: {
            content: "text*",
            marks: "",
            toDOM() {return ["div", {id: "total-duration"}, ["span", {id: "total-duration-label"}, "Total Time: "], ["span", {id: "total-duration-value", contenteditable: "false"}, 0]]},
            parseDOM: [{tag: "span#total-duration-label", ignore: true}, {tag: "span#total-duration-value"}],
            defining: true,
            isolating: true,
            selectable: false,
        },
        recipeHeader: {
            content: "recipeTitle recipeCategoryBlock recipeSource recipeTags recipeYieldsContainer recipePrepTimeContainer recipeCookTimeContainer recipeTotalTime",
            toDOM() {return ["div", {id: "header", class: "recipe-section"}, 0]},
            parseDOM: [{tag: "div#header.recipe-section"}],
            defining: true,
        },

        // =================================
        // === Recipe Description Nodes ===
        // =================================
        recipeDescription: {
            content: "richText+",
            toDOM() {return ["div", {id: "description", class:"recipe-section"}, 0]},
            parseDOM: [{tag: "div#description.recipe-section"}],
            defining: true,
            isolating: true,
        },

        // ======================================
        // === Recipe ingredient editor Nodes ===
        // ======================================
        recipeIngredient: {
            content: "text*",
            marks: "unitName ingredientName",
            group: "recipeIngredientType",
            toDOM() {return ["li", {class: "recipe-ingredient-item"}, 0]},
            parseDOM: [{tag: "li.recipe-ingredient-item"}],
            selectable: false,
            draggable: false,
            attrs: {
                item: {default: null},
            },
        },
        recipeIngredientFirstSubItem: {
            content: "text*",
            marks: "unitName ingredientName",
            group: "recipeIngredientType",
            toDOM() {return ["li", {class: "recipe-ingredient-item first-sub-item"}, 0]},
            parseDOM: [{tag: "li.recipe-ingredient-item.first-sub-item", priority: 51}],
            selectable: false,
            draggable: false,
            attrs: {
                item: {default: null},
            },
        },
        recipeIngredientSubItem: {
            content: "text*",
            marks: "unitName ingredientName",
            group: "recipeIngredientType",
            toDOM() {return ["li", {class: "recipe-ingredient-item sub-item"}, 0]},
            parseDOM: [{tag: "li.recipe-ingredient-item.sub-item", priority: 51}],
            selectable: false,
            draggable: false,
            attrs: {
                item: {default: null},
            },
        },
        recipeIngredientList: {
            content: "(recipeIngredient (recipeIngredientFirstSubItem recipeIngredientSubItem*)*)+",
            marks: "",
            toDOM() {return ["ul", {class: "ingredient-list"}, 0]},
            parseDOM: [{tag: "ul.ingredient-list"}],
            selectable: false,
        },
        recipeIngredientSectionHeader: {
            content: "text*",
            marks: "",
            toDOM() {return ["p", {class: "ingredient-section-header"}, 0]},
            parseDOM: [{tag: "p.ingredient-section-header"}],
            selectable: false
        },
        recipeIngredientSection: {
            content: "recipeIngredientSectionHeader recipeIngredientList",
            marks: "",
            toDOM() {return ["div", {class: "ingredient-section"}, 0]},
            parseDOM: [{tag: "div.ingredient-section"}],
            selectable: false,
        },
        recipeIngredientsBlock: {
            content: "recipeIngredientSection+",
            toDOM() {return ["div", {id: "ingredients", class: "recipe-section"}, ["h5", "Ingredients"], ["div", {id: "ingredients-container"}, 0]]},
            parseDOM: [{tag: "div#ingredients-container"}],
            defining: true,
            isolating: true,
        },

        // ================================
        // === Recipe Instruction Nodes ===
        // ================================
        recipeInstruction: {
            content: "inline*",
            marks: "richTextMarks",
            toDOM() {return ["li", 0]},
            parseDOM: [{tag: "li"}],
        },
        recipeInstructionList: {
            content: "recipeInstruction+",
            toDOM() {return ["ol", {class: "instruction-list"}, 0]},
            parseDOM: [{tag: "ol.instruction-list"}],
        },
        recipeInstructionSectionHeader: {
            content: "text*",
            marks: "",
            toDOM() {return ["p", {class: "instruction-section-header"}, 0]},
            parseDOM: [{tag: "p.instruction-section-header"}],
        },
        recipeInstructionSection: {
            content: "recipeInstructionSectionHeader recipeInstructionList",
            toDOM() {return ["div", {class: "instruction-section"}, 0]},
            parseDOM: [{tag: "div.instruction-section"}],
        },
        recipeInstructionBlock: {
            content: "recipeInstructionSection+",
            toDOM() {return ["div", {id: "instructions", class: "recipe-section"}, ["h5", "Instructions"], ["div", {id: "instructions-container"}, 0]]},
            parseDOM: [{tag: "div#instructions-container"}],
            defining: true,
            isolating: true,
        },

        // ===============================
        // === Recipe Variations Nodes ===
        // ===============================
        recipeVariationContent: {
            content: "richText+",
            toDOM() {return ["div", {class: "variation-content"}, 0]},
            parseDOM: [{tag: "div.variation-content"}],
        },
        recipeVariationName: {
            content: "text*",
            marks: "",
            toDOM() {return ["p", {class: "variation-name"}, 0]},
            parseDOM: [{tag: "p.variation-name"}],
        },
        recipeVariation: {
            content: "recipeVariationName recipeVariationContent",
            toDOM() {return ["div", {class: "recipe-variation"}, 0]},
            parseDOM: [{tag: "div.recipe-variation"}],
        },
        recipeVariationsBlock: {
            content: "recipeVariation+",
            toDOM() {return ["div", {id: "variations", class: "recipe-section"}, ["h5", "Variations"], ["div", {id: "variations-container"}, 0]]},
            parseDOM: [{tag: "div#variations-container"}],
            defining: true,
            isolating: true,
        },

        // =================================
        // === Recipe Description Nodes ===
        // =================================
        recipeNotes: {
            content: "richText+",
            toDOM() {return ["div", {id: "notes-wrapper"}, 0]},
            parseDOM: [{tag: "div#notes-wrapper"}],
            defining: true,
            isolating: true,
        },
        recipeNotesBlock: {
            content: "recipeNotes",
            toDOM() {return ["div", {id: "notes", class: "recipe-section"}, ["h5", "Notes"], ["div", {id: "notes-container"}, 0]]},
            parseDOM: [{tag: "div#notes>h5", ignore: true}, {tag: "div#notes-container"}],
            defining: true,
            isolating: true,
        },

        // ===============================
        // === Full Document Structure ===
        // ===============================
        doc: {
            content: "recipeHeader recipeDescription recipeIngredientsBlock recipeInstructionBlock recipeVariationsBlock recipeNotesBlock"
        }
    },
    marks: {
        link: {
            attrs: {
                href: {default: "https://"},
            },
            parseDOM: [{tag: "a[href]", getAttrs(dom) {
                return {href: dom.getAttribute("href"), title: dom.getAttribute("title")}
            }}],
            toDOM(node) { let {href, title} = node.attrs; return ["a", {href, title}, 0] },
            group: "richTextMarks",
        },
        em: {
            parseDOM: [{tag: "i"}, {tag: "em"}, {style: "font-style=italic"}],
            toDOM() { return ["em", 0] },
            group: "richTextMarks",
        },
        strong: {
            parseDOM: [{tag: "strong"},
                       {tag: "b", getAttrs: node => node.style.fontWeight != "normal" && null},
                       {style: "font-weight", getAttrs: value => /^(bold(er)?|[5-9]\d{2,})$/.test(value) && null}],
            toDOM() { return ["strong", 0] },
            group: "richTextMarks",
        },
        underline: {
            parseDOM: [{tag: "u"}, {tag: "u", style: "text-decoration=underline"}],
            toDOM() {return ["u", {style: "text-decoration:underline"}, 0]},
            group: "richTextMarks",
        },
        strikethrough: {
            parseDOM: [{tag: "u", style: "text-decoration=line-through", priority: 51}],
            toDOM() {return ["u", {style: "text-decoration:line-through"}, 0]},
            group: "richTextMarks",
        },
        unitName: {
            excludes: "_",
            spanning: false,
            toDOM() {return ["mark", {class: "unit-name"}]},
            parseDOM: [{tag: "mark.unit-name"}],
            attrs: {item: {default: null}},
            inclusive: false,
        },
        ingredientName: {
            excludes: "_",
            spanning: false,
            toDOM() {return ["mark", {class: "ingredient-name"}]},
            parseDOM: [{tag: "mark.ingredient-name"}],
            attrs: {item: {default: null}},
            inclusive: false,
        }
    }
};

const ingredientSchema = new Schema(ingredientSchemaSpec);

