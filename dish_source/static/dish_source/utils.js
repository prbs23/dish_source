function formatDecimal(value) {
    var [whole, integer_part, decimal_part] = value.match(/^\s*(\d+)?.?(\d{0,3}[1-9])?/);
    var fraction_part;
    if (decimal_part == '125')       {fraction_part = "⅛";}
    else if (decimal_part == '25')   {fraction_part = "¼";}
    else if (decimal_part == '3333') {fraction_part = "⅓";}
    else if (decimal_part == '375')  {fraction_part = "⅜";}
    else if (decimal_part == '5')    {fraction_part = "½";}
    else if (decimal_part == '625')  {fraction_part = "⅝";}
    else if (decimal_part == '6667') {fraction_part = "⅔";}
    else if (decimal_part == '75')   {fraction_part = "¾";}
    else if (decimal_part == '875')  {fraction_part = "⅞";}

    var result = ""
    if (integer_part != null && (fraction_part == null || integer_part != "0")) {
        result += integer_part;
    } else if (fraction_part == null) {
        result += "0";
    }

    if (fraction_part != null) {
        result += fraction_part;
    } else if (decimal_part != null) {
        result += "." + decimal_part;
    }

    return result;
}

function encodeDecimal(value) {
    var [whole, integer, frac_char, frac_num, frac_denom, decimal] =
        value.match(/(?<integer>\d*)??\s*(?:(?<frac_char>[⅛¼⅓⅜½⅝⅔¾⅞])|(?:(?<frac_num>\d+)[\/⁄](?<frac_denom>\d+))|(?:\.(?<decimal>\d+)))?(?:\s|$)/);
    var result;

    if (integer != null) {result = Number(integer);}
    else {result = 0;}

    if (frac_char != null) {
        if (frac_char == "⅛") {result += 0.125;}
        else if (frac_char == "¼") {result += 0.25;}
        else if (frac_char == "⅓") {result += 0.3333;}
        else if (frac_char == "⅜") {result += 0.375;}
        else if (frac_char == "½") {result += 0.5;}
        else if (frac_char == "⅝") {result += 0.624;}
        else if (frac_char == "⅔") {result += 0.6667}
        else if (frac_char == "¾") {result += 0.75}
        else if (frac_char == "⅞") {result += 0.875}
    } else if (frac_num != null && frac_denom != null) {
        result += Number(frac_num) / Number(frac_denom);
    } else if (decimal != null) {
        result += Number("0."+decimal);
    }
    return result.toFixed(4);
}

function formatQuantityValue(quantity) {
    result = ""
    if (quantity.approx) result += "~";
    result += formatDecimal(quantity.min_quantity)
    if (quantity.max_quantity) result += "-" + formatDecimal(quantity.max_quantity);
    return result;
}

function formatQuantityUnit(quantity) {
    if (quantity.unit.abbrev) {
        return quantity.unit.abbrev + ".";
    } else if (Number(quantity.min_quantity) > 1 || (quantity.max_quantity != null && Number(quantity.max_quantity) > 1)) {
        return quantity.unit.plural;
    } else {
        return quantity.unit.name;
    }
}

function format_duration(duration) {
    let result = ""
    duration = duration / 60
    if (duration >= 60) {
        result += Math.floor(duration / 60) + " hr";
        if (Math.ceil(duration % 60) != 0) {
            result += " ";
        }
    }
    if (Math.ceil(duration % 60) != 0) {
        result += Math.ceil(duration % 60) + " min";
    }
    return result;
}

function format_duration_range(min, max) {
    let result = format_duration(min)
    if (max && min != max) {
        result += " - " + format_duration(max)
    }
    return result
}

function filterInPlace(arr, callback) {
    for (let idx = 0; idx < arr.length; idx++) {
        if (!callback(arr[idx], idx, arr)) {
            arr.splice(idx, 1);
            idx -= 1;
        }
    }
}

function escapeRegExp(string) {
  return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
}

function getUnusedId(items) {
    let min_id = Math.min(...items.map(item => item.id));
    let id = -1;
    for (id = -1; id >= min_id; id--) if (items.find(item => item.id == id) == null) break;
    return id;
}

function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

function showModal(parent_dom, settings) {
    let modal_dom = document.createElement("div");
    modal_dom.className = "modal fade";
    modal_dom.style.display = "block";
    modal_dom.id = "save-modal";
    modal_dom.style.backgroundColor = "rgba(0,0,0,0.5)";
    modal_dom.setAttribute("tab-index", -1);
    let modal_dialog = document.createElement("div");
    modal_dialog.className = "modal-dialog";
    let modal_content = document.createElement("div");
    modal_content.className = "modal-content";
    let modal_header = document.createElement("div");
    modal_header.className = "modal-header " + settings.header_class;
    let modal_title = document.createElement("h5");
    modal_title.className = "modal-title";
    modal_title.textContent = settings.title;
    modal_header.append(modal_title);
    let modal_close_button = document.createElement("button");
    modal_close_button.type = "button";
    modal_close_button.className = "close";
    modal_close_button.innerHTML = "&times;";
    modal_close_button.parentModal = modal_dom;
    modal_close_button.addEventListener("click", e => {
        e.target.parentModal.className = "modal fade";
        e.target.parentModal.remove();
    });
    modal_header.append(modal_close_button);
    modal_content.append(modal_header);
    settings.body_node.className += " modal-body";
    settings.body_node.style.overflowY = "auto";
    settings.body_node.style.maxHeight = (window.innerHeight - 200) + "px";
    modal_content.append(settings.body_node);
    let modal_footer = document.createElement("div");
    modal_footer.className = "modal-footer";
    if (settings.cancel_en === undefined || settings.cancel_en) {
        let modal_cancel_button = document.createElement("button");
        modal_cancel_button.className = "btn btn-secondary";
        modal_cancel_button.type = "button";
        modal_cancel_button.textContent = settings.cancel_btn_text === undefined ? "Cancel" : settings.cancel_btn_text;
        modal_cancel_button.parentModal = modal_dom
        modal_cancel_button.addEventListener("click", e => {
            e.target.parentModal.className = "modal fade";
            e.target.parentModal.remove();
        });
        modal_footer.append(modal_cancel_button);
    }
    if (settings.ok_en === undefined || settings.ok_en) {
        let modal_ok_button = document.createElement("button");
        modal_ok_button.className = "btn btn-primary";
        modal_ok_button.type = "button";
        modal_ok_button.textContent = settings.ok_btn_text === undefined ? "Ok" : settings.ok_btn_text;
        modal_ok_button.parentModal = modal_dom;
        modal_ok_button.addEventListener("click", e => {
            e.target.parentModal.className = "modal fade";
            e.target.parentModal.remove();
            settings.ok_cb();
        });
        modal_footer.append(modal_ok_button);
    }
    modal_content.append(modal_footer);
    modal_dialog.append(modal_content);
    modal_dom.append(modal_dialog);
    parent_dom.append(modal_dom);
    modal_dom.className = "modal fade show";
    return modal_dom;
}
