
function insertHardBreak(state, dispatch) {
    if (!state.selection.$head.node().type.contentMatch.matchType(ingredientSchema.nodes.hardBreak)) return false;
    if (dispatch) {
        dispatch(state.tr.replaceSelectionWith(ingredientSchema.nodes.hardBreak.create()).scrollIntoView());
    }
    return true;
}

function insertHorizontalRule(state, dispatch) {
    if (!state.selection.$head.node().type.contentMatch.matchType(ingredientSchema.nodes.horizontalRule)) return false;
    if (dispatch) {
        dispatch(state.tr.replaceSelectionWith(ingredientSchema.nodes.horizontalRule.create()).scrollIntoView());
    }
    return true;
}

function changeNodeAttrValue(nodeType, attr, value) {
    return function(state, dispatch) {
        let {from, to} = state.selection;
        let tr = state.tr;
        let applicable = false;
        state.doc.nodesBetween(from, to, (node, pos) => {
            if (node.type == nodeType && node.attrs[attr] !== value) {
                applicable = true
                if (dispatch) tr = tr.setNodeMarkup(pos, node.type, Object.assign({}, node.attrs, {[attr]: value}));
            }
        });
        if (!applicable) return false
        if (dispatch) dispatch(tr);
        return true;
    }
}

function makeIngredientHeader(state, dispatch, view) {
    let {$head} = state.selection;
    if ($head.node().type.spec.group != "recipeIngredientType") return false;
    if (dispatch) {
        let tr = state.tr;
        // Check if we need to promote the next or next two nodes
        let nodeIndex = $head.index($head.depth-1);
        let parentNode = $head.node($head.depth-1);
        if (parentNode.childCount > nodeIndex + 1 && parentNode.child(nodeIndex+1).type == ingredientSchema.nodes.recipeIngredientFirstSubItem) {
            if (parentNode.childCount > nodeIndex + 2 && parentNode.child(nodeIndex+2).type == ingredientSchema.nodes.recipeIngredientSubItem) {
                tr = tr.setNodeMarkup($head.posAtIndex(nodeIndex + 2, $head.depth-1), ingredientSchema.nodes.recipeIngredientFirstSubItem);
            }
            tr = tr.setNodeMarkup($head.posAtIndex(nodeIndex + 1, $head.depth-1), ingredientSchema.nodes.recipeIngredient);
        }
        
        // Get the text of the current node, and a fragment of the remaining ingredient nodes in the section
        let nodeText = $head.node().textContent;
        let contentAfter = tr.doc.slice($head.after(4), $head.end(3)).content;

        // Insert a new section with the content the follows the current node
        tr = tr.insert($head.after(2), ingredientSchema.node(ingredientSchema.nodes.recipeIngredientSection, null, [
            ingredientSchema.node(ingredientSchema.nodes.recipeIngredientSectionHeader, null, nodeText.length == 0 ? null : ingredientSchema.text(nodeText)),
            ingredientSchema.node(ingredientSchema.nodes.recipeIngredientList, null, contentAfter.size == 0 ?
                ingredientSchema.node(ingredientSchema.nodes.recipeIngredient) : contentAfter)
        ]));
        // Remove the nodes we moved to the new section
        tr = tr.delete($head.before(4), $head.end(3));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function insertIngredientHeader(state, dispatch, view) {
    let {$from, $to} = state.selection;
    if (!($to.node().type == ingredientSchema.nodes.recipeIngredientSectionHeader ||
        $to.node().type.spec.group == "recipeIngredientType"))
        return false;
    if (dispatch) {
        let tr = state.tr;
        let contentAfter = ingredientSchema.node(ingredientSchema.nodes.recipeIngredientSection, null, [
            ingredientSchema.node(ingredientSchema.nodes.recipeIngredientSectionHeader, null, $to.pos == $to.end() ? null :
                ingredientSchema.text(tr.doc.textBetween($to.pos, $to.end()))),
            tr.doc.slice($to.after(), $to.after(2)-1).content.content[0]]);
        tr = tr.deleteSelection();
        tr = tr.delete(tr.mapping.map($to.pos), tr.mapping.map($to.end(2)));
        let insertPos = tr.mapping.map($to.after(2));
        tr = tr.insert(insertPos, contentAfter);
        tr = tr.setSelection(TextSelection.create(tr.doc, insertPos + 1));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function makeIngredient(state, dispatch, view) {
    let {$head} = state.selection;
    if (($head.node().type != ingredientSchema.nodes.recipeIngredientSectionHeader || $head.start(1) == $head.before(2)) &&
        ($head.node().type != ingredientSchema.nodes.recipeIngredientFirstSubItem &&
         $head.node().type != ingredientSchema.nodes.recipeIngredientSubItem)) return false;
    if (dispatch) {
        let tr = state.tr;
        if ($head.node().type == ingredientSchema.nodes.recipeIngredientSectionHeader) {
            tr = tr.replaceRange($head.before(2)-3, $head.after()+2,
                new Slice(Fragment.from(ingredientSchema.nodes.recipeIngredient.create(null, $head.node().content.size == 0 ? null : ingredientSchema.text($head.node().textContent))),0,0)
            );
        } else {
            let node = $head.node();
            let node_index = $head.index($head.depth - 1);
            let parent_node = $head.node($head.depth - 1);
            // If the next node is a substitution item, then we need to promote it before we can promote this item
            if (node_index < parent_node.childCount-1 &&
                parent_node.child(node_index + 1).type == ingredientSchema.nodes.recipeIngredientSubItem) {
                let next_node = parent_node.child(node_index + 1);
                tr = tr.replaceWith(
                    $head.after(), $head.after() + next_node.nodeSize,
                    ingredientSchema.node(ingredientSchema.nodes.recipeIngredientFirstSubItem, undefined, next_node.slice(0).content));
            }
            tr = tr.replaceWith(
                $head.before(), $head.after(),
                ingredientSchema.node(ingredientSchema.nodes.recipeIngredient, undefined, node.slice(0).content));
            tr = tr.setSelection(TextSelection.create(tr.doc, $head.pos));
        }
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function insertIngredient(state, dispatch, view) {
    let {$from, $to} = state.selection;
    if ($from.node().type != ingredientSchema.nodes.recipeIngredientSectionHeader &&
        $from.node().type.spec.group != "recipeIngredientType") return false;
    if (dispatch) {
        let tr = state.tr;
        if ($from.node().type == ingredientSchema.nodes.recipeIngredientSectionHeader) {
            let textAfter = "";
            if ($to.pos != $to.end()) {
                textAfter = tr.doc.textBetween($to.pos, $to.end());
            }
            tr = tr.delete($from.pos, $to.end());
            let insPos = insertPoint(tr.doc, tr.mapping.map($from.after()+1), ingredientSchema.nodes.recipeIngredient);
            if (insPos == undefined) return false;
            tr = tr.insert(insPos, ingredientSchema.node("recipeIngredient", null, textAfter ? ingredientSchema.text(textAfter) : null));
            tr = tr.setSelection(TextSelection.create(tr.doc, insPos+1));
        } else {
            tr = tr.deleteSelection();
            tr = tr.split(tr.mapping.map($from.pos, 1, {type: $from.node().type, attrs: {item: null}}));
        }
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function demoteSubstitution(state, dispatch, view) {
    let {$head} = state.selection;
    if ($head.node().type.spec.group != "recipeIngredientType") return false;
    if ($head.index($head.depth-1) == 0) return false;
    if (dispatch) {
        let node = $head.node();
        let node_index = $head.index($head.depth - 1);
        let parent_node = $head.node($head.depth-1);
        let tr = state.tr;
        if (node.type == ingredientSchema.nodes.recipeIngredient) {
            tr = tr.replaceWith(
                $head.before(), $head.after(),
                ingredientSchema.node(ingredientSchema.nodes.recipeIngredientFirstSubItem, undefined, node.slice(0).content));
        } else if (node.type == ingredientSchema.nodes.recipeIngredientFirstSubItem &&
                   parent_node.child(node_index-1).type != ingredientSchema.nodes.recipeIngredient) {
            tr = tr.replaceWith(
                $head.before(), $head.after(),
                ingredientSchema.node(ingredientSchema.nodes.recipeIngredientSubItem, undefined, node.slice(0).content));
        } else {
            // If the next node is a substitution item, then we need to promote it before we can promote this item
            if (node_index < parent_node.childCount-1 &&
                parent_node.child(node_index + 1).type == ingredientSchema.nodes.recipeIngredientSubItem) {
                let next_node = parent_node.child(node_index + 1);
                tr = tr.replaceWith(
                    $head.after(), $head.after() + next_node.nodeSize,
                    ingredientSchema.node(ingredientSchema.nodes.recipeIngredientFirstSubItem, undefined, next_node.slice(0).content));
            }
            tr = tr.replaceWith(
                $head.before(), $head.after(),
                ingredientSchema.node(ingredientSchema.nodes.recipeIngredient, undefined, node.slice(0).content));
        }
        tr = tr.setSelection(TextSelection.create(tr.doc, $head.pos));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function promoteSubstitution(state, dispatch, view) {
    let {$head} = state.selection;
    if ($head.node().type != ingredientSchema.nodes.recipeIngredientFirstSubItem &&
        $head.node().type != ingredientSchema.nodes.recipeIngredientSubItem) return false;
    if (dispatch) {
        let node = $head.node();
        let node_index = $head.index($head.depth - 1);
        let parent_node = $head.node($head.depth-1);
        let tr = state.tr;
        if (node.type == ingredientSchema.nodes.recipeIngredientFirstSubItem) {
            // If the next node is a substitution item, then we need to promote it before we can promote this item
            if (node_index < parent_node.childCount-1 &&
                parent_node.child(node_index + 1).type == ingredientSchema.nodes.recipeIngredientSubItem) {
                let next_node = parent_node.child(node_index + 1);
                tr = tr.replaceWith(
                    $head.after(), $head.after() + next_node.nodeSize,
                    ingredientSchema.node(ingredientSchema.nodes.recipeIngredientFirstSubItem, undefined, next_node.slice(0).content));
            }
            tr = tr.replaceWith(
                $head.before(), $head.after(),
                ingredientSchema.node(ingredientSchema.nodes.recipeIngredient, undefined, node.slice(0).content));
        } else {
            tr = tr.replaceWith(
                $head.before(), $head.after(),
                ingredientSchema.node(ingredientSchema.nodes.recipeIngredientFirstSubItem, undefined, node.slice(0).content));
        }
        tr = tr.setSelection(TextSelection.create(tr.doc, $head.pos));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function markUnit(state, dispatch, view) {
    let {$head, $anchor, from, to} = state.selection;
    if ($head.node().type.spec.group != "recipeIngredientType" && $head.node().type != ingredientSchema.nodes.recipeYields) return false;
    if ($head.node() != $anchor.node()) return false;
    if (dispatch) {
        let tr = state.tr;
        tr = tr.removeMark(from, to, ingredientSchema.marks.ingredientName) // Remove ingredientName marks from selection, so we don't end up with a node that's both an ingredient and unit.
        tr = tr.addMark(from, to, ingredientSchema.mark(ingredientSchema.marks.unitName, {item: null}));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function markIngredient(state, dispatch, view) {
    let {$head, $anchor, from, to} = state.selection;
    if ($head.node().type.spec.group != "recipeIngredientType") return false;
    if ($head.node() != $anchor.node()) return false;
    if (dispatch) {
        let tr = state.tr;
        tr = tr.removeMark(from, to, ingredientSchema.marks.unitName) // Remove all marks from selection, so we don't end up with a node that's both an ingredient and unit.
        tr = tr.removeMark($head.start(), $head.end(), ingredientSchema.marks.ingredientName); // Remove all ingredient marks from node, because only one ingredient name is allowed.
        tr = tr.addMark(from, to, ingredientSchema.mark(ingredientSchema.marks.ingredientName, {item: null}));
        dispatch(tr.scrollIntoView())
    }
    return true;
}

function clearMarks(state, dispatch, view) {
    let {$head, from, to} = state.selection;
    if ($head.node().type.spec.group != "recipeIngredientType" && $head.node().type != ingredientSchema.nodes.recipeYields) return false;
    if (from == to && $head.marks().length > 0) {
        from = $head.posAtIndex($head.index());
        to = $head.posAtIndex($head.indexAfter());
    }
    if (dispatch) {
        let tr = state.tr;
        tr = tr.removeMark(from, to, null); // Remove all marks
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function mergeSubBackwardsToIngredient(state, dispatch, view) {
    let {$head} = state.selection;
    let node_index = $head.index($head.depth - 1);
    let parent_node = $head.node($head.depth - 1);

    if ($head.pos != $head.start()) return false;
    if ($head.node().type != ingredientSchema.nodes.recipeIngredientFirstSubItem) return false;
    if (parent_node.child(node_index - 1).type != ingredientSchema.nodes.recipeIngredient) return false;
    if (node_index + 1 >= parent_node.childCount) return false;
    let next_node = parent_node.child(node_index + 1);
    if (next_node.type != ingredientSchema.nodes.recipeIngredientSubItem) return false;
    if (dispatch) {
        let tr = state.tr;
        let textAfter = tr.doc.textBetween($head.pos, $head.end());
        state.tr = tr.replaceWith(
            $head.after(), $head.after() + next_node.nodeSize,
            ingredientSchema.node(ingredientSchema.nodes.recipeIngredientFirstSubItem, undefined, next_node.slice(0).content));
        dispatch(tr.scrollIntoView());
    }
    return false;
}

function mergeIngredientBackwardsToHeader(state, dispatch, view) {
    let {$head} = state.selection;
    if ($head.pos != $head.start()) return false;
    if ($head.node().type != ingredientSchema.nodes.recipeIngredient) return false;
    if ($head.index($head.depth-1) > 0) return false;
    if (dispatch) {
        let tr = state.tr;
        let textAfter = tr.doc.textBetween($head.pos, $head.end());
        tr = tr.delete($head.before(), $head.after());
        tr = tr.insertText(textAfter, $head.pos - 3);
        tr = tr.setSelection(TextSelection.create(tr.doc, $head.pos - 3));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function mergeHeaderBackwardsToIngredient(state, dispatch, view) {
    let {$head} = state.selection;
    if ($head.pos != $head.start()) return false;
    if ($head.node().type != ingredientSchema.nodes.recipeIngredientSectionHeader) return false;
    if ($head.before(1) == 0) return false;
    if (dispatch) {
        let tr = state.tr;
        let textAfter = tr.doc.textBetween($head.pos, $head.end());
        tr = tr.delete($head.pos - 5, $head.after() + 1)
        tr = tr.insertText(textAfter, $head.pos - 5);
        tr = tr.setSelection(TextSelection.create(tr.doc, $head.pos - 5));
        dispatch(tr.scrollIntoView())
    }
    return true;
}

function makeInstructionHeader(state, dispatch, view) {
    let {$head} = state.selection;
    if ($head.node().type != ingredientSchema.nodes.recipeInstruction) return false;
    if (dispatch) {
        let tr = state.tr;
        // Get the text of the current node, and a fragment of the remaining instruction nodes in the section
        let nodeText = $head.node().textContent;
        let contentAfter = tr.doc.slice($head.after(4), $head.end(3)).content;

        // Insert a new section with the content the follows the current node
        tr = tr.insert($head.after(2), ingredientSchema.node(ingredientSchema.nodes.recipeInstructionSection, null, [
            ingredientSchema.node(ingredientSchema.nodes.recipeInstructionSectionHeader, null, nodeText.length == 0 ? null : ingredientSchema.text(nodeText)),
            ingredientSchema.node(ingredientSchema.nodes.recipeInstructionList, null, contentAfter.size == 0 ?
                ingredientSchema.node(ingredientSchema.nodes.recipeInstruction) : contentAfter)
        ]));
        // Remove the nodes we moved to the new section
        tr = tr.delete($head.before(4), $head.end(3));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function insertInstructionHeader(state, dispatch, view) {
    let {$from, $to} = state.selection;
    if (!($to.node().type == ingredientSchema.nodes.recipeInstructionSectionHeader ||
        $to.node().type == ingredientSchema.nodes.recipeInstruction))
        return false;
    if (dispatch) {
        let tr = state.tr;
        let contentAfter = ingredientSchema.node(ingredientSchema.nodes.recipeInstructionSection, null, [
            ingredientSchema.node(ingredientSchema.nodes.recipeInstructionSectionHeader, null, $to.pos == $to.end() ? null :
                ingredientSchema.text(tr.doc.textBetween($to.pos, $to.end()))),
            tr.doc.slice($to.after(), $to.after(2)-1).content.content[0]]);
        tr = tr.deleteSelection();
        tr = tr.delete(tr.mapping.map($to.pos), tr.mapping.map($to.end(2)));
        let insertPos = tr.mapping.map($to.after(2));
        tr = tr.insert(insertPos, contentAfter);
        tr = tr.setSelection(TextSelection.create(tr.doc, insertPos + 1));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function makeInstruction(state, dispatch, view) {
    let {$head} = state.selection;
    if ($head.node().type != ingredientSchema.nodes.recipeInstructionSectionHeader || $head.start(1) == $head.before(2)) return false
    if (dispatch) {
        let tr = state.tr;
        tr = tr.replaceRange($head.before(2)-3, $head.after()+2,
            new Slice(Fragment.from(ingredientSchema.nodes.recipeInstruction.create(null, $head.node().content.size == 0 ? null : ingredientSchema.text($head.node().textContent))),0,0)
        );
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function insertInstruction(state, dispatch, view) {
    let {$from, $to} = state.selection;
    if ($from.node().type != ingredientSchema.nodes.recipeInstructionSectionHeader &&
        $from.node().type != ingredientSchema.nodes.recipeInstruction) return false;
    if (dispatch) {
        let tr = state.tr;
        if ($from.node().type == ingredientSchema.nodes.recipeInstructionSectionHeader) {
            let textAfter = "";
            if ($to.pos != $to.end()) {
                textAfter = tr.doc.textBetween($to.pos, $to.end());
            }
            tr = tr.delete($from.pos, $to.end());
            let insPos = insertPoint(tr.doc, tr.mapping.map($from.after()+1), ingredientSchema.nodes.recipeInstruction);
            if (insPos == undefined) return false;
            tr = tr.insert(insPos, ingredientSchema.node(ingredientSchema.nodes.recipeInstruction, null, textAfter ? ingredientSchema.text(textAfter) : null));
            tr = tr.setSelection(TextSelection.create(tr.doc, insPos+1));
        } else {
            tr = tr.deleteSelection();
            tr = tr.split(tr.mapping.map($from.pos, 1, {type: $from.node().type, attrs: {item: null}}));
        }
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function mergeInstructionBackwardsToHeader(state, dispatch, view) {
    let {$head} = state.selection;
    if ($head.pos != $head.start()) return false;
    if ($head.node().type != ingredientSchema.nodes.recipeInstruction) return false;
    if (dispatch) {
        let tr = state.tr;
        let textAfter = tr.doc.textBetween($head.pos, $head.end());
        tr = tr.delete($head.before(), $head.after());
        tr = tr.insertText(textAfter, $head.pos - 3);
        tr = tr.setSelection(TextSelection.create(tr.doc, $head.pos - 3));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function mergeHeaderBackwardsToInstruction(state, dispatch, view) {
    let {$head} = state.selection;
    if ($head.pos != $head.start()) return false;
    if ($head.node().type != ingredientSchema.nodes.recipeInstructionSectionHeader) return false;
    if ($head.before(1) == 0) return false;
    if (dispatch) {
        let tr = state.tr;
        let textAfter = tr.doc.textBetween($head.pos, $head.end());
        tr = tr.delete($head.pos - 5, $head.after() + 1)
        tr = tr.insertText(textAfter, $head.pos - 5);
        tr = tr.setSelection(TextSelection.create(tr.doc, $head.pos - 5));
        dispatch(tr.scrollIntoView())
    }
    return true;
}

function insertVariation(state, dispatch, view) {
    let {$from, $to} = state.selection;
    let depth;
    for (depth = $to.depth; depth >= 0; depth--) {
        if ($to.node(depth).type == ingredientSchema.nodes.recipeVariationsBlock) break;
    }
    if (depth <= 0) return false;
    if (dispatch) {
        let tr = state.tr;
        let contentAfter = ingredientSchema.node(ingredientSchema.nodes.recipeVariation, null, [
            ingredientSchema.node(ingredientSchema.nodes.recipeVariationName, null, $to.pos == $to.end() ? null :
                ingredientSchema.text(tr.doc.textBetween($to.pos, $to.end()))),
            tr.doc.slice($to.after(), $to.after(2)-1).content.content[0]]);
        tr = tr.deleteSelection();
        tr = tr.delete(tr.mapping.map($to.pos), tr.mapping.map($to.end(2)));
        let insertPos = tr.mapping.map($to.after(2));
        tr = tr.insert(insertPos, contentAfter);
        tr = tr.setSelection(TextSelection.create(tr.doc, insertPos + 1));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function mergeVariationBackwardsToName(state, dispatch, view) {
    let {$head} = state.selection;
    if ($head.pos != $head.start()) return false;
    let depth;
    for (depth = $head.depth; depth >= 0; depth--) {
        if ($head.node(depth).type == ingredientSchema.nodes.recipeVariationContent) break;
    }
    if (depth <= 0) return false;
    if (dispatch) {
        let tr = state.tr;
        let textAfter = tr.doc.textBetween($head.pos, $head.end());
        tr = tr.delete($head.before(), $head.after());
        tr = tr.insertText(textAfter, $head.pos - 3);
        tr = tr.setSelection(TextSelection.create(tr.doc, $head.pos - 3));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function mergeNameBackwardsToVariation(state, dispatch, view) {
    let {$head} = state.selection;
    if ($head.pos != $head.start()) return false;
    if ($head.node().type != ingredientSchema.nodes.recipeVariationName) return false;
    if ($head.before(1) == 0) return false;
    if (dispatch) {
        let tr = state.tr;
        let textAfter = tr.doc.textBetween($head.pos, $head.end());
        tr = tr.delete($head.before(2) - 3, $head.after(3) + 1)
        tr = tr.insertText(textAfter, $head.pos - 5);
        tr = tr.setSelection(TextSelection.create(tr.doc, $head.pos - 5));
        dispatch(tr.scrollIntoView())
    }
    return true;
}

function moveFromVariationNameToContent(state, dispatch, view) {
    let {$from, $to} = state.selection;
    if ($from.node().type != ingredientSchema.nodes.recipeVariationName) return false;
    if (dispatch) {
        let tr = state.tr;
        let textAfter = "";
        if ($to.pos != $to.end()) {
            textAfter = tr.doc.textBetween($to.pos, $to.end());
        }
        tr = tr.delete($from.pos, $to.end());
        let insPos = insertPoint(tr.doc, tr.mapping.map($from.after()+1), ingredientSchema.nodes.paragraph);
        if (insPos == undefined) return false;
        tr = tr.insert(insPos, ingredientSchema.node(ingredientSchema.nodes.paragraph, null, textAfter ? ingredientSchema.text(textAfter) : null));
        tr = tr.setSelection(TextSelection.create(tr.doc, insPos+1));
        dispatch(tr.scrollIntoView());
    }
    return true;
}

function tabToNode(dir) {
    return function tabToNextNode(state, dispatch, view) {
        let {$head} = state.selection;

        // Walk up the document tree trying to find a depth where there's a next or previous node
        let depth = $head.depth;
        if (dir < 0) {
            while((depth >= 0) && ($head.index > 0)) depth--;
        } else {
            while((depth >= 0) && ($head.node(depth).childCount <= $head.index(depth) + 1)) depth--;
        }

        // If depth is less than 0, we got to the outermost node without there being a next or
        // previous node to go to. Return because there's nothing to do.
        if (depth < 0) return false;

        // Try to find the next position we can insert at
        let start, end;
        if (dir < 0) {
            start = 0;
            end = $head.before(depth);
        } else {
            start = $head.posAtIndex($head.index(depth)+1, depth);
            end = state.doc.nodeSize-2;
        }
        let new_cursor_pos;
        state.doc.nodesBetween(start, end, function(node, pos, parent, index) {
            if ((dir > 0) && (new_cursor_pos !== undefined)) return false;
            if (node.isTextblock) {
                // SPECIAL CASE: Don't tab into recipeTotalTime node
                if (node.type == ingredientSchema.nodes.recipeTotalTime) return false;
                new_cursor_pos = pos+1;
                return false;
            }
            return true;
        });

        // If no next cursor position was found, then return false, nothing to do
        if (new_cursor_pos === undefined) return false;

        if (dispatch) {
            let tr = state.tr;
            tr = tr.setSelection(TextSelection.create(state.doc, new_cursor_pos));
            tr = tr.scrollIntoView();
            dispatch(tr);
        }
        return true;
    }
}
