"""
URL/URI Configuration for Dish Source App
"""

from django.urls import include, path

from dish_source import views
from dish_source.routers import router

urlpatterns = [
    path('', views.index, name='index'),
    path('category/<int:category_id>/', views.category, name='category'),
    path('category/<int:category_id>/<slug:slug>', views.category, name='category'),
    path('recipe/<int:recipe_id>', views.recipe, name='recipe'),
    path('recipe/<int:recipe_id>/<slug:slug>', views.recipe, name='recipe'),
    path('recipe/new', views.recipe, name='new-recipe'),
    path('api/', include(router.urls)),
]
