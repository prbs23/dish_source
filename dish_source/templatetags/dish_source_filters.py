"""
Custom template filters for the dish_source app.
"""

from django import template

from dish_source.models import Recipe

register = template.Library()


@register.filter(name="is_recipe")
def is_recipe(obj):
    """
    Returns true if the given object is a Recipe model object, otherwise returns false.
    """
    return isinstance(obj, Recipe)
