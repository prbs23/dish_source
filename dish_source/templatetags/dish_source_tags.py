"""
Custom template tags for the dish_source app.
"""

from math import ceil

from django import template
from django.utils.safestring import mark_safe

from dish_source import utils

register = template.Library()


@register.simple_tag
def format_decimal(value):
    """Converts a decimal number to a fractional string (ex. 1 1/2)"""
    return utils.format_decimal(value)


@register.simple_tag
def format_quantity(quantity):
    """
    Format a quantity model object for rendering.
    This is easier to express in python code than in a django template file
    """
    result = ""
    if quantity.approx:
        result += "~"
    result += utils.format_decimal(quantity.min_quantity)
    if quantity.max_quantity:
        result += "-" + utils.format_decimal(quantity.max_quantity)
    if quantity.unit:
        result += ' <mark class="unit-name">'
        if quantity.unit.abbrev:
            result += quantity.unit.abbrev + "."
        elif quantity.min_quantity > 1 or (quantity.max_quantity and quantity.max_quantity > 1):
            result += quantity.unit.plural
        else:
            result += quantity.unit.name
        result += '</mark>'
    return mark_safe(result)


@register.simple_tag
def format_ingredient(ingredient):
    """
    Format an ingredient model object for rendering.
    This is easier to express in python code than in a django template file
    """
    result = ""
    plural_ingredient = False
    for idx, quantity in enumerate(ingredient.quantities.all()):
        if (idx == 0 and quantity.unit is None and
                ((quantity.min_quantity > 1) or
                 (quantity.max_quantity and quantity.max_quantity > 1))):
            plural_ingredient = True
        if idx == 1:
            result += "("
        result += format_quantity(quantity)
        if idx > 0:
            if idx == len(ingredient.quantities.all())-1:
                result += ") "
            else:
                result += ", "
        else:
            result += " "

    if ingredient.qualifier:
        result += ingredient.qualifier + " "
    result += '<mark class="ingredient-name">'
    if plural_ingredient:
        result += ingredient.ingredient.plural
    else:
        result += ingredient.ingredient.name
    result += '</mark>'
    if ingredient.processing:
        result += ", " + ingredient.processing

    return mark_safe(result)


def format_duration(duration):
    """
    Format a duration (in seconds) into a "X hr X min" string
    """
    result = ""
    duration = duration / 60.0
    if duration >= 60:
        result += f"{int(duration // 60):d} hr"
        if duration % 60:
            result += " "
    if duration % 60:
        result += f"{int(ceil(duration % 60)):d} min"
    return result


@register.simple_tag
def format_prep_duration(recipe):
    """
    Format the prep duration of the given recipe.
    """
    result = format_duration(recipe.prep_duration_min)
    if recipe.prep_duration_max:
        result += " - " + format_duration(recipe.prep_duration_max)
    return result


@register.simple_tag
def format_cook_duration(recipe):
    """
    Format the cook duration of the given recipe.
    """
    result = format_duration(recipe.cook_duration_min)
    if recipe.cook_duration_max:
        result += " - " + format_duration(recipe.cook_duration_max)
    return result


@register.simple_tag
def format_total_duration(recipe):
    """
    Format the total recipe duration of the given recipe.
    """
    result = format_duration((recipe.prep_duration_min if recipe.prep_duration_min else 0) +
                             (recipe.cook_duration_min if recipe.cook_duration_min else 0))
    if recipe.prep_duration_max or recipe.cook_duration_max:
        result += " - " + format_duration(
            (recipe.prep_duration_max if recipe.prep_duration_max else 0) +
            (recipe.cook_duration_max if recipe.cook_duration_max else 0)
        )
    return result
