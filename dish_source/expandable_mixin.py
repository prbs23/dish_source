"""
Custom "expandable serializer" mixin implementation.

Provides a "Mixin" for on the django-reset-framework serializer classes to add the ability to return
partial or full objects based on a query string. This is based on an API proposal from this blog
post: https://medium.com/quick-code/crud-app-using-vue-js-and-django-516edf4e4217 but somewhat
modified/simplified for the Dish Source use case.

The basic idea is that when the serialized object being returned by the API has nested objects or
lists, by default the child objects will only contain the id/pk field of the model, If the client
wants to get the full child objects, it includes an "expand" query string with the name of the
field that it want's the expanded data for.
"""

from rest_framework import serializers


class ExpandFieldsMixin:
    """
    Mixin class that adds expand query string behavior to any serializer

    When the query string does not request an expanded version of the object, only the fields in
    the base_fields class variable are serialized. By default, this only serializes the "id"
    field of the object, but this can be customized in the serializer by providing a different value
    for the base_fields variable.
    """
    base_fields = ['id']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if 'expand' in self.context:
            # If expand list already exist in the context, then don't overwrite it
            return

        if 'request' not in self.context:
            # If request object is not in the context, then just set the expand context to an empty
            # list
            self.context['expand'] = []
            return

        if self.context['request'].method != 'GET':
            # If the method of the request in the context is not GET, then set the expand context to
            # an empty list
            self.context['expand'] = []
            return

        if 'expand' not in self.context['request'].query_params:
            # If the query parameters of the context's request does not include expand, then set the
            # expand context to an empty list
            self.context['expand'] = []
            return

        # If the checks above haven't failed, then get the expand list from the request to
        # initialize the expand context
        self.context['expand'] = self.context['request'].query_params.getlist('expand')

    @property
    def _real_parent(self):
        if isinstance(self.parent, serializers.ListSerializer):
            return self.parent.parent
        return self.parent

    @property
    def _expand_path(self):
        path = self.field_name
        parent = self.parent
        while parent is not None:
            if parent.field_name:
                path = parent.field_name + ('.' if path else '') + path
            parent = parent.parent
        return path

    @property
    def _readable_fields(self):
        all_fields = (self._expand_path is None or
                      'expand' in self.context and
                      self._expand_path in self.context['expand'])

        for field in self.fields.values():
            if (not field.write_only) and (all_fields or field.field_name in self.base_fields):
                yield field
