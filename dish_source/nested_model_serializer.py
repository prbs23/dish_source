"""
Base type for model serializers that support being nested.

Subclasses of the NestedModelSerializer implement specific behaviors. A NestedModelSerializer
serializer class can include subclasses of the NestedModelSerializer, but cannot be directly
nested inside another NestedModelSerializer.
"""
import traceback

from rest_framework.serializers import BaseSerializer
from rest_framework.fields import empty
from rest_framework.utils.model_meta import get_field_info

from dish_source.base_serializer import DishSourceListSerializer, DishSourceModelSerializer


class NestedModelSerializer(DishSourceModelSerializer):
    """
    Base type for nested model serializers.
    """

    # Indicate if this nested serializer class should be created before or after the parent class.
    # If the deferred_create field is False, then the child serializer will be created before the
    # parent, and passed to the parent model. When True, the parent serializer will be created
    # before the child, and the created model will be passed to the child serializer.
    deferred_create = False

    def __new__(cls, *args, **kwargs):
        """
        Override the default list_serializer_class field of the Meta class to instance a
        NestedListSerializer instead of the default ListSerializer.
        """
        meta = getattr(cls, 'Meta', None)
        setattr(meta, 'list_serializer_class',
                getattr(meta, 'list_serializer_class', NestedListSerializer))
        return super().__new__(cls, *args, **kwargs)

    def _raise_errors_on_nested_writes(self, method_name, validated_data):
        """
        Give explicit errors when users attempt to pass writable nested data.

        Nested serializers that inherit from NestedModelSerializer are allowed, but throw errors
        for other serializer classes.
        """
        # pylint: disable=invalid-name
        ModelClass = getattr(self, "Meta").model
        model_field_info = get_field_info(ModelClass)

        # Ensure we don't have a writable nested field that don't inherit from NestedModelSerializer
        #
        # class UserSerializer(NestedModelSerializer):
        #     ...
        #     profile = ProfileSerializer()
        assert not any(
            isinstance(field, BaseSerializer) and
            not isinstance(field, (NestedListSerializer, NestedModelSerializer)) and
            (field.source in validated_data) and
            (field.source in model_field_info.relations) and
            isinstance(validated_data[field.source], (list, dict))
            for field in self._writable_fields
        ), (
            'The `.{method_name}()` method does not support writable nested '
            'fields by default, unless they are also inherit from NestedModelSerializer class.\n'
            'Write an explicit `.{method_name}()` method for '
            'serializer `{module}.{class_name}`, or set `read_only=True` on '
            'nested serializer fields.'.format(
                method_name=method_name,
                module=self.__class__.__module__,
                class_name=self.__class__.__name__
            )
        )

        # Ensure we don't have a writable dotted-source field. For example:
        #
        # class UserSerializer(ModelSerializer):
        #     ...
        #     address = serializer.CharField('profile.address')
        #
        # Though, non-relational fields (e.g., JSONField) are acceptable. For example:
        #
        # class NonRelationalPersonModel(models.Model):
        #     profile = JSONField()
        #
        # class UserSerializer(ModelSerializer):
        #     ...
        #     address = serializer.CharField('profile.address')
        assert not any(
            len(field.source_attrs) > 1 and
            (field.source_attrs[0] in validated_data) and
            (field.source_attrs[0] in model_field_info.relations) and
            isinstance(validated_data[field.source_attrs[0]], (list, dict))
            for field in self._writable_fields
        ), (
            'The `.{method_name}()` method does not support writable dotted-source '
            'fields by default.\nWrite an explicit `.{method_name}()` method for '
            'serializer `{module}.{class_name}`, or set `read_only=True` on '
            'dotted-source serializer fields.'.format(
                method_name=method_name,
                module=self.__class__.__module__,
                class_name=self.__class__.__name__
            )
        )

    def create_or_update_child(self, method_name, parent_serializer, validated_data,
                               parent_instance=None, **kwargs):
        """
        Entry method for creating or updating a model object from a parent serializer.

        Implementation must be provided by subclasses. Should return the created or updated child
        model.
        """
        raise NotImplementedError(
            "This method must be implemented by subclasses of the NestedModelSerializer class to"
            "provide an implementation specific behavior of updating or creating models nested"
            "inside a NestedModelSerializer."
        )

    def fixup_child_objects(self, method_name, validated_data):
        """
        Method for modifying created child objects or validated items before the model object
        is created for this serializer. Should return updated validated_data structure.
        By default nothing is done in this method.
        """
        # pylint: disable=unused-argument,no-self-use
        return validated_data

    def create(self, validated_data):
        """
        Create a new model object

        The implementation here is based on the base ModelSerializer implementation but supports
        nesting other NestedModelSerializers inside this serializer
        """
        self._raise_errors_on_nested_writes('create', validated_data)

        # pylint: disable=invalid-name
        ModelClass = getattr(self, "Meta").model
        info = get_field_info(ModelClass)

        deferred_fields = {}
        many_to_many = {}

        # Iterate through the serializer fields to either create child models or, defer many-to-many
        # fields or serializers marked with deferred create.
        for field in self._writable_fields:
            # Remove many-to-many relationships from validated_data.
            # They are not valid arguments to the default `.create()` method,
            # as they require that the instance has already been saved.
            try:
                if field.field_name in info.relations and \
                        info.relations[field.field_name].to_many and \
                        (field.field_name in validated_data):
                    many_to_many[field.field_name] = validated_data.pop(field.field_name)
            except KeyError as key_error:
                trace_back = traceback.format_exc()
                msg = (f"Associated model field not found found for '{field.field_name}'.\n"
                       f"Original exception was:{trace_back}")
                raise KeyError(msg) from key_error

            # Skip plain fields, they will be converted correctly when passed to create()
            if not isinstance(field, (NestedModelSerializer, NestedListSerializer)):
                continue

            # Check if this is a deferred field. If it is, then save off the validated data and
            # we will create/update the child model after creating this one.
            if field.deferred_create and field.field_name in validated_data:
                deferred_fields[field.field_name] = validated_data[field.field_name]

            # Get the validated data for this serializer field and update the validated data
            # with the deserialized model
            field_data = field.get_value(validated_data)
            if field_data is empty:
                # Skip empty fields
                continue
            validated_data[field.field_name] = field.create_or_update_child(
                "create", self, field_data)

        validated_data = self.fixup_child_objects("create", validated_data)

        # Create the new model object
        try:
            # pylint: disable=protected-access
            instance = ModelClass._default_manager.create(**validated_data)
        except TypeError as type_error:
            trace_back = traceback.format_exc()
            # pylint: disable=consider-using-f-string
            msg = (
                'Got a `TypeError` when calling `%s.%s.create()`. '
                'This may be because you have a writable field on the '
                'serializer class that is not a valid argument to '
                '`%s.%s.create()`. You may need to make the field '
                'read-only, or override the %s.create() method to handle '
                'this correctly.\nOriginal exception was:\n %s' %
                (
                    ModelClass.__name__,
                    # pylint: disable=protected-access
                    ModelClass._default_manager.name,
                    ModelClass.__name__,
                    # pylint: disable=protected-access
                    ModelClass._default_manager.name,
                    self.__class__.__name__,
                    trace_back
                )
            )
            raise TypeError(msg) from type_error

        # Save many-to-many relationships after the instance is created.
        if many_to_many:
            for field_name, value in many_to_many.items():
                field = getattr(instance, field_name)
                field.set(self.fields[field_name].create_or_update_child("create", self,
                                                                         value, instance))

        # Create or update deferred nested serializers
        for field_name, value in deferred_fields.items():
            self.fields[field_name].create_or_update_child("create", self, value, instance)

        return instance

    def update(self, instance, validated_data):
        """
        Update an existing model instance

        The implementation here is based on the base ModelSerializer implementation but supports
        nesting other NestedModelSerializers inside this serializer
        """
        self._raise_errors_on_nested_writes('update', validated_data)
        info = get_field_info(instance)

        for field in self._writable_fields:
            # Skip plain fields, they will be converted automatically
            if not isinstance(field, (NestedModelSerializer, NestedListSerializer)):
                continue

            # Get the validated data for this serializer field and update the validated data
            # with the deserialized model
            field_data = field.get_value(validated_data)
            if field_data is empty:
                # Skip empty fields
                continue
            validated_data[field.field_name] = field.create_or_update_child(
                'update', self, field_data, instance)

        validated_data = self.fixup_child_objects("update", validated_data)

        # Simply set each attribute on the instance, and then save it.
        # Note that unlike `.create()` we don't need to treat many-to-many
        # relationships as being a special case. During updates we already
        # have an instance pk for the relationships to be associated with.
        m2m_fields = []
        for attr, value in validated_data.items():
            if attr in info.relations and info.relations[attr].to_many:
                m2m_fields.append((attr, value))
            else:
                setattr(instance, attr, value)

        instance.save()

        # Note that many-to-many fields are set after updating instance.
        # Setting m2m fields triggers signals which could potentially change
        # updated instance and we do not want it to collide with .update()
        for attr, value in m2m_fields:
            field = getattr(instance, attr)
            field.set(value)

        return instance


class NestedListSerializer(DishSourceListSerializer):
    """
    Wrapper around the ListSerializer for supporting nested writable list serializers.
    """

    @property
    def deferred_create(self):
        """
        Indicate if this nested serializer class should be created before or after the parent class.
        If the deferred_create field is False, then the child serializer will be created before the
        parent, and passed to the parent model. When True, the parent serializer will be created
        before the child, and the created model will be passed to the child serializer.
        """
        return self.child.deferred_create

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        assert isinstance(self.child, NestedModelSerializer), (
            "Child serializer for NestedListSerializer must be a NestedModelSerializer"
        )

    def create_or_update_child(self, method_name, parent_serializer, validated_data,
                               parent_instance=None, **kwargs):
        # pylint: disable=unused-argument
        """
        Entry method for creating or updating a model object from a parent serializer.

        Simply calls the create_or_update method for all child items, and returns the
        resulting list.
        """

        assert isinstance(validated_data, list), (
            "validated_data argument to create_or_update_child method of NestedListSerializer "
            "must be a list."
        )

        child_models = []
        for validated_item in validated_data:
            child_models.append(
                self.child.create_or_update_child(method_name, parent_serializer, validated_item,
                                                  parent_serializer, **kwargs)
            )
        return child_models

    def update(self, instance, validated_data):
        raise NotImplementedError(
            "Serializers with many=True do not support multiple update by "
            "default, only multiple create. For updates it is unclear how to "
            "deal with insertions and deletions. If you need to support "
            "multiple update, use a `ListSerializer` class and override "
            "`.update()` so you can specify the behavior exactly."
        )
