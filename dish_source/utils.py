"""
Generic utility functions for Dish Source.
"""

from decimal import Decimal, localcontext
from itertools import count

from django.utils.text import slugify


def format_decimal(val):
    """Format a decimal value as stored into the database into a fractional string"""
    with localcontext() as decimal_context:
        decimal_context.prec = 4

        try:
            fraction = {
                Decimal('0.125'):  "⅛",
                Decimal('0.25'):   "¼",
                Decimal('0.3333'): "⅓",
                Decimal('0.375'):  "⅜",
                Decimal('0.5'):    "½",
                Decimal('0.625'):  "⅝",
                Decimal('0.6667'): "⅔",
                Decimal('0.75'):   "¾",
                Decimal('0.875'):  "⅞",
            }[val % 1]
            if val < 1:
                return fraction
            return f"{int(val):d}{fraction}"
        except KeyError:
            return f"{val:1.2f}".rstrip("0").rstrip(".")


def get_slug(name, model, slug_field_name="slug"):
    """
    Create a slug for the given name string.
    Uses the django slugify function to convert the name into a slug, then the given model is
    checked for conflicts. If there are conflicts a suffix integer is added, and incremented until
    an unused slug is found.
    """
    slug = slug_original = slugify(name)
    for suffix in count(1):
        if not model.objects.filter(**{slug_field_name: slug}).exists():
            break
        slug = f"{slug_original}-{suffix}"
    return slug
