"""
Django configuration of Dish Source app
"""

from django.apps import AppConfig


class DishSourceConfig(AppConfig):
    """Configuration object for Dish Source app"""
    name = 'dish_source'
    verbose_name = "Dish Source"
    default_auto_field = 'django.db.models.AutoField'
