"""
Serializezr wrapper class to allow serializers classes to include serializers that have not been
declared yet. Care must be taken with this class as it's possible for it to create an infinitely
recursive serializers that would eventually hit the call stack depth limit.
"""
from collections import OrderedDict

from django.core.exceptions import ImproperlyConfigured
from rest_framework.serializers import SerializerMetaclass
from rest_framework.fields import Field
from rest_framework.utils import representation


class ForwardDeclaredSerializer:
    """
    Forward declared serializer class. This class essentially behaves as a placeholder object until
    the lazily evaluated get_fields method is called on the parent serializer. This class holds
    all the required information about how the declared serializer should be created, so that it
    can be created later.
    """
    registered_serializers = {}

    def __init__(self, class_name, *args, **kwargs):
        self.class_name = class_name
        self.args = args
        self.kwargs = kwargs
        self._creation_counter = Field._creation_counter
        Field._creation_counter += 1

    @classmethod
    def register_serializer(cls, ser):
        """
        Register a serializer for use with forward declaration

        The function should be called with each serializer class that will be wrapped by a
        ForwardDeclaredSerializer object.
        """
        if ser.__name__ in cls.registered_serializers:
            raise ImproperlyConfigured(f"Expandable serializer '{ser.__name__}' already registered")
        cls.registered_serializers[ser.__name__] = ser


class ForwardDeclaredSerializerMetaclass(SerializerMetaclass):
    """
    Extends the SerializerMetaclass to include ForwardDeclaredSerializer fields in the
    _declared_fields attribute, even though they don't inherit from the Field base class.
    """

    @classmethod
    def _get_declared_fields(cls, bases, attrs):
        fields = [(field_name, attrs.pop(field_name))
                  for field_name, obj in list(attrs.items())
                  if isinstance(obj, (Field, ForwardDeclaredSerializer))]
        # pylint: disable=protected-access
        fields.sort(key=lambda x: x[1]._creation_counter)

        # Ensures a base class field doesn't override cls attrs, and maintains
        # field precedence when inheriting multiple parents. e.g. if there is a
        # class C(A, B), and A and B both define 'field', use 'field' from A.
        known = set(attrs)

        def visit(name):
            known.add(name)
            return name

        base_fields = [
            (visit(name), f)
            for base in bases if hasattr(base, '_declared_fields')
            # pylint: disable=protected-access
            for name, f in base._declared_fields.items() if name not in known
        ]

        return OrderedDict(base_fields + fields)


class ForwardDeclaredSerializerMixin(metaclass=ForwardDeclaredSerializerMetaclass):
    """
    Mixin class which extends the default get_fields method of a serializer to replace instances
    of a ForwardDeclaredSerializer with the forward declared type.
    This mixin must be used in any serializer which uses ForwardDeclaredSerializer fields.
    """

    _forward_declared_fields = {}

    def get_fields(self):
        """
        Returns a dictionary of {field_name: field_instance}.
        """
        for field_name in self._declared_fields.keys():
            if not isinstance(self._declared_fields[field_name], ForwardDeclaredSerializer):
                continue

            forward_inst = self._declared_fields[field_name]
            if forward_inst.class_name not in ForwardDeclaredSerializer.registered_serializers:
                raise ImproperlyConfigured(
                    f"Forward declared serializer '{forward_inst.class_name}' not registered")
            inst = ForwardDeclaredSerializer.registered_serializers[forward_inst.class_name](
                *forward_inst.args,
                **forward_inst.kwargs)

            self._declared_fields[field_name] = inst
            type(self)._forward_declared_fields[field_name] = forward_inst

        return super().get_fields()

    def __repr__(self):
        ret = representation.field_repr(self) + ':'
        indent_str = '    '

        for field_name, field in self.fields.items():
            ret += '\n' + indent_str + field_name + ' = '
            if field_name in self._forward_declared_fields:
                ret += "[Forward Declared] "
                if hasattr(field, 'child'):
                    ret += representation.field_repr(field.child)
                else:
                    ret += representation.field_repr(field)
            elif hasattr(field, 'fields'):
                ret += repr(field).replace("\n", "\n" + indent_str)
            elif hasattr(field, 'child'):
                ret += repr(field).replace("\n", "\n" + indent_str)
            elif hasattr(field, 'child_relation'):
                ret += representation.field_repr(field.child_relation,
                                                 force_many=field.child_relation)
            else:
                ret += representation.field_repr(field)

        if self.validators:
            ret += '\n' + indent_str + 'class Meta:'
            ret += ('\n' + indent_str + '    validators = ' +
                    representation.smart_repr(self.validators))

        return ret
