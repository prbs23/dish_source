"""
View classes for views
"""

from django.conf import settings
from django.http import HttpResponse, Http404
from django.template import loader
from django.shortcuts import redirect


from dish_source import models
from dish_source import serializers


def get_page_context(request):
    """
    Return the default page data structure required for rendering the page. If the page needs
    additional data (such as a recipe) the view function will add addtional data to this data
    structure.
    """
    return {
        'categories': models.Category.objects.all(),
        'recipe_tags': models.RecipeTag.objects.all(),
        'ingredient_tags': models.IngredientTag.objects.all(),
        'user_config': models.UserConfig.get_config_dict(request.user),
    }


def index(request):
    """Home page view"""
    template = loader.get_template("index.html")
    context = get_page_context(request)
    return HttpResponse(template.render(context, request))


def category(request, category_id, slug=None):
    """Category details view"""
    # pylint: disable=unused-argument

    try:
        category_inst = models.Category.objects.get(id=category_id)
    except models.Category.DoesNotExist as does_not_exist:
        raise Http404("Invalid category") from does_not_exist

    template = loader.get_template("category.html")
    context = get_page_context(request)
    context['category'] = category_inst
    return HttpResponse(template.render(context, request))


def recipe(request, recipe_id=None, slug=None):
    """Recipe detail view"""
    # pylint: disable=unused-argument

    if recipe_id is None:
        recipe_inst = None
    else:
        try:
            recipe_inst = models.Recipe.objects.get(id=recipe_id)
        except models.Recipe.DoesNotExist as does_not_exist:
            raise Http404("Invalid recipe") from does_not_exist

    if ("edit" in request.GET or recipe_inst is None) and not request.user.is_authenticated:
        return redirect(f"{settings.LOGIN_URL}?next={request.get_full_path()}")

    template = loader.get_template("recipe.html")
    context = get_page_context(request)
    context['recipe'] = recipe_inst
    context['new_recipe'] = recipe_id is None

    if request.method == "GET" and 'edit' in request.GET or recipe_id is None:
        context['categories_json'] = serializers.CategorySerializer(
            models.Category.objects.all(), context={'expand': [""]}, many=True).data
        context['recipe_json'] = serializers.RecipeSerializer(recipe_inst,
                                                              context={'expand': ['category']}).data
        context['recipe_tags_json'] = serializers.RecipeTagSerializer(
            models.RecipeTag.objects.all(), many=True).data
        context['units_json'] = serializers.UnitSerializer(
            models.Unit.objects.all(), many=True).data
        context['ingredients_json'] = serializers.IngredientSerializer(
            models.Ingredient.objects.all(), many=True).data
        context['sources_json'] = serializers.SourceSerializer(
            models.Source.objects.all(), many=True).data

    return HttpResponse(template.render(context, request))
