"""
Custom "ID or Content" nested model serializer

Subclasses NestedModelSerializer to allow child items to be specified either using all values of
the object, or just by providing a valid id field. Partial updates are also supported by specifying
the object's id field, as well as any optional other set of fields.
"""

from collections import OrderedDict
from collections.abc import Mapping
from django.core.exceptions import ValidationError as DjangoValidationError
from django.core.exceptions import ObjectDoesNotExist
from rest_framework.exceptions import ValidationError
from rest_framework.settings import api_settings
from rest_framework.fields import get_error_detail, SkipField, set_value
from rest_framework.utils.model_meta import get_field_info
from dish_source.nested_model_serializer import NestedModelSerializer


class IdOrContentSerializer(NestedModelSerializer):
    """
    NesetdModelSerializer subclass that provides "id or content" behavior if the serializer is
    nested inside another serializer. If this is a top level serializer, then normal rules for
    required fields apply.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.default_error_messages['pk_invalid'] =\
            "Could not find item with given public key value {pk}"

    def _validate_pk_field(self, data, ret, errors):
        """
        Find the public key field if present, and if so validate that it matches an
        existing model
        """
        pk_field = None
        if self.parent:
            field_info = get_field_info(getattr(self, "Meta").model)
            for field in self.fields.values():
                if field.source == field_info.pk.name and field.field_name in data and \
                        data[field.field_name]:
                    pk_field = field
                    break

        if pk_field is None:
            return None

        pk_value = pk_field.get_value(data)
        try:
            validated_pk_value = pk_field.to_internal_value(pk_value)
        except ValidationError as exc:
            errors[pk_field.field_name] = exc
        else:
            if validated_pk_value < 0:
                try:
                    stored_pk_value = self.context['created_pks'][type(self)][validated_pk_value]
                    if stored_pk_value is not None:
                        validated_pk_value = stored_pk_value
                except KeyError:
                    if 'created_pks' not in self.context:
                        self.context['created_pks'] = {}
                    if  type(self) not in self.context['created_pks']:
                        self.context['created_pks'][type(self)] = {}
                    self.context['created_pks'][type(self)][validated_pk_value] = None
            else:
                try:
                    self.instance = getattr(self, "Meta").model.objects.get(pk=validated_pk_value)
                except ObjectDoesNotExist:
                    self.fail('pk_invalid', pk=validated_pk_value)
            set_value(ret, pk_field.source_attrs, validated_pk_value)

        return pk_field

    def to_internal_value(self, data):
        """
        Dict of native values <- Dict of primitive datatypes.
        """
        if not isinstance(data, Mapping):
            message = self.error_messages['invalid'].format(
                datatype=type(data).__name__
            )
            raise ValidationError({
                api_settings.NON_FIELD_ERRORS_KEY: [message]
            }, code='invalid')

        ret = OrderedDict()
        errors = OrderedDict()

        # Check if the id field is present
        pk_field = self._validate_pk_field(data, ret, errors)

        fields = self._writable_fields
        for field in fields:
            validate_method = getattr(self, 'validate_' + field.field_name, None)
            primitive_value = field.get_value(data)
            try:
                validated_value = field.run_validation(primitive_value)
                if validate_method is not None:
                    validated_value = validate_method(validated_value)
            except ValidationError as exc:
                if not (pk_field is not None and
                        all((detail.code == 'required' for detail in exc.detail),)):
                    errors[field.field_name] = exc.detail
            except DjangoValidationError as exc:
                errors[field.field_name] = get_error_detail(exc)
            except SkipField:
                pass
            else:
                set_value(ret, field.source_attrs, validated_value)

        if errors:
            raise ValidationError(errors)

        return ret

    def create_or_update_child(self, method_name, parent_serializer, validated_data,
                               parent_instance=None, **kwargs):
        """
        Entry method for creating or updating a model object from a parent serializer.
        """

        if validated_data is None:
            return None

        # Find the pk field name. This is used to determine if we need to update or create a
        # model object.
        field_info = get_field_info(getattr(self, "Meta").model)
        pk_field = None
        for cur_field in self.fields.values():
            if cur_field.source == field_info.pk.name:
                pk_field = cur_field
                break

        assert (pk_field is not None), f"Could not get pk field for serializer: {self.field_name}"

        # Deserialize data values
        if pk_field.field_name in validated_data:
            # If the pk field value is a placeholder id, then check if a real id has been created
            # and populate it if so.
            if validated_data[pk_field.field_name] < 0:
                created_pk = self.context.get('created_pks', {}).get(type(self), {}).\
                    get(validated_data[pk_field.field_name], None)
                if created_pk is not None:
                    validated_data[pk_field.field_name] = created_pk

            if validated_data[pk_field.field_name] >= 0:
                # If we don't have a placeholder id then we can just update the model
                model_object = getattr(self, "Meta").model.objects.get(
                    pk=validated_data[pk_field.field_name])
                instance = self.update(model_object, validated_data)
            else:
                # Otherwise we need to create a new item and populate the context
                placeholder_pk = validated_data.pop(pk_field.field_name, None)
                instance = self.create(validated_data)
                if 'created_pks' not in self.context:
                    self.context['created_pks'] = {}
                if type(self) not in self.context['created_pks']:
                    self.context['created_pks'][type(self)] = {}
                self.context['created_pks'][type(self)][placeholder_pk] = instance.pk
        else:
            instance = self.create(validated_data)

        return instance
