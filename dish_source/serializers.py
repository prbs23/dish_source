"""
Model serializers for Dish Source REST API

Based on django-rest-framework serializer classes
"""

from rest_framework.fields import BooleanField, CharField, ImageField, SlugField
from rest_framework.serializers import ValidationError

from dish_source.nested_model_serializer import NestedModelSerializer
from dish_source.expandable_mixin import ExpandFieldsMixin
from dish_source.forward_declared_serializer import ForwardDeclaredSerializer
from dish_source.forward_declared_serializer import ForwardDeclaredSerializerMixin
from dish_source.id_or_content_serializer import IdOrContentSerializer
from dish_source.ordered_list_serializer import OrderedListItemSerializer
from dish_source import models


class IngredientTagSerializer(ExpandFieldsMixin, IdOrContentSerializer):
    """Serializer for Ingredient Tag Object"""
    slug = SlugField(required=False, allow_blank=True)
    base_fields = ["id", "name", "slug"]

    class Meta:
        model = models.IngredientTag
        fields = '__all__'


class RecipeTagSerializer(ExpandFieldsMixin, IdOrContentSerializer):
    """Serializer for Recipe Tag model object"""
    slug = SlugField(required=False, allow_blank=True)
    base_fields = ["id", "name", "slug"]

    class Meta:
        model = models.RecipeTag
        fields = '__all__'


class UnitSerializer(ExpandFieldsMixin, IdOrContentSerializer):
    """Serializer for Unit model object"""
    name = CharField(required=False, allow_null=True, allow_blank=True)
    plural = CharField(required=False, allow_null=True, allow_blank=True)
    base_fields = ["id", "name", "plural", "abbrev"]

    class Meta:
        model = models.Unit
        fields = "__all__"

    def validate(self, attrs):
        """
        Validate that either a name or plural name was given
        """
        if not self.partial and not attrs.get("id", -1) >= 0:
            if not attrs.get("name", None) and not attrs.get("plural", None):
                raise ValidationError("One of either 'name' or 'plural' fields must be given")

        return super().validate(attrs)


class SourceAuthorSerializer(ForwardDeclaredSerializerMixin, ExpandFieldsMixin,
                             IdOrContentSerializer):
    """Serializer for SourceAuthor model object"""
    recipes = ForwardDeclaredSerializer("RecipeSerializer", many=True, read_only=True)
    sources = ForwardDeclaredSerializer("SourceSerializer", many=True, read_only=True)
    slug = SlugField(required=False, allow_blank=True)
    base_fields = ["id", "name", "slug"]

    class Meta:
        model = models.SourceAuthor
        fields = '__all__'


class IngredientSerializer(ExpandFieldsMixin, IdOrContentSerializer):
    """Serializer for Ingredient model object"""
    name = CharField(required=False, allow_null=True, allow_blank=True)
    plural = CharField(required=False, allow_null=True, allow_blank=True)
    tags = IngredientTagSerializer(many=True, required=False)
    slug = SlugField(required=False, allow_blank=True)
    base_fields = ["id", "name", "plural", "slug", "tags"]

    class Meta:
        model = models.Ingredient
        fields = "__all__"

    def validate(self, attrs):
        """
        Validate that either a name or plural name was given
        """
        if not self.partial and not attrs.get("id", -1) >= 0:
            if not attrs.get("name", None) and not attrs.get("plural", None):
                raise ValidationError("One of either 'name' or 'plural' fields must be given")

        return super().validate(attrs)


class RecipeYieldSerializer(OrderedListItemSerializer):
    """Serializer for RecipeYield model object"""
    approx = BooleanField(required=False, default=False)
    unit = UnitSerializer(required=False, allow_null=True)

    class Meta:
        model = models.RecipeYield
        fields = ["approx", "min_quantity", "max_quantity", "unit"]


class SourceSerializer(ForwardDeclaredSerializerMixin, ExpandFieldsMixin, IdOrContentSerializer):
    """Serializer for the Source model object"""
    authors = SourceAuthorSerializer(many=True, allow_empty=True, required=False)
    recipes = ForwardDeclaredSerializer("RecipeSerializer", many=True, read_only=True)
    base_fields = ["id", "name", "url"]

    class Meta:
        model = models.Source
        fields = ["id", "name", "authors", "isbn", "url", "notes", "recipes"]

    def validate(self, attrs):
        """
        Validate that either a url or name has been specified
        """
        cur_name = ""
        cur_url = ""
        if self.partial:
            cur_name = self.instance.name
            cur_url = self.instance.url
        if 'id' in attrs and attrs['id'] >= 0:
            try:
                src = models.Source.objects.get(id=attrs['id'])
            except models.Source.DoesNotExist as does_not_exist:
                raise ValidationError("Invalid source id") from does_not_exist
            cur_name = src.name
            cur_url = src.url
        if attrs.get("name", cur_name) == "" and attrs.get("url", cur_url) == "":
            raise ValidationError("'name' and 'url' field of source cannot both be empty")

        return super().validate(attrs)


class RecipeIngredientQuantitySerializer(OrderedListItemSerializer):
    """Serializer for Recipe ingredient quantity model"""
    unit = UnitSerializer(allow_null=True)

    class Meta:
        model = models.RecipeIngredientQuantity
        fields = ['approx', 'min_quantity', 'max_quantity', 'unit']


class RecipeIngredientSubstitutionSerializer(ForwardDeclaredSerializerMixin,
                                             OrderedListItemSerializer):
    """Serializer for Recipe ingredient substitution model"""
    ingredients = ForwardDeclaredSerializer('RecipeIngredientSerializer',
                                            order_parent_field="substitution",
                                            many=True, allow_empty=False)

    class Meta:
        model = models.RecipeIngredientSubstitution
        fields = ['ingredients']

    def to_representation(self, instance):
        return self.fields['ingredients'].to_representation(instance.ingredients)

    def to_internal_value(self, data):
        return {'ingredients': self.fields['ingredients'].to_internal_value(data)}


class RecipeIngredientSerializer(OrderedListItemSerializer):
    """Serializer for Recipe ingredient model"""
    quantities = RecipeIngredientQuantitySerializer(many=True,
                                                    order_parent_field="recipe_ingredient")
    ingredient = IngredientSerializer()
    substitutions = RecipeIngredientSubstitutionSerializer(
        many=True, allow_empty=True, order_parent_field="substituted_ingredient")

    class Meta:
        model = models.RecipeIngredient
        fields = ['quantities', 'qualifier', 'ingredient', 'processing', 'substitutions']


class RecipeIngredientSectionSerializer(OrderedListItemSerializer):
    """Serializer Recipe ingredient section model"""
    ingredients = RecipeIngredientSerializer(many=True, order_parent_field="section")

    class Meta:
        model = models.RecipeIngredientSection
        fields = ['name', 'ingredients']


class RecipeInstructionSerializer(OrderedListItemSerializer):
    """Serializer for Recipe instruction model"""
    class Meta:
        model = models.RecipeInstruction
        fields = ['instruction']

    def to_representation(self, instance):
        return self.fields['instruction'].to_representation(instance.instruction)

    def to_internal_value(self, data):
        return {'instruction': self.fields['instruction'].to_internal_value(data)}


class RecipeInstructionSectionSerializer(OrderedListItemSerializer):
    """Serializer for Recipe instruction section model"""
    instructions = RecipeInstructionSerializer(many=True, order_parent_field="section")

    class Meta:
        model = models.RecipeInstructionSection
        fields = ['name', 'instructions']


class RecipeVariationSerializer(OrderedListItemSerializer):
    """Serializer for Recipe variation model"""
    class Meta:
        model = models.RecipeVariation
        fields = ['name', 'variation']


class RecipeSerializer(ForwardDeclaredSerializerMixin, ExpandFieldsMixin, IdOrContentSerializer):
    """Serializer for Recipe model object"""
    slug = SlugField(required=False, allow_blank=True)
    category = ForwardDeclaredSerializer('CategorySerializer', required=False, allow_null=True)
    subcategory = ForwardDeclaredSerializer('SubcategorySerializer', required=False,
                                            allow_null=True)
    source = ForwardDeclaredSerializer("SourceSerializer", required=False, allow_null=True)
    yields = RecipeYieldSerializer(many=True, order_parent_field="recipe")
    image = ImageField(required=False, allow_null=True)
    tags = RecipeTagSerializer(many=True, required=False)
    ingredient_sections = RecipeIngredientSectionSerializer(many=True,
                                                            order_parent_field="recipe")
    instruction_sections = RecipeInstructionSectionSerializer(many=True,
                                                              order_parent_field="recipe")
    variations = RecipeVariationSerializer(many=True, order_parent_field="recipe")
    base_fields = ["id", "name", "slug", "category", "subcategory", "tags"]

    def validate(self, attrs):
        """
        Validate that the category and subcategory fields are consistent.
        Because the subcategory must always be a subcategory of the given category, and new
        categories and subcategories can be created as part of a recipe request, there are a lot of
        different error conditions here.
        """
        if 'category' in attrs and attrs['category'] is None:
            del attrs['category']

        if 'category' not in attrs and 'subcategory' not in attrs and not self.partial:
            raise ValidationError("Either the category or subcategory fields are required")
        if 'category' in attrs and 'subcategory' in attrs:
            if 'id' not in attrs['category']:
                if 'id' not in attrs['subcategory']:
                    raise ValidationError("Invalid to add new category and subcategory in recipe "
                                          "request without ids")
                if attrs['subcategory']['id'] >= 0:
                    raise ValidationError("Invalid to create new category while also specifying "
                                          "an existing subcategory")
            if attrs['subcategory'] is not None and 'id' not in attrs['subcategory'] and \
                    'category' in attrs['subcategory']:
                if 'id' not in attrs['subcategory']['category']:
                    raise ValidationError("Invalid to assign new category to subcategory when "
                                          "existing category given for recipe")
                if attrs['category']['id'] != attrs['subcategory']['category']['id']:
                    raise ValidationError("Category and subcategory must be the same as "
                                          "category of recipe")
            elif attrs['subcategory'] is not None and 'id' in attrs['subcategory']:
                if 'category' not in attrs['subcategory'] and attrs['subcategory']['id'] >= 0:
                    try:
                        sub_cat = models.Subcategory.objects.get(id=attrs['subcategory']['id'])
                    except models.Subcategory.DoesNotExist as does_not_exist:
                        raise ValidationError("Invalid subcategory id") from does_not_exist
                    if attrs['category']['id'] != sub_cat.category.id:
                        raise ValidationError("Category and subcategory must be the same as "
                                              "category of recipe")
                elif 'category' in attrs['subcategory']:
                    if 'id' not in attrs['subcategory']['category']:
                        raise ValidationError("Invalid to assign new category to subcategory "
                                              "when existing category existing category given "
                                              "for recipe")
                    if attrs['category']['id'] != attrs['subcategory']['category']['id']:
                        raise ValidationError("Category and subcategory must be the same as "
                                              "category of recipe")
                else:
                    if 'id' not in attrs['category']:
                        raise ValidationError("Invalid for category to be unspecified in"
                                              "subcategory and recipe category to not have id")
                    attrs['subcategory']['category'] = {'id': attrs['category']['id']}

        return super().validate(attrs)

    def fixup_child_objects(self, method_name, validated_data):
        """
        If the category field is not given and the subcategory is, then populate the category field
        with the subcategory's category.
        """
        if 'category' not in validated_data and 'subcategory' in validated_data:
            validated_data['category'] = validated_data['subcategory'].category

        return super().fixup_child_objects(method_name, validated_data)

    class Meta:
        model = models.Recipe
        fields = '__all__'


class SubcategorySerializer(ForwardDeclaredSerializerMixin, ExpandFieldsMixin,
                            IdOrContentSerializer):
    """Serializer for Subcategory model object"""
    category = ForwardDeclaredSerializer('CategorySerializer')
    recipes = ForwardDeclaredSerializer('RecipeSerializer', many=True, read_only=True)
    slug = SlugField(required=False, allow_blank=True)

    base_fields = ["id", "name", "slug"]

    def validate(self, attrs):
        """"
        Validate that we're not trying to change the category of a subcategory after creation.
        """

        if self.partial and 'category' in attrs:
            if 'id' not in attrs['category']:
                raise ValidationError("Invalid to change category of subcategory after creation")

            try:
                cat = models.Subcategory.objects.get(id=attrs['category']['id'])
            except models.Category.DoesNotExist as does_not_exist:
                raise ValidationError("Invalid category id") from does_not_exist

            if self.instance.id != cat.id:
                raise ValidationError("Invalid to change category of subcategory after creation")

        return super().validate(attrs)

    class Meta:
        model = models.Subcategory
        fields = "__all__"


class CategorySerializer(ForwardDeclaredSerializerMixin, ExpandFieldsMixin, IdOrContentSerializer):
    """Serializer for Category model object"""
    subcategories = ForwardDeclaredSerializer('SubcategorySerializer', many=True, read_only=True)
    unsubcategorized_recipes = ForwardDeclaredSerializer('RecipeSerializer', many=True,
                                                         read_only=True)
    slug = SlugField(required=False, allow_blank=True)

    base_fields = ["id", "name", "slug"]

    class Meta:
        model = models.Category
        fields = "__all__"


class UnimportedRecipeSerializer(ExpandFieldsMixin, NestedModelSerializer):
    """Serializer for UnimportedRecipe model object"""
    text = CharField(required=True, allow_blank=True, style={'base_template': 'textarea.html'})
    source = SourceSerializer()

    base_fields = ["id", "text"]

    def create_or_update_child(self, method_name, parent_serializer, validated_data,
                               parent_instance=None, **kwargs):
        assert NotImplementedError("Nesting UnimportedRecipeSerializer inside another serializer "
                                   "is not supported")

    class Meta:
        model = models.UnimportedRecipe
        fields = "__all__"


ForwardDeclaredSerializer.register_serializer(SourceSerializer)
ForwardDeclaredSerializer.register_serializer(SubcategorySerializer)
ForwardDeclaredSerializer.register_serializer(CategorySerializer)
ForwardDeclaredSerializer.register_serializer(RecipeIngredientSerializer)
ForwardDeclaredSerializer.register_serializer(RecipeSerializer)
