"""
Base classes for all Dish Source serializers. These classes wrap the serializer classes from
the django-rest-framework.
"""

from rest_framework.serializers import ListSerializer, ModelSerializer
from rest_framework.utils import representation


class DishSourceListSerializer(ListSerializer):
    """Wrapper around ListSerializer class to provide an overridable repr() api."""
    def __repr__(self):
        if hasattr(self.child, 'fields'):
            return repr(self.child)
        return representation.field_repr(self)

    def update(self, instance, validated_data):
        raise NotImplementedError(
            "Serializers with many=True do not support multiple update by "
            "default, only multiple create. For updates it is unclear how to "
            "deal with insertions and deletions. If you need to support "
            "multiple update, use a `ListSerializer` class and override "
            "`.update()` so you can specify the behavior exactly."
        )


class DishSourceModelSerializer(ModelSerializer):
    """Wrapper around ModelSerializer class to provide an overridable repr() api"""
    def __new__(cls, *args, **kwargs):
        meta = getattr(cls, 'Meta', None)
        setattr(meta, 'list_serializer_class',
                getattr(meta, 'list_serializer_class', DishSourceListSerializer))
        return super().__new__(cls, *args, **kwargs)

    def __repr__(self):
        ret = representation.field_repr(self) + ':'
        indent_str = '    '

        for field_name, field in self.fields.items():
            ret += '\n' + indent_str + field_name + ' = '
            if hasattr(field, 'fields'):
                ret += repr(field).replace("\n", "\n" + indent_str)
            elif hasattr(field, 'child'):
                ret += repr(field).replace("\n", "\n" + indent_str)
            elif hasattr(field, 'child_relation'):
                ret += representation.field_repr(field.child_relation,
                                                 force_many=field.child_relation)
            else:
                ret += representation.field_repr(field)

        if self.validators:
            ret += '\n' + indent_str + 'class Meta:'
            ret += ('\n' + indent_str + '    validators = ' +
                    representation.smart_repr(self.validators))

        return ret
