"""
Viewsets for Dish Source REST API
"""

from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from dish_source import models
from dish_source import serializers


class CategoryViewSet(viewsets.ModelViewSet):
    """Category model viewset class"""
    queryset = models.Category.objects.all()
    serializer_class = serializers.CategorySerializer


class SubcategoryViewSet(viewsets.ModelViewSet):
    """Subcatategory model viewset class"""
    queryset = models.Subcategory.objects.all()
    serializer_class = serializers.SubcategorySerializer


class RecipeViewSet(viewsets.ModelViewSet):
    """Recipe model viewset class"""
    queryset = models.Recipe.objects.all()
    serializer_class = serializers.RecipeSerializer

    @action(detail=True, methods=["put", "patch", "delete"])
    def tag(self, request, *_args, **_kwargs):
        """
        Provide /tag URI to the recipe view for quickly modifying tags of a recipe.
        - A PUT request with a RecipeTag object will create a new tag and assign it to the recipe.
        - A PATCH request will assign an existing tag to the recipe.
        - A DELETE request will remove the given tag from the recipe.
        """
        instance = self.get_object()
        if request.method == "PUT":
            tag_serializer = serializers.RecipeTagSerializer(data=request.data)
            tag_serializer.is_valid(raise_exception=True)
            instance.tags.add(tag_serializer.save())
        elif request.method == "PATCH":
            try:
                instance.tags.add(models.RecipeTag.objects.get(pk=request.data['id']))
            except ObjectDoesNotExist:
                return Response({'id': "Invalid id value"}, status=status.HTTP_400_BAD_REQUEST)
            except KeyError:
                return Response({'id': "id field is required"}, status=status.HTTP_400_BAD_REQUEST)
        elif request.method == "DELETE":
            try:
                instance.tags.remove(models.RecipeTag.objects.get(pk=request.data['id']))
            except ObjectDoesNotExist:
                return Response({'id': "Invalid id value"}, status=status.HTTP_400_BAD_REQUEST)
            except KeyError:
                return Response({'id': "id field is required"}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializers.RecipeSerializer(instance).data)


class IngredientViewSet(viewsets.ModelViewSet):
    """Ingredient model viewset class"""
    queryset = models.Ingredient.objects.all()
    serializer_class = serializers.IngredientSerializer

    @action(detail=True, methods=["put", "patch", "delete"])
    def tag(self, request, *_args, **_kwargs):
        """
        Provide /tag URI to the recipe view for quickly modifying tags of an ingredient.
        - A PUT request with a RecipeTag object will create a new tag and assign it to the
          ingredient.
        - A PATCH request will assign an existing tag to the ingredient.
        - A DELETE request will remove the given tag from the ingredient.
        """
        instance = self.get_object()
        if request.method == "PUT":
            tag_serializer = serializers.IngredientTagSerializer(data=request.data)
            tag_serializer.is_valid(raise_exception=True)
            instance.tags.add(tag_serializer.save())
        elif request.method == "PATCH":
            try:
                instance.tags.add(models.IngredientTag.objects.get(pk=request.data['id']))
            except ObjectDoesNotExist:
                return Response({'id': "Invalid id value"}, status=status.HTTP_400_BAD_REQUEST)
            except KeyError:
                return Response({'id': "id field is required"}, status=status.HTTP_400_BAD_REQUEST)
        elif request.method == "DELETE":
            try:
                instance.tags.remove(models.IngredientTag.objects.get(pk=request.data['id']))
            except ObjectDoesNotExist:
                return Response({'id': "Invalid id value"}, status=status.HTTP_400_BAD_REQUEST)
            except KeyError:
                return Response({'id': "id field is required"}, status=status.HTTP_400_BAD_REQUEST)
        return Response(serializers.RecipeSerializer(instance).data)


class RecipeTagViewSet(viewsets.ModelViewSet):
    """RecipeTag model viewset class"""
    queryset = models.RecipeTag.objects.all()
    serializer_class = serializers.RecipeTagSerializer


class IngredientTagViewSet(viewsets.ModelViewSet):
    """IngredientTag model viewset class"""
    queryset = models.IngredientTag.objects.all()
    serializer_class = serializers.IngredientTagSerializer


class UnitViewSet(viewsets.ModelViewSet):
    """Unit model viewset class"""
    queryset = models.Unit.objects.all()
    serializer_class = serializers.UnitSerializer


class SourceAuthorViewSet(viewsets.ModelViewSet):
    """SourceAuthor model viewset class"""
    queryset = models.SourceAuthor.objects.all()
    serializer_class = serializers.SourceAuthorSerializer


class UnimportedRecipeViewSet(viewsets.ModelViewSet):
    """UnimportedRecipe model viewset class"""
    queryset = models.UnimportedRecipe.objects.all()
    serializer_class = serializers.UnimportedRecipeSerializer
