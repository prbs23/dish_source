"""
Django admin customizations for dish_source models.
"""

from django.contrib import admin
from nested_admin.nested import NestedModelAdmin, NestedStackedInline, NestedTabularInline
from dish_source import models


class CategoryAdmin(NestedModelAdmin):
    """
    Admin class for Category model.

    Customizations
    - Configure slug field to auto-populate based on the name field
    """
    model = models.Category
    prepopulated_fields = {"slug": ("name",)}


class SubcategoryAdmin(NestedModelAdmin):
    """
    Admin class for Subcategory model.

    Customizations
    - Configure slug field to auto-populate based on the name field
    """
    model = models.Subcategory
    prepopulated_fields = {"slug": ("name",)}


class RecipeTagAdmin(NestedModelAdmin):
    """
    Admin class for Recipe Tag model.

    Customizations
    - Configure slug field to auto-populate based on the name field
    """
    model = models.RecipeTag
    prepopulated_fields = {"slug": ("name",)}


class IngredientTagAdmin(NestedModelAdmin):
    """
    Admin class for Ingredient Tag model.

    Customizations
    - Configure slug field to auto-populate based on the name field
    """
    model = models.IngredientTag
    prepopulated_fields = {"slug": ("name",)}


class SourceAuthorAdmin(NestedModelAdmin):
    """
    Admin class for Source Author model.

    Customizations
    - Configure slug field to auto-populate based on the name field
    """
    model = models.SourceAuthor
    prepopulated_fields = {"slug": ("name",)}


class SourceAdmin(NestedModelAdmin):
    """
    Admin class for Source model.

    Customizations
    - Use "horizontal filter" field for "authors" many-to-many field instead of multi-select.
    """
    mode = models.Source
    filter_horizontal = ('authors',)


class UnitAdmin(NestedModelAdmin):
    """
    admin class for unit model.

    customizations
    - Configure name and abbrev as search fields so that other models can use autocomplete field
      for unit fields
    """
    model = models.Unit
    search_fields = ['name', 'abbrev']


class IngredientAdmin(NestedModelAdmin):
    """
    Admin class for Ingredient model.

    Customizations
    - Use "horizontal filter" field for "tags" many-to-many field instead of multi-select.
    - Configure search fields so that other models can use autocomplete field for ingredient fields
    - Configure slug field to auto-populate based on the name field
    """
    model = models.Ingredient
    filter_horizontal = ('tags',)
    search_fields = ['name']
    prepopulated_fields = {"slug": ("name",)}


class RecipeIngredientQuantityInline(NestedTabularInline):
    """
    Inline Admin class for Recipe Ingredient Quantity model.

    Customizations
    - Do not show any extra input fields for RecipeIngredientQuantity by default
    - Use autocomplete drop down field for unit so that new units will show up without a page reload
    """
    model = models.RecipeIngredientQuantity
    extra = 0
    autocomplete_fields = ['unit']


class RecipeIngredientAdmin(NestedModelAdmin):
    """
    Admin Class for Recipe Ingredient model.

    Customizations
    - Use inline input for ingredient quantity fields to allow recipe editing all on one page
    - Use autocomplete drop down field for ingredient field so that new ingredients will show up
      without requiring a page reload
    - Configure name as search fields so that other models can use autocomplete field for Recipe
      Ingredient fields
    """
    model = models.RecipeIngredient
    inlines = [RecipeIngredientQuantityInline]
    autocomplete_fields = ['ingredient']
    search_fields = ['ingredient__name']


class RecipeIngredientInline(NestedStackedInline):
    """
    Inline admin class for Recipe Ingredient model.

    Customizations
    - Do not show any extra input fields for RecipeIngredient by default
    - Use inline input for ingredient quantity fields to allow recipe editing all on one page
    - Use autocomplete drop down field for ingredient and section fields so that new values will
      show up without requiring a page reload
    - Configure ingredient.name as search fields so that other models can use autocomplete field
      for Recipe Ingredient fields, based on the name of the ingredient
    - Specify custom field ordering/grouping for aesthetics
    """
    model = models.RecipeIngredient
    inlines = [RecipeIngredientQuantityInline]
    extra = 0
    search_fields = ['ingredient__name']
    autocomplete_fields = ['ingredient']
    fields = ('order',
              'substitution',
              ('qualifier', 'ingredient', 'processing'))


class RecipeIngredientSubstitutionAdmin(NestedModelAdmin):
    """
    Admin Class for Recipe Ingredient Substitution model.

    Customizations
    - Use inline input for recipe ingredients field
    """
    model = models.RecipeIngredientSubstitution
    inlines = [RecipeIngredientInline]


class RecipeIngredientSectionAdmin(NestedModelAdmin):
    """
    Admin class for Recipe Ingredient Section model.

    Customizations
    - Inline input for recipe ingredient form
    """
    model = models.RecipeIngredientSection
    inlines = [
        RecipeIngredientInline
    ]


class RecipeIngredientSectionInline(NestedStackedInline):
    """
    Inline admin class for Recipe Ingredient Section model.

    Customizations
    - Inline input for recipe ingredient form
    - Don't show extra empty section inputs
    """
    model = models.RecipeIngredientSection
    inlines = [RecipeIngredientInline]
    extra = 0


class RecipeInstructionInline(NestedStackedInline):
    """
    Inline admin class for Recipe Ingredient model.

    Customizations
    - Specify custom field ordering/grouping for aesthetics
    """
    model = models.RecipeInstruction
    extra = 0
    fields = ('order', 'instruction')


class RecipeInstructionAdmin(NestedModelAdmin):
    """
    Admin class for Recipe Instruction model.

    Customizations
    - Specify custom field ordering/grouping for aesthetics
    """
    model = models.RecipeInstruction
    fields = ('order', 'instruction')


class RecipeInstructionSectionAdmin(NestedModelAdmin):
    """
    Admin Class for Recipe Instruction Section model.

    Customizations:
    - Inline input for recipe ingredient form
    """
    model = models.RecipeInstructionSection
    inlines = [
        RecipeInstructionInline
    ]


class RecipeInstructionSectionInline(NestedStackedInline):
    """
    Inline admin class for Recipe Instruction Section model.

    Customizations
    - Inline input for recipe instruction form
    - Don't show extra inputs for recipe section
    """
    model = models.RecipeInstructionSection
    inlines = [
        RecipeInstructionInline
    ]
    extra = 0


class RecipeVariationInline(NestedStackedInline):
    """
    Inline admin class for Recipe Variation model.

    Customizations
    - Do not show any extra input fields for RecipeIngredient by default
    - Specify custom field ordering/grouping for aesthetics
    """
    model = models.RecipeVariation
    extra = 0
    fields = (('order', 'name'), 'variation')


class RecipeYieldInline(NestedTabularInline):
    """
    Inline admin class for Recipe Instruction model.

    Customizations
    - Do not show any extra input fields for RecipeIngredient by default
    - Use autocomplete drop down field for unit field so that new units will show up without
      requiring a page reload
    """
    model = models.RecipeYield
    extra = 0
    autocomplete_fields = ['unit']


class RecipeAdmin(NestedModelAdmin):
    """
    Admin class for Recipe model.

    Customizations
    - Use inline input for yield, ingredients, instructions and variations fields to allow recipe
      editing all on one page
    - Use "horizontal filter" field for "tags" many-to-many field instead of multi-select.
    - Configure slug field to auto-populate based on the name field
    - Specify custom field ordering/grouping for aesthetics
    """
    model = models.Recipe
    fieldsets = (
        (None, {'fields': ('name', 'slug', ('category', 'subcategory'), 'source', 'tags')}),
        ('Recipe Duration', {
            'fields': (('prep_duration_min', 'prep_duration_max'),
                       ('cook_duration_min', 'cook_duration_max'))
        }),
        ('Content', {'fields': ('description', 'image', 'notes')}),
    )
    filter_horizontal = ('tags',)
    inlines = [
        RecipeYieldInline,
        RecipeIngredientSectionInline,
        RecipeInstructionSectionInline,
        RecipeVariationInline
    ]
    prepopulated_fields = {"slug": ("name",)}


# Register all dish_source models
admin.site.register(models.UserConfig)
admin.site.register(models.Source, SourceAdmin)
admin.site.register(models.SourceAuthor, SourceAuthorAdmin)
admin.site.register(models.UnimportedRecipe)
admin.site.register(models.Unit, UnitAdmin)
admin.site.register(models.Ingredient, IngredientAdmin)
admin.site.register(models.IngredientTag, IngredientTagAdmin)
admin.site.register(models.IngredientCalories)
admin.site.register(models.Category, CategoryAdmin)
admin.site.register(models.Subcategory, SubcategoryAdmin)
admin.site.register(models.RecipeTag, RecipeTagAdmin)
admin.site.register(models.RecipeYield)
admin.site.register(models.RecipeIngredientSection, RecipeIngredientSectionAdmin)
admin.site.register(models.RecipeIngredient, RecipeIngredientAdmin)
admin.site.register(models.RecipeIngredientQuantity)
admin.site.register(models.RecipeInstructionSection, RecipeInstructionSectionAdmin)
admin.site.register(models.RecipeIngredientSubstitution, RecipeIngredientSubstitutionAdmin)
admin.site.register(models.RecipeInstruction, RecipeInstructionAdmin)
admin.site.register(models.RecipeVariation)
admin.site.register(models.Recipe, RecipeAdmin)
