"""
Unit tests for Dish Source models

Model classes for dish source do not have a huge amount of logic in them. The unit testing in this
library only really targets models that have functionality outside the default Django provided
model API.
"""
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError
from django.db import transaction
from django.test import TestCase

from dish_source import models


class UnitModelTests(TestCase):
    """
    Unit tests for Unit model class
    """

    def setUp(self):
        """Set up models for example tests"""
        models.Unit.objects.create(name="tablespoon", abbrev="tbsp")
        models.Unit.objects.create(name="cup")

    def test_unit_abbreviation(self):
        """Test that a unit with an abbreviation results in the abbreviated name if there is one."""
        tbsp_unit = models.Unit.objects.get(name="tablespoon")
        cup_unit = models.Unit.objects.get(name="cup")
        self.assertEqual(str(tbsp_unit), "tbsp.")
        self.assertEqual(str(cup_unit), "cup")

    def test_unit_normalize(self):
        """Test the normalization behaviors of the Unit model class."""
        # Capitalized names should be converted to lower case, and plural generated
        unit = models.Unit.objects.create(name="Cup")
        self.assertEqual(unit.name, "cup")
        self.assertEqual(unit.plural, "cups")

        # If only the plural is given, then the singular name should be predicted
        unit = models.Unit.objects.create(plural="cups")
        self.assertEqual(unit.name, "cup")
        self.assertEqual(unit.plural, "cups")

        # If both name and plural are given they should be used be directly
        unit = models.Unit.objects.create(name="tablespoon", plural="cups")
        self.assertEqual(unit.name, "tablespoon")
        self.assertEqual(unit.plural, "cups")

        # If neither name or plural are given, a validation error should be thrown
        with transaction.atomic():
            self.assertRaises(ValidationError, models.Unit.objects.create, abbrev="l.")


class IngredientModelTests(TestCase):
    """
    Unit tests for Ingredient model class
    """

    def test_ingredient_normalize(self):
        """Test that the plural field handling for the ingredient model works correctly."""
        # If the plural field is not provided then the one should be computed
        ingredient = models.Ingredient.objects.create(name="First Ingredient")
        self.assertEqual(ingredient.name, "First Ingredient")
        self.assertEqual(ingredient.plural, "First Ingredients")

        # If the plural field is provided but name is missing, then singular name should be figured
        # computed from plural.
        ingredient = models.Ingredient.objects.create(plural="Second Ingredients")
        self.assertEqual(ingredient.name, "Second Ingredient")
        self.assertEqual(ingredient.plural, "Second Ingredients")

        # If both name and plural are given, then they should be saved directly
        ingredient = models.Ingredient.objects.create(name="Third Ingredient",
                                                      plural="Forth Ingredients")
        self.assertEqual(ingredient.name, "Third Ingredient")
        self.assertEqual(ingredient.plural, "Forth Ingredients")

        # If neither name or plural are given, then and error should be thrown
        with transaction.atomic():
            self.assertRaises(ValidationError, models.Ingredient.objects.create)


class SourceAuthorModelTests(TestCase):
    """
    Unit tests for SourceAuthor model
    """

    def setUp(self):
        """Creates a minimal source_author/recipe/source model for testing SourceAuthor model"""
        self.author1 = models.SourceAuthor.objects.create(name="Test Author1", slug="test_author1")
        self.author2 = models.SourceAuthor.objects.create(name="Test Author2", slug="test_author2")
        self.author3 = models.SourceAuthor.objects.create(name="Test Author2", slug="test_author3")
        self.recipe1 = models.Recipe.objects.create(
            name="Recipe 1",
            slug="recipe-1",
            category=models.Category.objects.create(name="Test Category", slug="test-category"),
            source=models.Source.objects.create(name="Test Source 1")
        )
        self.recipe1.source.authors.add(self.author1)
        self.recipe2 = models.Recipe.objects.create(
            name="Recipe 2",
            slug="recipe-2",
            category=self.recipe1.category,
            source=models.Source.objects.create(name="Test Source 2")
        )
        self.recipe2.source.authors.add(self.author1)
        self.recipe3 = models.Recipe.objects.create(
            name="Recipe 3",
            slug="recipe-3",
            category=self.recipe1.category,
            source=models.Source.objects.create(name="Test Source 3")
        )
        self.recipe3.source.authors.add(self.author2)
        self.recipe3.source.authors.add(self.author3)

    def test_author_model_recipes(self):
        """
        Test that the recipes property of the SourceAuthor model returns the correct list of
        recipes
        """
        self.assertEqual(self.author1.recipes, [self.recipe1, self.recipe2])
        self.assertEqual(self.author2.recipes, [self.recipe3])
        self.assertEqual(self.author3.recipes, [self.recipe3])


class UserConfigModelTests(TestCase):
    """
    Unit tests for UserConfig model
    """

    def setUp(self):
        """Create user and unit models to have UserConfigs associated with"""
        self.user0 = models.User.objects.create_user("user0")
        self.user1 = models.User.objects.create_user("user1")
        self.user2 = models.User.objects.create_user("user2")
        self.unit0 = models.Unit.objects.create(name="unit0")
        self.unit1 = models.Unit.objects.create(name="unit1")

    def test_user_config_model_unique_user(self):
        """
        Test that only one UserConfig model can be associated with a user model
        """

        # Create a user and associated config
        user3 = models.User.objects.create_user("user3")
        config3 = models.UserConfig.objects.create(user=user3, default_yield_unit=self.unit0)

        # Creating yet another config associated with user3 should be an IntegrityError
        with transaction.atomic():
            self.assertRaises(IntegrityError, models.UserConfig.objects.create,
                              user=user3, default_yield_unit=self.unit1)

        # When we delete user3, config3_0 should also be deleted
        user3.delete()
        self.assertRaises(models.UserConfig.DoesNotExist, models.UserConfig.objects.get,
                          id=config3.id)

    def test_user_config_model_global_default(self):
        """
        Test the unique global_default constraints on the UserConfig model
        """

        # To start with, we should be able to create a new globally default user config
        config0 = models.UserConfig.objects.create(user=self.user0, global_default=True,
                                                   default_yield_unit=self.unit0)

        # Trying to create another config with global_default should raise an integrity error
        # Note: Create call wrapped in transaction to ensure proper rollback on a failure.
        with transaction.atomic():
            self.assertRaises(IntegrityError, models.UserConfig.objects.create,
                              user=self.user1, global_default=True, default_yield_unit=self.unit1)

        # However we should be able to create multiple configs with global_default set to false
        config1 = models.UserConfig.objects.create(user=self.user1, default_yield_unit=self.unit1)
        config2 = models.UserConfig.objects.create(user=self.user2, default_yield_unit=self.unit0)

        # Check that global_default correctly defaulted to False
        self.assertFalse(config1.global_default)
        self.assertFalse(config2.global_default)

        # If we update config0, setting global_default to False, then we should be able to make
        # another config default
        config0.global_default = False
        config0.save()
        config2.global_default = True
        config2.save()

        # But updating a node to be global default, if there's already a default should raise
        # an IntegrityException
        with transaction.atomic():
            config1.global_default = True
            self.assertRaises(IntegrityError, config1.save)

    def test_user_config_model_get_config_dict(self):
        """
        Test the get_config_dict classmethod of the UserConfig model.
        """

        # Create some new config objects to query
        models.UserConfig.objects.create(user=self.user0, global_default=True,
                                         default_yield_unit=self.unit0)
        models.UserConfig.objects.create(user=self.user1, default_yield_unit=self.unit1)

        # Just getting config_dict of the two users we associated a config with should just return
        # the settings that were set.
        self.assertEqual(models.UserConfig.get_config_dict(self.user0),
                         {"default_yield_unit": self.unit0.id})
        self.assertEqual(models.UserConfig.get_config_dict(self.user1),
                         {"default_yield_unit": self.unit1.id})

        # Calling get_config_dict on user without a config should return the settings from the
        # global_default
        self.assertEqual(models.UserConfig.get_config_dict(self.user2),
                         {"default_yield_unit": self.unit0.id})

        # Calling get_config_dict with a null user should also return the global default
        self.assertEqual(models.UserConfig.get_config_dict(None),
                         {"default_yield_unit": self.unit0.id})

        # Calling with an AnonymousUser instance should also return the global default
        self.assertEqual(models.UserConfig.get_config_dict(models.AnonymousUser()),
                         {"default_yield_unit": self.unit0.id})


class CategoryModelTests(TestCase):
    """
    Unit tests for Category model
    """

    def setUp(self):
        """Create category, subcategories, and recipes to test against"""
        self.category = models.Category.objects.create(name="Test Category")

        # Create some subcategories
        self.subcategory_b = models.Subcategory.objects.create(name="b Test Subcategory",
                                                               category=self.category)
        self.subcategory_g = models.Subcategory.objects.create(name="g Test Subcategory",
                                                               category=self.category)

        # Create some recipes, both in and outside of subcategories. Items appear in expected order
        self.recipe_a = models.Recipe.objects.create(name="a Test Recipe", category=self.category)
        self.recipe_x = models.Recipe.objects.create(name="x Test Recipe", category=self.category,
                                                     subcategory=self.subcategory_b)
        self.recipe_z = models.Recipe.objects.create(name="z Test Recipe", category=self.category,
                                                     subcategory=self.subcategory_b)
        self.recipe_d = models.Recipe.objects.create(name="d Test Recipe", category=self.category)
        self.recipe_c = models.Recipe.objects.create(name="c Test Recipe", category=self.category,
                                                     subcategory=self.subcategory_g)
        # Empty subcategory to be filtered out
        self.subcategory_m = models.Subcategory.objects.create(name="m Test Subcategory",
                                                               category=self.category)

    def test_unsubcategorized_recipes_property(self):
        """
        Test that the unsubcategorized_recipes property returns the correct items
        """

        # Check that the returned list is the items we added in the correct order
        self.assertListEqual(list(self.category.unsubcategorized_recipes),
                             [self.recipe_a, self.recipe_d])

    def test_get_all_recipes(self):
        """
        Test that the get_all_recipes method returns a mixed list of unsubcategorized recipes
        and subcategories in alphabetical order.
        """

        # First check the default case where empty subcategories are filtered
        self.assertListEqual(self.category.get_all_recipes(),
                             [self.recipe_a,
                              self.subcategory_b,
                              self.recipe_d,
                              self.subcategory_g])

        # If including empty subcategories, then subcategory_m should appear in the list
        self.assertListEqual(self.category.get_all_recipes(include_empty_subcategories=True),
                             [self.recipe_a,
                              self.subcategory_b,
                              self.recipe_d,
                              self.subcategory_g,
                              self.subcategory_m])

        # Test all_recipes property
        self.assertListEqual(self.category.all_recipes,
                             [self.recipe_a,
                              self.subcategory_b,
                              self.recipe_d,
                              self.subcategory_g])
