"""
Unit Tests for UnitSerializer
"""

from django.test import TestCase
from dish_source import models
from dish_source import serializers


class UnitSerializerTests(TestCase):
    """
    Unit Serializer class unit tests
    """

    def setUp(self):
        """Create a Unit entry for updating"""
        models.Unit.objects.create(name="Cup", abbrev=None)

    def test_unit_serialize(self):
        """Test unit serialization"""
        tag = models.Unit(id=1, name="teaspoon", plural="teaspoons", abbrev="tsp")
        self.assertEqual(serializers.UnitSerializer(tag).data, {
            'id': 1,
            'name': "teaspoon",
            'plural': "teaspoons",
            'abbrev': "tsp"
        })

        tag = models.Unit(id=2, name="Cup", plural="Cups", abbrev=None)
        self.assertEqual(serializers.UnitSerializer(tag).data, {
            'id': 2,
            'name': "Cup",
            'plural': "Cups",
            'abbrev': None
        })

        # Serialize a list of units without expanding
        self.assertEqual(serializers.UnitSerializer([tag], many=True).data, [{
            'id': 2,
            'name': "Cup",
            'plural': "Cups",
            'abbrev': None
        }])

    def test_unit_validation(self):
        """Test unit validation"""
        # Valid unit with just name
        serializer = serializers.UnitSerializer(data={
            'name': "Tablespoon",
            'plural': None,
            'abbrev': "tbsp"
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer = serializers.UnitSerializer(data={
            'name': "Tablespoon",
            'plural': "",
            'abbrev': "tbsp"
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer = serializers.UnitSerializer(data={
            'name': "Tablespoon",
            'abbrev': "tbsp"
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # Valid unit with just plural
        serializer = serializers.UnitSerializer(data={
            'plural': "Tablespoons",
            'name': None,
            'abbrev': "tbsp"
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer = serializers.UnitSerializer(data={
            'plural': "Tablespoons",
            'name': "",
            'abbrev': "tbsp"
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer = serializers.UnitSerializer(data={
            'plural': "Tablespoons",
            'abbrev': "tbsp"
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # Either name or plural field is required
        serializer = serializers.UnitSerializer(data={
            'abbrev': "tbsp"
        })
        self.assertEqual(serializer.is_valid(), False)

        # However the abbrev field is not required
        serializer = serializers.UnitSerializer(data={
            'name': "Tablespoon"
        })
        self.assertEqual(serializer.is_valid(), True)

        # The name and plural fields aren't required for a partial update
        serializer = serializers.UnitSerializer(
            models.Unit.objects.get(id=1),
            partial=True,
            data={
                'abbrev': "tbsp"
            })
        self.assertEqual(serializer.is_valid(), True)

    def test_unit_create(self):
        """Test creating a new unit model"""
        serializer = serializers.UnitSerializer(data={
            'name': "Teaspoon",
            'abbrev': "tsp"
        })
        self.assertEqual(serializer.is_valid(), True)
        unit = serializer.save()
        self.assertTrue(unit.id is not None)
        self.assertEqual(unit.name, "teaspoon")
        self.assertEqual(unit.abbrev, "tsp")

    def test_unit_update(self):
        """Test updating unit models"""
        # Full model update
        serializer = serializers.UnitSerializer(
            models.Unit.objects.get(id=1),
            data={
                'name': "Tablespoon",
                'abbrev': "tbsp."
            })
        self.assertEqual(serializer.is_valid(), True)
        serializer.save()
        self.assertEqual(models.Unit.objects.get(id=1).name, "tablespoon")
        self.assertEqual(models.Unit.objects.get(id=1).abbrev, "tbsp")

        # Partial model update
        serializer = serializers.UnitSerializer(
            models.Unit.objects.get(id=1),
            partial=True,
            data={
                'abbrev': None
            })
        self.assertEqual(serializer.is_valid(), True)
        serializer.save()
        self.assertEqual(models.Unit.objects.get(id=1).name, "tablespoon")
        self.assertEqual(models.Unit.objects.get(id=1).abbrev, None)
