"""
Unit Tests for IngredientSerializer
"""

from django.test import TestCase
from dish_source import models
from dish_source import serializers


class IngredientSerializerTests(TestCase):
    """
    Ingredient Serializer class unit tests
    """

    def setUp(self):
        """
        Creates a few ingredient entries to test the serializers with
        """
        self.tag1 = models.IngredientTag.objects.create(name="Tag1", slug="tag1")
        self.tag2 = models.IngredientTag.objects.create(name="Tag2", slug="tag2")
        self.ingredient1 = models.Ingredient.objects.create(
            name="Test Ingredient1",
            slug="test-ingredient1",
            description="Ingredient Description 1",
            usda_number=1234
        )
        self.ingredient1.tags.add(self.tag1)
        self.ingredient1.tags.add(self.tag2)
        self.ingredient2 = models.Ingredient.objects.create(
            name="Test Ingredient2",
            slug="test-ingredient2",
            description="Ingredient Description 2",
            usda_number=5678
        )

    def test_ingredient_serialize(self):
        """Test Ingredient serialization"""

        # serialize Ingredient
        self.assertEqual(serializers.IngredientSerializer(self.ingredient1).data, {
            'id': self.ingredient1.id,
            'name': "Test Ingredient1",
            'plural': "Test Ingredient1s",
            'slug': "test-ingredient1",
            'description': "Ingredient Description 1",
            'usda_number': 1234,
            'tags': [
                {'id': self.tag1.id, 'name': "Tag1", 'slug': "tag1"},
                {'id': self.tag2.id, 'name': "Tag2", 'slug': "tag2"}
            ]
        })

        # serialize list of Ingredients
        self.assertEqual(
            serializers.IngredientSerializer([self.ingredient1, self.ingredient2], many=True).data,
            [
                {
                    'id': self.ingredient1.id,
                    'name': "Test Ingredient1",
                    'plural': "Test Ingredient1s",
                    'slug': "test-ingredient1",
                    'tags': [
                        {'id': self.tag1.id, 'name': "Tag1", 'slug': "tag1"},
                        {'id': self.tag2.id, 'name': "Tag2", 'slug': "tag2"}
                    ]
                },
                {
                    'id': self.ingredient2.id,
                    'name': "Test Ingredient2",
                    'plural': "Test Ingredient2s",
                    'slug': "test-ingredient2",
                    'tags': []
                }
            ]
        )

        # serialize expanded list of Ingredients
        self.assertEqual(
            serializers.IngredientSerializer([self.ingredient1, self.ingredient2],
                                             many=True, context={'expand': ['']}).data,
            [
                {
                    'id': self.ingredient1.id,
                    'name': "Test Ingredient1",
                    'plural': "Test Ingredient1s",
                    'slug': "test-ingredient1",
                    'description': "Ingredient Description 1",
                    'usda_number': 1234,
                    'tags': [
                        {'id': self.tag1.id, 'name': "Tag1", 'slug': "tag1"},
                        {'id': self.tag2.id, 'name': "Tag2", 'slug': "tag2"}
                    ]
                },
                {
                    'id': self.ingredient2.id,
                    'name': "Test Ingredient2",
                    'plural': "Test Ingredient2s",
                    'slug': "test-ingredient2",
                    'description': "Ingredient Description 2",
                    'usda_number': 5678,
                    'tags': []
                }
            ]
        )

    def test_ingredient_validation(self):
        """Test Ingredient validation"""
        # Valid ingredient with no tag
        serializer = serializers.IngredientSerializer(data={
            'name': "Test Ingredient 3",
            'plural': "Test Ingredients 3",
            'slug': "test-ingredient-3",
            'description': "This is an ingredient description",
            'usda_number': 9012,
            'tags': []
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # The slug, description, USDA number, and tag fields are not required
        serializer = serializers.IngredientSerializer(data={
            'name': "Test Ingredient 3",
            'plural': None,
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer = serializers.IngredientSerializer(data={
            'name': "Test Ingredient 3",
            'plural': "",
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer = serializers.IngredientSerializer(data={
            'name': "Test Ingredient 3",
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer = serializers.IngredientSerializer(data={
            'plural': "Test Ingredient 3",
            'name': None,
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer = serializers.IngredientSerializer(data={
            'plural': "Test Ingredient 3",
            'name': "",
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer = serializers.IngredientSerializer(data={
            'plural': "Test Ingredient 3",
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # Name isn't required if plural is included
        serializer = serializers.IngredientSerializer(data={
            'plural': "Test Ingredients 3",
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # Valid ingredient with tags
        serializer = serializers.IngredientSerializer(data={
            'name': "Test Ingredient 3",
            'slug': "test-ingredient-3",
            'description': "This is an ingredient description",
            'usda_number': 9012,
            'tags': [
                {'id': self.tag1.id},
                {'name': "Tag 3", 'slug': "tag-3"}
            ]
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # An invalid ingredient that doesn't include name or plural, even if everything else
        # is included.
        serializer = serializers.IngredientSerializer(data={
            'slug': "test-ingredient-3",
            'description': "This is an ingredient description",
            'usda_number': 9012,
            'tags': [
                {'id': self.tag1.id},
                {'name': "Tag 3", 'slug': "tag-3"}
            ]
        })
        self.assertEqual(serializer.is_valid(), False)

    def test_ingredient_create(self):
        """Test creating new ingredient models"""

        # Create an ingredient without any tags
        serializer = serializers.IngredientSerializer(data={
            'name': "Test Ingredient 3",
            'slug': "test-ingredient-3",
            'description': "This is an ingredient description",
            'usda_number': 9012,
            'tags': []
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        ingredient = serializer.save()
        self.assertEqual(ingredient.name, "Test Ingredient 3")
        self.assertEqual(ingredient.slug, "test-ingredient-3")
        self.assertEqual(ingredient.description, "This is an ingredient description")
        self.assertEqual(ingredient.usda_number, 9012)
        self.assertEqual(len(ingredient.tags.all()), 0)

        # Create an ingredient with a tag that already exists
        serializer = serializers.IngredientSerializer(data={
            'name': "Test Ingredient 4",
            'slug': "test-ingredient-4",
            'description': "This is an ingredient description 4",
            'usda_number': 3456,
            'tags': [
                {'id': self.tag1.id},
                {'id': self.tag2.id}
            ]
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        ingredient = serializer.save()
        self.assertEqual(ingredient.name, "Test Ingredient 4")
        self.assertEqual(ingredient.slug, "test-ingredient-4")
        self.assertEqual(ingredient.description, "This is an ingredient description 4")
        self.assertEqual(ingredient.usda_number, 3456)
        self.assertEqual(list(ingredient.tags.all()), [self.tag1, self.tag2])

        # Create an ingredient with a new tag
        serializer = serializers.IngredientSerializer(data={
            'name': "Test Ingredient 5",
            'slug': "test-ingredient-5",
            'description': "This is an ingredient description 5",
            'usda_number': 7890,
            'tags': [
                {'name': "Tag3", 'slug': "tag3"}
            ]
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        ingredient = serializer.save()
        self.assertEqual(ingredient.name, "Test Ingredient 5")
        self.assertEqual(ingredient.slug, "test-ingredient-5")
        self.assertEqual(ingredient.description, "This is an ingredient description 5")
        self.assertEqual(ingredient.usda_number, 7890)
        self.assertEqual(len(ingredient.tags.all()), 1)
        tag3 = list(ingredient.tags.all())[0]
        self.assertEqual(tag3.name, "Tag3")
        self.assertEqual(tag3.slug, "tag3")

        # Create an ingredient, updating an existing tag
        serializer = serializers.IngredientSerializer(data={
            'name': "Test Ingredient 6",
            'slug': "test-ingredient-6",
            'description': "This is an ingredient description 6",
            'usda_number': 1234,
            'tags': [
                {'id': tag3.id, 'name': "Tag3 Updated", 'slug': "tag3"}
            ]
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        ingredient = serializer.save()
        self.assertEqual(ingredient.name, "Test Ingredient 6")
        self.assertEqual(ingredient.slug, "test-ingredient-6")
        self.assertEqual(ingredient.description, "This is an ingredient description 6")
        self.assertEqual(ingredient.usda_number, 1234)
        self.assertEqual(len(ingredient.tags.all()), 1)
        tag3.refresh_from_db()
        self.assertEqual(tag3.name, "Tag3 Updated")
        self.assertEqual(tag3.slug, "tag3")

        # Create an ingredient without any tags, and not specifying the tags element
        # and no slug
        serializer = serializers.IngredientSerializer(data={
            'name': "Test Ingredient 6",
            'description': "This is an ingredient description",
            'usda_number': 1234,
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        ingredient = serializer.save()
        self.assertEqual(ingredient.name, "Test Ingredient 6")
        self.assertEqual(ingredient.slug, "test-ingredient-6-1")
        self.assertEqual(ingredient.description, "This is an ingredient description")
        self.assertEqual(ingredient.usda_number, 1234)
        self.assertEqual(len(ingredient.tags.all()), 0)

    def test_ingredient_update(self):
        """Test that we can update ingredient models"""
        # Full model update
        serializer = serializers.IngredientSerializer(
            self.ingredient1,
            data={
                'name': "Test Ingredient1 Updated",
                'slug': "test-ingredient1-updated",
                'description': "Ingredient Description 1 Updated",
                'usda_number': 5678,
                'tags': [
                    {'id': self.tag1.id}
                ]})
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer.save()
        self.assertEqual(self.ingredient1.name, "Test Ingredient1 Updated")
        self.assertEqual(self.ingredient1.slug, "test-ingredient1-updated")
        self.assertEqual(self.ingredient1.description, "Ingredient Description 1 Updated")
        self.assertEqual(self.ingredient1.usda_number, 5678)
        self.assertEqual(list(self.ingredient1.tags.all()), [self.tag1])

        # Full model update, updating and creating a tag
        serializer = serializers.IngredientSerializer(
            self.ingredient2,
            data={
                'name': "Test Ingredient2 Updated",
                'slug': "test-ingredient2-updated",
                'description': "Ingredient Description 2 Updated",
                'usda_number': 1234,
                'tags': [
                    {'id': self.tag1.id, 'name': 'Tag1 Updated'},
                    {'name': "Tag4", 'slug': "tag4"}
                ]})
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer.save()
        self.assertEqual(self.ingredient2.name, "Test Ingredient2 Updated")
        self.assertEqual(self.ingredient2.slug, "test-ingredient2-updated")
        self.assertEqual(self.ingredient2.description, "Ingredient Description 2 Updated")
        self.assertEqual(self.ingredient2.usda_number, 1234)
        self.assertEqual(len(self.ingredient2.tags.all()), 2)
        self.tag1.refresh_from_db()
        tag4 = list(self.ingredient2.tags.all())[1]
        self.assertEqual(self.tag1.name, "Tag1 Updated")
        self.assertEqual(self.tag1.slug, "tag1")
        self.assertEqual(tag4.name, "Tag4")
        self.assertEqual(tag4.slug, "tag4")

        # Partial model update, clearing tags
        serializer = serializers.IngredientSerializer(
            self.ingredient1,
            partial=True,
            data={
                'name': "Test Ingredient1",
                'tags': []})
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer.save()
        self.assertEqual(self.ingredient1.name, "Test Ingredient1")
        self.assertEqual(self.ingredient1.slug, "test-ingredient1-updated")
        self.assertEqual(self.ingredient1.description, "Ingredient Description 1 Updated")
        self.assertEqual(self.ingredient1.usda_number, 5678)
        self.assertEqual(len(self.ingredient1.tags.all()), 0)

        # Partial model update, not including the tags list at all
        serializer = serializers.IngredientSerializer(
            self.ingredient2,
            partial=True,
            data={
                'name': "Test Ingredient2",
                'usda_number': 5678})
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer.save()
        self.assertEqual(self.ingredient2.name, "Test Ingredient2")
        self.assertEqual(self.ingredient2.slug, "test-ingredient2-updated")
        self.assertEqual(self.ingredient2.description, "Ingredient Description 2 Updated")
        self.assertEqual(self.ingredient2.usda_number, 5678)
        self.assertEqual(len(self.ingredient2.tags.all()), 2)
        self.tag1 = list(self.ingredient2.tags.all())[0]
        tag4 = list(self.ingredient2.tags.all())[1]
        self.assertEqual(self.tag1.name, "Tag1 Updated")
        self.assertEqual(self.tag1.slug, "tag1")
        self.assertEqual(tag4.name, "Tag4")
        self.assertEqual(tag4.slug, "tag4")
