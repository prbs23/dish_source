"""
Unit Tests for SourceSerializer
"""
from django.test import TestCase
from dish_source import models
from dish_source import serializers


class SourceSerializerTests(TestCase):
    """
    Source Serializer class unit tests
    """

    def setUp(self):
        """
        Creates a minimal source_author/recipe/source model for testing Source serializer
        """
        self.author1 = models.SourceAuthor.objects.create(name="Test Author1", slug="test-author1")
        self.source1 = models.Source.objects.create(name="Test Source 1")
        self.source1.authors.add(self.author1)
        self.source2 = models.Source.objects.create(url="http://www.test_source_2.com")
        self.source2.authors.add(self.author1)
        self.category = models.Category.objects.create(name="Test Category", slug="test-category")
        self.recipe1 = models.Recipe.objects.create(
            name="Recipe 1",
            slug="recipe-1",
            category=self.category,
            source=self.source1
        )
        self.recipe2 = models.Recipe.objects.create(
            name="Recipe 2",
            slug="recipe-2",
            category=self.category,
            source=self.source2
        )
        self.recipe3 = models.Recipe.objects.create(
            name="Recipe 3",
            slug="recipe-3",
            category=self.category,
            source=self.source1
        )

    def test_source_serialize(self):
        """Test Source serialization"""

        # serialize author without expanding recipes or source list
        self.assertEqual(serializers.SourceSerializer(self.source1).data, {
            'id': 1,
            'name': "Test Source 1",
            'url': '',
            'isbn': '',
            'notes': '',
            'authors': [
                {
                    'id': self.author1.id,
                    'name': 'Test Author1',
                    'slug': 'test-author1'
                },
            ],
            'recipes': [
                {
                    'id': self.recipe1.id,
                    'name': 'Recipe 1',
                    'slug': 'recipe-1',
                    'category': {
                        'id': self.category.id,
                        'name': "Test Category",
                        'slug': "test-category"
                    },
                    'subcategory': None,
                    'tags': []
                },
                {
                    'id': self.recipe3.id,
                    'name': 'Recipe 3',
                    'slug': 'recipe-3',
                    'category': {
                        'id': self.category.id,
                        'name': "Test Category",
                        'slug': "test-category"
                    },
                    'subcategory': None,
                    'tags': []
                },
            ]
        })

        # Serialize source with the expanded recipe list
        self.assertEqual(
            serializers.SourceSerializer(self.source2, context={'expand': ['recipes']}).data,
            {
                'id': 2,
                'name': "",
                'url': 'http://www.test_source_2.com',
                'isbn': '',
                'notes': '',
                'authors': [
                    {
                        'id': self.author1.id,
                        'name': 'Test Author1',
                        'slug': 'test-author1'
                    },
                ],
                'recipes': [
                    serializers.RecipeSerializer(self.recipe2).data,
                ]
            }
        )

        # serialize source with the expanded author list
        self.assertEqual(serializers.SourceSerializer(
            self.source1,
            context={'expand': ['authors']}).data, {
                'id': 1,
                'name': "Test Source 1",
                'url': '',
                'isbn': '',
                'notes': '',
                'authors': [
                    serializers.SourceAuthorSerializer(self.author1).data
                ],
                'recipes': [
                    {
                        'id': self.recipe1.id,
                        'name': 'Recipe 1',
                        'slug': 'recipe-1',
                        'category': {
                            'id': self.category.id,
                            'name': "Test Category",
                            'slug': "test-category"
                        },
                        'subcategory': None,
                        'tags': []
                    },
                    {
                        'id': self.recipe3.id,
                        'name': 'Recipe 3',
                        'slug': 'recipe-3',
                        'category': {
                            'id': self.category.id,
                            'name': "Test Category",
                            'slug': "test-category"
                        },
                        'subcategory': None,
                        'tags': []
                    },
                ]
            })

        # serialize list of sources without expanding the base object
        self.assertEqual(
            serializers.SourceSerializer([self.source1, self.source2], many=True).data,
            [
                {
                    'id': 1,
                    'name': "Test Source 1",
                    'url': ""
                },
                {
                    'id': 2,
                    'name': "",
                    'url': "http://www.test_source_2.com"
                }
            ])

        # serialize list of expanded sources
        self.assertEqual(
            serializers.SourceSerializer([self.source1], many=True, context={'expand': ['']}).data,
            [{
                'id': 1,
                'name': "Test Source 1",
                'url': '',
                'isbn': '',
                'notes': '',
                'authors': [
                    {
                        'id': self.author1.id,
                        'name': 'Test Author1',
                        'slug': 'test-author1'
                    },
                ],
                'recipes': [
                    {
                        'id': self.recipe1.id,
                        'name': 'Recipe 1',
                        'slug': 'recipe-1',
                        'category': {
                            'id': self.category.id,
                            'name': "Test Category",
                            'slug': "test-category"
                        },
                        'subcategory': None,
                        'tags': []
                    },
                    {
                        'id': self.recipe3.id,
                        'name': 'Recipe 3',
                        'slug': 'recipe-3',
                        'category': {
                            'id': self.category.id,
                            'name': "Test Category",
                            'slug': "test-category"
                        },
                        'subcategory': None,
                        'tags': []
                    },
                ]
            }])

    def test_source_validation(self):
        """Test Source validation"""
        # Valid source
        serializer = serializers.SourceSerializer(data={
            'name': "Test Source",
            'url': "http://www.test-source.com",
            'isbn': "12345",
            'notes': "Source notes...",
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # Only the name or url is required
        serializer = serializers.SourceSerializer(data={
            'name': "Test Source",
            'isbn': "12345",
            'notes': "Source notes...",
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer = serializers.SourceSerializer(data={
            'url': "http://www.test-source.com",
            'isbn': "12345",
            'notes': "Source notes...",
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # However, both name and url cannot both be missing
        serializer = serializers.SourceSerializer(data={
            'isbn': "12345",
            'notes': "Source notes...",
        })
        self.assertEqual(serializer.is_valid(), False, serializer.errors)

        # For partial updates it's not required that name or url is provided
        serializer = serializers.SourceSerializer(
            self.source1,
            partial=True,
            data={
                'isbn': "12222"
            })
        self.assertEqual(serializer.is_valid(), True)

        # As long as applying the update wouldn't cause both name and url to be empty
        serializer = serializers.SourceSerializer(
            self.source1,
            partial=True,
            data={
                'name': ""
            })
        self.assertEqual(serializer.is_valid(), False)

    def test_source_create(self):
        """Test creating a new source item"""
        serializer = serializers.SourceSerializer(data={
            'name': "Test Source",
            'authors': [
                {
                    'name': "Test Author",
                    'slug': "test-author",
                }
            ]
        })
        self.assertEqual(serializer.is_valid(), True)
        source = serializer.save()
        self.assertTrue(source.id is not None)
        self.assertEqual(source.name, "Test Source")
        self.assertEqual(source.url, "")
        self.assertEqual(source.isbn, "")
        self.assertEqual(source.notes, "")
        self.assertEqual(len(source.authors.all()), 1)
        self.assertEqual(source.authors.all()[0].name, "Test Author")
        self.assertEqual(source.authors.all()[0].slug, "test-author")

        # Specifying a negative ID should create a new ID
        serializer = serializers.SourceSerializer(data={
            'id': -1,
            'name': "Test Source 2",
            'authors': [
            ]
        })
        self.assertEqual(serializer.is_valid(), True)
        source = serializer.save()
        self.assertTrue(source.id is not None)
        self.assertTrue(source.id >= 0)
        self.assertEqual(source.name, "Test Source 2")
        self.assertEqual(source.url, "")

    def test_source_update(self):
        """Test updating source models"""
        # Full model update
        serializer = serializers.SourceSerializer(
            self.source1,
            data={
                'name': "",
                'url': "https://www.test-source-5.com",
                'isbn': "9870"
            })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        serializer.save()
        self.assertEqual(self.source1.name, "")
        self.assertEqual(self.source1.url, "https://www.test-source-5.com")
        self.assertEqual(self.source1.isbn, "9870")

        # Partial model update to change the author
        serializer = serializers.SourceSerializer(
            self.source2,
            partial=True,
            data={
                "authors": [
                    {
                        'id': self.author1.id,
                    },
                    {
                        "name": "Completely new author",
                        "slug": "new-author"
                    }
                ]
            })
        self.assertEqual(serializer.is_valid(), True)
        serializer.save()
        self.assertEqual(self.source2.name, "")
        self.assertEqual(self.source2.url, "http://www.test_source_2.com")
        self.assertEqual(self.source2.isbn, "")
        self.assertEqual(self.source2.notes, "")
        self.assertEqual(len(self.source2.authors.all()), 2)
        self.assertNotEqual(self.source2.authors.all()[0], self.author1)
        self.assertEqual(self.source2.authors.all()[0].name, "Completely new author")
        self.assertEqual(self.source2.authors.all()[0].slug, "new-author")
        self.assertEqual(self.source2.authors.all()[1], self.author1)
