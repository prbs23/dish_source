"""
Unit Tests for category and subcategory serializer
"""

from django.test import TestCase
from dish_source import models
from dish_source import serializers


class CategorySubcategorySerializerTestBase(TestCase):
    """
    Base class for Category and Subcategory serializer unit tests
    """

    def setUp(self):
        """
        Set up some categories, subcategories, and empty recipes to put in the categories. Note that
        the recipes aren't fully fleshed out, but that isn't required for testing the
        category/subcategory serializers.
        """
        self.category1 = models.Category.objects.create(
            name="Test Category 1",
            slug="test-category-1",
            description="Description of category 1"
        )
        self.category2 = models.Category.objects.create(
            name="Test Category 2",
            slug="test-category-2",
            description="Description of second category"
        )
        self.subcategory1 = models.Subcategory.objects.create(
            name="Test Subcategory 1",
            slug="test-subcategory-1",
            description="Subcategory description",
            category=self.category1
        )
        self.subcategory2 = models.Subcategory.objects.create(
            name="Test Subcategory 2",
            slug="test-subcategory-2",
            category=self.category2
        )
        self.recipe1 = models.Recipe.objects.create(
            name="Recipe 1",
            slug="recipe-1",
            category=self.category1,
            subcategory=self.subcategory1,
            source=models.Source.objects.create(
                name="Test Source",
                isbn="112657",
                url="http://www.test.com",
                notes="source notes"),
        )
        self.recipe2 = models.Recipe.objects.create(
            name="Recipe 2",
            slug="recipe-2",
            category=self.category1,
            subcategory=None,
            source=models.Source.objects.create(
                name="Test Source 2",
                isbn="112658"),
        )
        self.recipe3 = models.Recipe.objects.create(
            name="Recipe 3",
            slug="recipe-3",
            category=self.category2,
            subcategory=None,
            source=models.Source.objects.create(
                name="Test Source 3",
                isbn="112659"),
        )
        self.recipe4 = models.Recipe.objects.create(
            name="Recipe 4",
            slug="recipe-4",
            category=self.category2,
            subcategory=self.subcategory2,
            source=models.Source.objects.create(url="http://www.test.com"),
        )


class CategorySerializerTests(CategorySubcategorySerializerTestBase):
    """
    Category Serializer class unit tests
    """

    def test_category_serialize(self):
        """Test Category serialization"""

        # Test serializing a collapsed recipe
        self.assertEqual(serializers.CategorySerializer([self.category1], many=True).data[0], {
            'id': self.category1.id,
            'name': "Test Category 1",
            'slug': "test-category-1",
        })

        # Test serializing a full category
        self.assertEqual(serializers.CategorySerializer(self.category2).data, {
            'id': self.category2.id,
            'name': "Test Category 2",
            'slug': "test-category-2",
            'description': "Description of second category",
            'subcategories': serializers.SubcategorySerializer([self.subcategory2], many=True).data,
            'unsubcategorized_recipes': serializers.RecipeSerializer([self.recipe3],
                                                                     many=True).data
        })

    def test_category_validation(self):
        """Test Category validation"""

        # Specify all writable fields
        serializer = serializers.CategorySerializer(data={
            'name': "Test Category 3",
            'slug': "test-category-3",
            'description': "Some other description"
        })
        self.assertTrue(serializer.is_valid())

        # The description field should not be required
        serializer = serializers.CategorySerializer(data={
            'name': "Test Category 3",
            'slug': "test-category-3",
        })
        self.assertTrue(serializer.is_valid())

        # You can pass a subcategories field, but it should be stripped in the validated data
        serializer = serializers.CategorySerializer(data={
            'name': "Test Category 3",
            'slug': "test-category-3",
            'subcategories': [
                {
                    'name': "Test Subcategory 3",
                    'slug': "test-subcategory-3"
                }
            ]
        })
        self.assertTrue(serializer.is_valid())
        self.assertTrue('subcategories' not in serializer.validated_data)

    def test_category_create(self):
        """Test creating a new category item"""

        serializer = serializers.CategorySerializer(data={
            'name': "Test Category 3",
            'slug': "test-category-3",
            'description': "Some other description"
        })
        self.assertTrue(serializer.is_valid())
        new_category = serializer.save()

        self.assertEqual(new_category.name, "Test Category 3")
        self.assertEqual(new_category.slug, "test-category-3")
        self.assertEqual(new_category.description, "Some other description")

        # Test that if no slug field is populated one will be created
        serializer = serializers.CategorySerializer(data={
            'name': "Test Category 4",
            'description': "Yet another description"
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        new_category = serializer.save()

        self.assertEqual(new_category.name, "Test Category 4")
        self.assertEqual(new_category.slug, "test-category-4")
        self.assertEqual(new_category.description, "Yet another description")

    def test_category_update(self):
        """Test updating a category."""

        serializer = serializers.CategorySerializer(self.category2, data={
            'name': "Updated category name",
        }, partial=True)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        self.assertEqual(self.category2.name, "Updated category name")
        self.assertEqual(self.category2.slug, "test-category-2")
        self.assertEqual(self.category2.description, "Description of second category")


class SubcategorySerializerTests(CategorySubcategorySerializerTestBase):
    """
    Subcategory Serializer class unit tests
    """

    def test_subcategory_serialize(self):
        """Test Subcategory serialization"""

        # Test serializing a collapsed recipe
        self.assertEqual(
            serializers.SubcategorySerializer([self.subcategory1], many=True).data[0],
            {
                'id': self.subcategory1.id,
                'name': "Test Subcategory 1",
                'slug': "test-subcategory-1",
            })

        # Test serializing a full category
        self.assertEqual(serializers.SubcategorySerializer(self.subcategory2).data, {
            'id': self.subcategory2.id,
            'name': "Test Subcategory 2",
            'slug': "test-subcategory-2",
            'description': "",
            'category': serializers.CategorySerializer([self.category2], many=True).data[0],
            'recipes': serializers.RecipeSerializer(self.subcategory2.recipes, many=True).data
        })

    def test_subcategory_validation(self):
        """Test Subcategory validation"""

        # Specify all writable fields
        serializer = serializers.SubcategorySerializer(data={
            'name': "Test Subcategory 3",
            'slug': "test-subcategory-3",
            'description': "Some other description",
            'category': {'id': self.category1.id}
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)

        # The description field should not be required
        serializer = serializers.SubcategorySerializer(data={
            'name': "Test Subcategory 3",
            'slug': "test-subcategory-3",
            'category': {'id': self.category1.id}
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)

    def test_subcategory_create(self):
        """Test creating a new subcategory item"""

        serializer = serializers.SubcategorySerializer(data={
            'name': "Test Subcategory 3",
            'slug': "test-subcategory-3",
            'category': {'id': self.category1.id}
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        new_subcategory = serializer.save()

        self.assertEqual(new_subcategory.name, "Test Subcategory 3")
        self.assertEqual(new_subcategory.slug, "test-subcategory-3")
        self.assertEqual(new_subcategory.description, '')
        self.assertEqual(new_subcategory.category, self.category1)

        # If no slug is provided one should be created
        serializer = serializers.SubcategorySerializer(data={
            'name': "Test Subcategory 4",
            'category': {'id': self.category1.id}
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        new_subcategory = serializer.save()

        self.assertEqual(new_subcategory.name, "Test Subcategory 4")
        self.assertEqual(new_subcategory.slug, "test-subcategory-4")
        self.assertEqual(new_subcategory.description, '')
        self.assertEqual(new_subcategory.category, self.category1)

    def test_subcategory_update(self):
        """Test updating a subcategory."""

        # Just update a subcateogry
        serializer = serializers.SubcategorySerializer(self.subcategory2, data={
            'name': "Updated subcategory name",
            'category': {'id': self.category2.id}
        }, partial=True)
        self.assertTrue(serializer.is_valid())
        serializer.save()
        self.assertEqual(self.subcategory2.name, "Updated subcategory name")
        self.assertEqual(self.subcategory2.slug, "test-subcategory-2")
        self.assertEqual(self.subcategory2.description, "")

        # It shouldn't be allowed to change the category
        serializer = serializers.SubcategorySerializer(self.subcategory2, data={
            'category': {'id': self.category1.id}
        }, partial=True)
        self.assertFalse(serializer.is_valid())
