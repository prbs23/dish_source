"""
Unit Tests for IngredientTagSerializer
"""

from django.test import TestCase
from dish_source import models
from dish_source import serializers


class IngredientTagSerializerTests(TestCase):
    """
    Ingredient Tag serializer class unit tests
    """

    def setUp(self):
        """Create a Ingredient tag entry for updating"""
        models.IngredientTag.objects.create(name="Vegetarian", slug="vegetarian")

    def test_ingredient_tag_serialize(self):
        """Test ingredient tag serialization"""
        tag = models.IngredientTag(id=5, name="Gluten Free", slug="gluten-free")
        self.assertEqual(serializers.IngredientTagSerializer(tag).data, {
            'id': 5,
            'name': "Gluten Free",
            'slug': "gluten-free"
        })

        # Serialize a list of tags without expanding
        self.assertEqual(serializers.IngredientTagSerializer([tag], many=True).data, [{
            'id': 5,
            'name': "Gluten Free",
            'slug': "gluten-free"
        }])

    def test_ingredient_tag_validation(self):
        """Test ingredient tag validation"""
        # Valid ingredient tag
        serializer = serializers.IngredientTagSerializer(data={
            'name': "Vegan",
            'slug': "vegan"
        })
        self.assertEqual(serializer.is_valid(), True)

        # A space in slug field should cause a failure
        serializer = serializers.IngredientTagSerializer(data={
            'name': "Vegan",
            'slug': "veg an"
        })
        self.assertEqual(serializer.is_valid(), False)

        # If a slug field is not provided then it should be automatically created
        serializer = serializers.IngredientTagSerializer(data={
            'name': "Vegetarian Ingredient"
        })
        self.assertEqual(serializer.is_valid(), True)

        # Unless partial is set to true and an existing model is provided
        serializer = serializers.IngredientTagSerializer(
            models.IngredientTag.objects.get(id=1),
            partial=True,
            data={
                'name': "Vegetarian Ingredient"
            })
        self.assertEqual(serializer.is_valid(), True)

    def test_ingredient_tag_create(self):
        """Test creating a new ingredient tag model"""
        serializer = serializers.IngredientTagSerializer(data={
            'name': "Gluten Free",
            'slug': "gluten-free"
        })
        self.assertEqual(serializer.is_valid(), True)
        tag = serializer.save()
        self.assertTrue(tag.id is not None)
        self.assertEqual(tag.name, "Gluten Free")
        self.assertEqual(tag.slug, "gluten-free")

        serializer = serializers.IngredientTagSerializer(data={
            'name': "Test tag 1",
        })
        self.assertEqual(serializer.is_valid(), True)
        tag = serializer.save()
        self.assertTrue(tag.id is not None)
        self.assertEqual(tag.name, "Test tag 1")
        self.assertEqual(tag.slug, "test-tag-1")

    def test_ingredient_tag_update(self):
        """Test updating ingredient tags models"""
        # Full model update
        serializer = serializers.IngredientTagSerializer(
            models.IngredientTag.objects.get(id=1),
            data={
                'name': "Vegetarian Ingredient",
                'slug': "vegetarian-ingredient"
            })
        self.assertEqual(serializer.is_valid(), True)
        serializer.save()
        self.assertEqual(models.IngredientTag.objects.get(id=1).name, "Vegetarian Ingredient")
        self.assertEqual(models.IngredientTag.objects.get(id=1).slug, "vegetarian-ingredient")

        # Partial model update
        serializer = serializers.IngredientTagSerializer(
            models.IngredientTag.objects.get(id=1),
            partial=True,
            data={
                'slug': "vegetarian"
            })
        self.assertEqual(serializer.is_valid(), True)
        serializer.save()
        self.assertEqual(models.IngredientTag.objects.get(id=1).name, "Vegetarian Ingredient")
        self.assertEqual(models.IngredientTag.objects.get(id=1).slug, "vegetarian")
