"""
Unit Tests for RecipeTagSerializezr
"""

from django.test import TestCase
from dish_source import models
from dish_source import serializers


class RecipeTagSerializerTests(TestCase):
    """
    Recipe Tag serializer class unit tests
    """

    def setUp(self):
        """Create a Ingredient tag entry for updating"""
        models.RecipeTag.objects.create(name="Indian Cuisine", slug="indian-cuisine")

    def test_recipe_tag_serialize(self):
        """Test recipe tag serialization"""
        tag = models.RecipeTag(id=2, name="Korean Cuisine", slug="korean-cuisine")
        self.assertEqual(serializers.RecipeTagSerializer(tag).data, {
            'id': 2,
            'name': "Korean Cuisine",
            'slug': "korean-cuisine"
        })

        # Serialize a list of tags without expanding
        self.assertEqual(serializers.RecipeTagSerializer([tag], many=True).data, [{
            'id': 2,
            'name': "Korean Cuisine",
            'slug': "korean-cuisine"
        }])

    def test_recipe_tag_validation(self):
        """Test recipe tag validation"""
        # Valid recipe tag
        serializer = serializers.RecipeTagSerializer(data={
            'name': "Make Ahead",
            'slug': "make-ahead"
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # A space in slug field should cause a failure
        serializer = serializers.RecipeTagSerializer(data={
            'name': "Make Ahead",
            'slug': "make ahead"
        })
        self.assertEqual(serializer.is_valid(), False, serializer.errors)

        # If the name isn't given there should be a validation error
        serializer = serializers.RecipeTagSerializer(data={
            'slug': "make-ahead"
        })
        self.assertEqual(serializer.is_valid(), False, serializer.errors)

        # However, the slug field isn't required.
        serializer = serializers.RecipeTagSerializer(data={
            'name': "Make Ahead"
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # Unless partial is set to true and an existing model is provided
        serializer = serializers.RecipeTagSerializer(
            models.RecipeTag.objects.get(id=1),
            partial=True,
            data={
                'slug': "indian-cuisine"
            })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

    def test_recipe_tag_create(self):
        """Test creating a new recipe tag model"""
        serializer = serializers.RecipeTagSerializer(data={
            'name': "Make Ahead",
            'slug': "make-ahead"
        })
        self.assertEqual(serializer.is_valid(), True)
        tag = serializer.save()
        self.assertTrue(tag.id is not None)
        self.assertEqual(tag.name, "Make Ahead")
        self.assertEqual(tag.slug, "make-ahead")

        serializer = serializers.RecipeTagSerializer(data={
            'name': "Test tag",
            'slug': "test-tag"
        })
        self.assertEqual(serializer.is_valid(), True)
        tag = serializer.save()
        self.assertTrue(tag.id is not None)
        self.assertEqual(tag.name, "Test tag")
        self.assertEqual(tag.slug, "test-tag")

    def test_recipe_tag_update(self):
        """Test updating recipe tags models"""
        # Full model update
        serializer = serializers.RecipeTagSerializer(
            models.RecipeTag.objects.get(id=1),
            data={
                'name': "Korean Cuisine",
                'slug': "korean-cuisine"
            })
        self.assertEqual(serializer.is_valid(), True)
        serializer.save()
        self.assertEqual(models.RecipeTag.objects.get(id=1).name, "Korean Cuisine")
        self.assertEqual(models.RecipeTag.objects.get(id=1).slug, "korean-cuisine")

        # Partial model update
        serializer = serializers.RecipeTagSerializer(
            models.RecipeTag.objects.get(id=1),
            partial=True,
            data={
                'slug': "korean"
            })
        self.assertEqual(serializer.is_valid(), True)
        serializer.save()
        self.assertEqual(models.RecipeTag.objects.get(id=1).name, "Korean Cuisine")
        self.assertEqual(models.RecipeTag.objects.get(id=1).slug, "korean")
