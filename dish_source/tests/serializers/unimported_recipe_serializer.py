"""
Unit Tests for UnimportedRecipeSerializer
"""

from django.test import TestCase
from dish_source import models
from dish_source import serializers


class UnimportedRecipeSerializerTests(TestCase):
    """
    UnimportedRecipe Serializer class unit tests
    """

    def setUp(self):
        """
        Declare an unimported recipe for serialization tests.
        """
        self.author1 = models.SourceAuthor.objects.create(name="Test Author1", slug="test-author1")
        self.unimported_recipe1 = models.UnimportedRecipe.objects.create(
            text="Text of unimported recipe",
            source=models.Source.objects.create(
                url="https://www.testsource.com",
            )
        )
        self.unimported_recipe1.source.authors.add(self.author1)

    def test_unimported_recipe_serialize(self):
        """Test UnimportedRecipe serialization"""

        # Serialize a collapsed unimported recipe
        self.assertEqual(
            serializers.UnimportedRecipeSerializer([self.unimported_recipe1], many=True).data[0],
            {
                'id': self.unimported_recipe1.id,
                'text': "Text of unimported recipe"
            }
        )

        # Serialize a whole unimported recipe
        self.assertEqual(
            serializers.UnimportedRecipeSerializer(self.unimported_recipe1).data,
            {
                'id': self.unimported_recipe1.id,
                'text': "Text of unimported recipe",
                'source': {
                    'id': self.unimported_recipe1.source.id,
                    'name': "",
                    'url': "https://www.testsource.com",
                }
            }
        )

    def test_unimported_recipe_validation(self):
        """Test UnimportedRecipe validation"""

        serializer = serializers.UnimportedRecipeSerializer(data={
            'text': "New recipe",
            'source': {
                'name': "Some cookbook",
                'authors': [{'id': self.author1.id}]
            }
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)

        # Some recipe text must be given
        serializer = serializers.UnimportedRecipeSerializer(data={
            'source': {
                'name': "Some cookbook",
                'authors': [{'id': self.author1.id}]
            }
        })
        self.assertFalse(serializer.is_valid(), serializer.errors)

        # The source can be excluded
        serializer = serializers.UnimportedRecipeSerializer(data={
            'text': "Some text"
        })
        self.assertFalse(serializer.is_valid(), serializer.errors)

    def test_unimported_recipe_create(self):
        """Test creating new unimported recipe entry"""

        serializer = serializers.UnimportedRecipeSerializer(data={
            'text': "New recipe",
            'source': {
                'name': "Source Name",
                'notes': "Source notes",
                'authors': [{'id': self.author1.id}]
            }
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        new_recipe = serializer.save()
        self.assertEqual(new_recipe.text, "New recipe")
        self.assertEqual(new_recipe.source.name, "Source Name")
        self.assertEqual(new_recipe.source.isbn, "")
        self.assertEqual(new_recipe.source.url, "")
        self.assertEqual(new_recipe.source.notes, "Source notes")
        self.assertEqual(list(new_recipe.source.authors.all()), [self.author1])



    def test_unimported_recipe_update(self):
        """Test that we can update unimported recipe models"""\

        # Test that we can update the text
        serializer = serializers.UnimportedRecipeSerializer(
            self.unimported_recipe1,
            partial=True,
            data={
                'text': "Updated recipe text",
            },
        )
        self.assertTrue(serializer.is_valid(), serializer.errors)
        serializer.save()
        self.assertEqual(self.unimported_recipe1.text, "Updated recipe text")
        self.assertEqual(self.unimported_recipe1.source.name, "")
        self.assertEqual(self.unimported_recipe1.source.isbn, "")
        self.assertEqual(self.unimported_recipe1.source.url, "https://www.testsource.com")
        self.assertEqual(self.unimported_recipe1.source.notes, "")
        self.assertEqual(list(self.unimported_recipe1.source.authors.all()), [self.author1])

        # Test that we can update just a single field of source and everything else stays the same
        serializer = serializers.UnimportedRecipeSerializer(
            self.unimported_recipe1,
            partial=True,
            data={
                'source': {
                    'id': self.author1.id,
                    'name': "New source name"
                }
            },
        )
        self.assertTrue(serializer.is_valid(), serializer.errors)
        serializer.save()
        self.assertEqual(self.unimported_recipe1.text, "Updated recipe text")
        self.assertEqual(self.unimported_recipe1.source.name, "New source name")
        self.assertEqual(self.unimported_recipe1.source.isbn, "")
        self.assertEqual(self.unimported_recipe1.source.url, "https://www.testsource.com")
        self.assertEqual(self.unimported_recipe1.source.notes, "")
        self.assertEqual(list(self.unimported_recipe1.source.authors.all()), [self.author1])
