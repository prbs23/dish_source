"""
Unit Tests for SourceAuthorSerializer
"""
from django.test import TestCase
from dish_source import models
from dish_source import serializers


class SourceAuthorSerializerTests(TestCase):
    """
    SourceAuthor Serializer class unit tests
    """

    def setUp(self):
        """
        Creates a minimal source_author/recipe/source model for testing SourceAuthor serializer
        """
        self.author1 = models.SourceAuthor.objects.create(name="Test Author1", slug="test-author1")
        self.source1 = models.Source.objects.create(name="Test Source 1")
        self.source1.authors.add(self.author1)
        self.source2 = models.Source.objects.create(name="Test Source 2")
        self.source2.authors.add(self.author1)
        self.category = models.Category.objects.create(name="Test Category", slug="test-category")
        self.recipe1 = models.Recipe.objects.create(
            name="Recipe 1",
            slug="recipe-1",
            category=self.category,
            source=self.source1
        )
        self.recipe2 = models.Recipe.objects.create(
            name="Recipe 2",
            slug="recipe-2",
            category=self.category,
            source=self.source2
        )

    def test_source_author_serialize(self):
        """Test SourceAuthor serialization"""

        # serialize author without expanding recipes or source list
        self.assertEqual(serializers.SourceAuthorSerializer(self.author1).data, {
            'id': 1,
            'name': "Test Author1",
            'slug': "test-author1",
            'sources': [
                {
                    'id': self.source1.id,
                    'name': 'Test Source 1',
                    'url': '',
                },
                {
                    'id': self.source2.id,
                    'name': 'Test Source 2',
                    'url': '',
                }
            ],
            'recipes': [
                {
                    'id': self.recipe1.id,
                    'name': 'Recipe 1',
                    'slug': 'recipe-1',
                    'category': {
                        'id': self.category.id,
                        'name': "Test Category",
                        'slug': "test-category"
                    },
                    'subcategory': None,
                    'tags': []
                },
                {
                    'id': self.recipe2.id,
                    'name': 'Recipe 2',
                    'slug': 'recipe-2',
                    'category': {
                        'id': self.category.id,
                        'name': "Test Category",
                        'slug': "test-category"
                    },
                    'subcategory': None,
                    'tags': []
                },
            ]
        })

        # Serialize author with the expanded recipe list
        self.assertEqual(
            serializers.SourceAuthorSerializer(self.author1, context={'expand': ['recipes']}).data,
            {
                'id': 1,
                'name': "Test Author1",
                'slug': "test-author1",
                'sources': [
                    {
                        'id': self.source1.id,
                        'name': 'Test Source 1',
                        'url': '',
                    },
                    {
                        'id': self.source2.id,
                        'name': 'Test Source 2',
                        'url': '',
                    }
                ],
                'recipes': [
                    serializers.RecipeSerializer(self.recipe1).data,
                    serializers.RecipeSerializer(self.recipe2).data
                ]
            }
        )
        # serialize author with the expanded source list
        self.assertEqual(serializers.SourceAuthorSerializer(
            self.author1,
            context={'expand': ['sources']}).data, {
                'id': 1,
                'name': "Test Author1",
                'slug': "test-author1",
                'sources': [
                    serializers.SourceSerializer(self.source1).data,
                    serializers.SourceSerializer(self.source2).data,
                ],
                'recipes': [
                    {
                        'id': self.recipe1.id,
                        'name': 'Recipe 1',
                        'slug': 'recipe-1',
                        'category': {
                            'id': self.category.id,
                            'name': "Test Category",
                            'slug': "test-category"
                        },
                        'subcategory': None,
                        'tags': []
                    },
                    {
                        'id': self.recipe2.id,
                        'name': 'Recipe 2',
                        'slug': 'recipe-2',
                        'category': {
                            'id': self.category.id,
                            'name': "Test Category",
                            'slug': "test-category"
                        },
                        'subcategory': None,
                        'tags': []
                    },
                ]
            })

        # serialize list of authors without expanding the base object
        self.assertEqual(serializers.SourceAuthorSerializer([self.author1], many=True).data, [{
            'id': 1,
            'name': "Test Author1",
            'slug': "test-author1",
        }])

        # serialize list of expanded author
        self.assertEqual(
            serializers.SourceAuthorSerializer([self.author1], many=True,
                                               context={'expand': ['']}).data,
            [{
                'id': 1,
                'name': "Test Author1",
                'slug': "test-author1",
                'sources': [
                    {
                        'id': self.source1.id,
                        'name': 'Test Source 1',
                        'url': '',
                    },
                    {
                        'id': self.source2.id,
                        'name': 'Test Source 2',
                        'url': '',
                    }
                ],
                'recipes': [
                    {
                        'id': self.recipe1.id,
                        'name': 'Recipe 1',
                        'slug': 'recipe-1',
                        'category': {
                            'id': self.category.id,
                            'name': "Test Category",
                            'slug': "test-category"
                        },
                        'subcategory': None,
                        'tags': []
                    },
                    {
                        'id': self.recipe2.id,
                        'name': 'Recipe 2',
                        'slug': 'recipe-2',
                        'category': {
                            'id': self.category.id,
                            'name': "Test Category",
                            'slug': "test-category"
                        },
                        'subcategory': None,
                        'tags': []
                    },
                ]
            }])

    def test_source_author_validation(self):
        """Test SourceAuthor validation"""
        # Valid source author
        serializer = serializers.SourceAuthorSerializer(data={
            'name': "Test Author",
            'slug': "test-author"
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # Name field is required
        serializer = serializers.SourceAuthorSerializer(data={
            'slug-field': "test-author"
        })
        self.assertEqual(serializer.is_valid(), False, serializer.errors)

        # Slug field is not required
        serializer = serializers.SourceAuthorSerializer(data={
            'name': "Test Author"
        })
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # Slug field cannot contain spaces
        serializer = serializers.SourceAuthorSerializer(data={
            'name': "Test Author",
            'slug': "test author"
        })
        self.assertEqual(serializer.is_valid(), False, serializer.errors)

        # Not all fields are required for a partial update
        serializer = serializers.SourceAuthorSerializer(
            models.SourceAuthor.objects.get(id=self.author1.id),
            partial=True,
            data={
                'name': "Author 3"
            })
        self.assertEqual(serializer.is_valid(), True)

    def test_source_author_create(self):
        """Test creating a new source author item"""
        serializer = serializers.SourceAuthorSerializer(data={
            'name': "Test Author4",
            'slug': "test-author4"
        })
        self.assertEqual(serializer.is_valid(), True)
        author = serializer.save()
        self.assertTrue(author.id is not None)
        self.assertEqual(author.name, "Test Author4")
        self.assertEqual(author.slug, "test-author4")

        # Test that a slug will be generated if not provided
        serializer = serializers.SourceAuthorSerializer(data={
            'name': "Test Author5",
        })
        self.assertEqual(serializer.is_valid(), True)
        author = serializer.save()
        self.assertTrue(author.id is not None)
        self.assertEqual(author.name, "Test Author5")
        self.assertEqual(author.slug, "test-author5")

    def test_source_author_update(self):
        """Test updating author models"""
        # Full model update
        serializer = serializers.SourceAuthorSerializer(
            self.author1,
            data={
                'name': "Test Author 5",
                'slug': "test-author-5"
            })
        self.assertEqual(serializer.is_valid(), True)
        serializer.save()
        self.assertEqual(self.author1.name, "Test Author 5")
        self.assertEqual(self.author1.slug, "test-author-5")

        # Partial model update
        serializer = serializers.SourceAuthorSerializer(
            self.author1,
            partial=True,
            data={
                'slug': "test-author5"
            })
        self.assertEqual(serializer.is_valid(), True)
        serializer.save()
        self.assertEqual(self.author1.name, "Test Author 5")
        self.assertEqual(self.author1.slug, "test-author5")
