"""
Unit Tests for RecipeSerializer
"""
from decimal import Decimal

from django.test import TestCase
from dish_source import models
from dish_source import serializers


class RecipeSerializerTests(TestCase):
    """
    Recipe Serializer class unit tests
    """

    def setUp(self):
        """
        Set up some test ingredients and other model items to define a recipe
        """
        self.category = models.Category.objects.create(name="Test Category", slug="test-category")
        self.category2 = models.Category.objects.create(name="Test Category 2",
                                                        slug="test-category-2")
        self.subcategory = models.Subcategory.objects.create(
            name="Test Subcategory",
            slug="test-subcategory",
            category=self.category
        )
        self.subcategory2 = models.Subcategory.objects.create(
            name="Test Subcategory 2",
            slug="test-subcategory2",
            category=self.category2
        )
        self.author = models.SourceAuthor.objects.create(name="Test Author1", slug="test-author")
        self.source = models.Source.objects.create(name="Test Source", isbn="112657",
                                                   url="http://www.test.com", notes="source notes")
        self.source.authors.add(self.author)
        self.ingredient_tag1 = models.IngredientTag.objects.create(
            name="Ing Tag 1", slug="ing-tag-1")
        self.recipe_tag1 = models.RecipeTag.objects.create(name="Rec Tag 1", slug="rec-tag-1")
        self.ingredient1 = models.Ingredient.objects.create(name="Ingredient1", slug="ingredient-1")
        self.ingredient1.tags.add(self.ingredient_tag1)
        self.ingredient2 = models.Ingredient.objects.create(name="Ingredient2", slug="ingredient-2")
        self.ingredient3 = models.Ingredient.objects.create(name="Ingredient3", slug="ingredient-3")
        self.ingredient4 = models.Ingredient.objects.create(name="Ingredient4", slug="ingredient-4")
        self.unit_cup = models.Unit.objects.create(name="cup", abbrev=None)
        self.unit_tbsp = models.Unit.objects.create(name="tablespoon", abbrev="tbsp")
        self.unit_servings = models.Unit.objects.create(name="serving")
        self.recipe1 = models.Recipe.objects.create(
            name="Recipe 1",
            slug="recipe-1",
            category=self.category,
            subcategory=self.subcategory,
            source=self.source,
            description="Recipe description 1",
            notes="Notes 1",
            prep_duration_min=600,
            prep_duration_max=None,
            cook_duration_min=1200,
            cook_duration_max=2000
        )
        models.RecipeYield.objects.create(
            recipe=self.recipe1,
            order=0,
            approx=True,
            min_quantity=5,
            max_quantity=None,
            unit=self.unit_servings
        )
        models.RecipeIngredientSection.objects.create(recipe=self.recipe1, order=0, name=None)
        self.recipe_ingredient1 = models.RecipeIngredient.objects.create(
            section=self.recipe1.ingredient_sections.all()[0],
            order=0,
            qualifier="qual",
            ingredient=self.ingredient1,
            processing="proc",
        )
        models.RecipeIngredientQuantity.objects.create(
            recipe_ingredient=self.recipe_ingredient1,
            order=0,
            approx=False,
            min_quantity=1,
            max_quantity=None,
            unit=self.unit_cup
        )
        self.recipe_ingredient2 = models.RecipeIngredient.objects.create(
            section=self.recipe1.ingredient_sections.all()[0],
            order=1,
            ingredient=self.ingredient2,
        )
        models.RecipeIngredientQuantity.objects.create(
            recipe_ingredient=self.recipe_ingredient2,
            order=0,
            approx=False,
            min_quantity=1,
            max_quantity=1.5,
            unit=self.unit_tbsp
        )
        models.RecipeIngredientQuantity.objects.create(
            recipe_ingredient=self.recipe_ingredient2,
            order=1,
            approx=True,
            min_quantity=10,
            max_quantity=None,
            unit=self.unit_cup
        )
        models.RecipeIngredientSubstitution.objects.create(
            substituted_ingredient=self.recipe_ingredient2,
            order=0,
        )
        models.RecipeIngredient.objects.create(
            substitution=self.recipe_ingredient2.substitutions.all()[0],
            order=0,
            qualifier="qualifier 2",
            ingredient=self.ingredient3,
        )
        models.RecipeIngredientQuantity.objects.create(
            recipe_ingredient=self.recipe_ingredient2.substitutions.all()[0].ingredients.all()[0],
            order=0,
            approx=True,
            min_quantity=4,
            unit=self.unit_tbsp
        )
        models.RecipeIngredientSection.objects.create(recipe=self.recipe1, order=1,
                                                      name="Ingredient Section 2")
        self.recipe_ingredient3 = models.RecipeIngredient.objects.create(
            section=self.recipe1.ingredient_sections.all()[1],
            order=0,
            qualifier="qual 3",
            ingredient=self.ingredient4,
            processing="proc 3",
        )
        models.RecipeIngredientQuantity.objects.create(
            recipe_ingredient=self.recipe_ingredient3,
            order=0,
            approx=False,
            min_quantity=1,
            max_quantity=None,
            unit=self.unit_cup
        )
        models.RecipeInstructionSection.objects.create(recipe=self.recipe1, order=0, name=None)
        models.RecipeInstruction.objects.create(
            section=self.recipe1.instruction_sections.all()[0],
            order=0,
            instruction="Recipe 1 Instruction 1"
        )
        models.RecipeInstruction.objects.create(
            section=self.recipe1.instruction_sections.all()[0],
            order=1,
            instruction="Recipe 1 Instruction 2"
        )
        models.RecipeInstructionSection.objects.create(recipe=self.recipe1, order=1,
                                                       name="Instruction Section 2")
        models.RecipeInstruction.objects.create(
            section=self.recipe1.instruction_sections.all()[1],
            order=1,
            instruction="Recipe 1 Instruction 3"
        )
        models.RecipeVariation.objects.create(
            recipe=self.recipe1,
            order=0,
            name="Var 0",
            variation="Recipe 1 Variation 0"
        )
        self.recipe_ingredient4 = None

    def test_recipe_serialize(self):
        """Test Recipe serialization"""

        # Test serializing a collapsed recipe
        self.assertEqual(serializers.RecipeSerializer([self.recipe1], many=True).data[0], {
            'id': self.recipe1.id,
            'name': "Recipe 1",
            'slug': "recipe-1",
            'category': serializers.CategorySerializer([self.category], many=True).data[0],
            'subcategory': serializers.SubcategorySerializer([self.subcategory], many=True).data[0],
            'tags': [],
        })

        # serialize a simple ingredient without substitutions
        self.assertEqual(serializers.RecipeSerializer(self.recipe1).data, {
            'id': self.recipe1.id,
            'name': "Recipe 1",
            'slug': "recipe-1",
            'category': serializers.CategorySerializer([self.category], many=True).data[0],
            'subcategory': serializers.SubcategorySerializer([self.subcategory], many=True).data[0],
            'source': {
                'id': self.source.id,
                'name': "Test Source",
                'url': "http://www.test.com",
            },
            'yields': [
                {
                    'approx': True,
                    'min_quantity': '5.0000',
                    'max_quantity': None,
                    'unit': {
                        'id': self.unit_servings.id,
                        'name': "serving",
                        'plural': "servings",
                        'abbrev': None
                    }
                },
            ],
            'prep_duration_min': 600,
            'prep_duration_max': None,
            'cook_duration_min': 1200,
            'cook_duration_max': 2000,
            'description': "Recipe description 1",
            'image': None,
            'notes': "Notes 1",
            'tags': [],
            'ingredient_sections': [
                {
                    'name': None,
                    'ingredients': [
                        serializers.RecipeIngredientSerializer(
                            self.recipe_ingredient1, order_parent_field="section").data,
                        serializers.RecipeIngredientSerializer(
                            self.recipe_ingredient2, order_parent_field="section").data,
                    ]
                },
                {
                    'name': "Ingredient Section 2",
                    'ingredients': [
                        serializers.RecipeIngredientSerializer(
                            self.recipe_ingredient3, order_parent_field="section").data
                    ]
                }
            ],
            'instruction_sections': [
                {
                    'name': None,
                    'instructions': ["Recipe 1 Instruction 1", "Recipe 1 Instruction 2"]
                },
                {
                    'name': "Instruction Section 2",
                    'instructions': ["Recipe 1 Instruction 3"]
                }
            ],
            'variations': [
                {
                    'name': "Var 0",
                    'variation': "Recipe 1 Variation 0"
                }
            ]
        })

    def test_recipe_validation(self):
        """Test Recipe validation"""

        # Minimal recipe fields
        serializer = serializers.RecipeSerializer(data={
            'name': "Recipe 2",
            'slug': "recipe-2",
            'category': serializers.CategorySerializer([self.category], many=True).data[0],
            'subcategory': None,
            'source': {
                'name': "Test Source",
                'isbn': "112657",
                'url': "http://www.test.com",
                'notes': "source notes",
                'authors': [
                    serializers.SourceAuthorSerializer([self.author], many=True).data[0]
                ]
            },
            'yields': [],
            'ingredient_sections': [],
            'instruction_sections': [],
            'variations': []
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)

        # Try a fully filled out recipe
        serializer = serializers.RecipeSerializer(data={
            'name': "Recipe 3",
            'slug': "recipe-3",
            'category': serializers.CategorySerializer([self.category], many=True).data[0],
            'subcategory': serializers.SubcategorySerializer([self.subcategory], many=True).data[0],
            'source': {
                'name': "Test Source 2",
                'isbn': "1126222",
                'url': "http://www.testing.com",
                'notes': "source notes",
                'authors': [
                    serializers.SourceAuthorSerializer([self.author], many=True).data[0]
                ]
            },
            'yields': [
                {
                    'min_quantity': '1.0000',
                    'unit': {'id': self.unit_servings.id}
                },
                {
                    'min_quantity': '2.0000',
                    'unit': {'id': self.unit_cup.id}
                },
            ],
            'prep_duration_min': 100,
            'prep_duration_max': 800,
            'cook_duration_min': 1200,
            'cook_duration_max': None,
            'description': "Recipe description 3",
            'notes': "Notes 3",
            'tags': [
                {'id': self.recipe_tag1.id}
            ],
            'ingredient_sections': [
                {
                    'name': None,
                    'ingredients': [
                        serializers.RecipeIngredientSerializer(
                            self.recipe_ingredient1, order_parent_field="section").data,
                    ]
                },
                {
                    'name': "Ingredient Section 2",
                    'ingredients': [
                        serializers.RecipeIngredientSerializer(
                            self.recipe_ingredient2, order_parent_field="section").data
                    ]
                }
            ],
            'instruction_sections': [
                {
                    'name': None,
                    'instructions': ["Recipe 3 Instruction 1", "Recipe 3 Instruction 2"]
                },
                {
                    'name': "Instruction Section 3",
                    'instructions': ["Recipe 3 Instruction 3"]
                }
            ],
            'variations': [
                {
                    'name': "Var 1",
                    'variation': "Recipe 3 Variation 0"
                }
            ]
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)

        # Check a few cases around inconsistent category/subcateogry specifications
        serialized_data = {
            'name': "Recipe 2",
            'slug': "recipe-2",
            'source': {
                'name': "Test Source",
                'isbn': "112657",
                'url': "http://www.test.com",
                'notes': "source notes",
                'authors': [
                    serializers.SourceAuthorSerializer([self.author], many=True).data[0]
                ]
            },
            'yields': [],
            'ingredient_sections': [],
            'instruction_sections': [],
            'variations': []
        }

        # First, just validate that we don't have to specify both category and subcategory
        serializer = serializers.RecipeSerializer(data=serialized_data)
        self.assertFalse(serializer.is_valid(), serializer.errors)
        serialized_data['category'] = {'id': self.category.id}
        serializer = serializers.RecipeSerializer(data=serialized_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        serialized_data.pop('category')
        serialized_data['subcategory'] = {'id': self.subcategory.id}
        serializer = serializers.RecipeSerializer(data=serialized_data)
        self.assertTrue(serializer.is_valid(), serializer.errors)

        # Now try to use a subcategory that belongs to a different category
        serialized_data['category'] = {'id': self.subcategory2.id}
        serializer = serializers.RecipeSerializer(data=serialized_data)
        self.assertFalse(serializer.is_valid(), serializer.errors)

        # How about creating a new category, but using an existing subcategory
        serialized_data['category'] = {'name': 'Category 3', 'slug': 'category-3'}
        serializer = serializers.RecipeSerializer(data=serialized_data)
        self.assertFalse(serializer.is_valid(), serializer.errors)

        # Or creating a new subcategory with a different category to the recipe's category
        serialized_data['category'] = {'id': self.category.id}
        serialized_data['subcategory'] = {'name': "Sub 3",
                                          'slug': "sub-3",
                                          'category': {'id': self.category2.id}
                                          }
        serializer = serializers.RecipeSerializer(data=serialized_data)
        self.assertFalse(serializer.is_valid(), serializer.errors)

        # Recipe with negative id field for created object
        serializer = serializers.RecipeSerializer(data={
            'name': "Recipe 3",
            'slug': "recipe-3",
            'category': serializers.CategorySerializer([self.category], many=True).data[0],
            'subcategory': None,
            'source': None,
            'yields': [],
            'ingredient_sections': [
                {
                    'name': None,
                    'ingredients': [
                        {
                            'quantities': [],
                            'ingredient': {'id': -1, 'name': 'new_ingredient'},
                            'substitutions': []
                        },
                        {
                            'quantities': [],
                            'ingredient': {'id': -2, 'name': 'new_ingredient'},
                            'substitutions': [[
                                {
                                    'quantities': [],
                                    'ingredient': {'id': -1, 'name': 'new_ingredient'},
                                    'substitutions': []
                                }
                            ]]
                        }
                    ]
                }
            ],
            'instruction_sections': [],
            'variations': []
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)

    def test_recipe_create(self):
        """Test creating a new recipe item"""
        serializer = serializers.RecipeSerializer(data={
            'name': "Recipe 3",
            'slug': "recipe-3",
            'category': serializers.CategorySerializer([self.category], many=True).data[0],
            'subcategory': serializers.SubcategorySerializer([self.subcategory], many=True).data[0],
            'source': {
                'name': "Test Source 2",
                'isbn': "1126222",
                'url': "http://www.testing.com",
                'notes': "source notes",
                'authors': [
                    serializers.SourceAuthorSerializer([self.author], many=True).data[0]
                ]
            },
            'yields': [
                {
                    'min_quantity': '1.0000',
                    'unit': {'id': self.unit_servings.id}
                },
                {
                    'min_quantity': '2.0000',
                    'unit': {'id': self.unit_cup.id}
                },
            ],
            'prep_duration_min': 100,
            'prep_duration_max': 800,
            'cook_duration_min': 1200,
            'cook_duration_max': None,
            'description': "Recipe description 3",
            'notes': "Notes 3",
            'tags': [
                {'id': self.recipe_tag1.id}
            ],
            'ingredient_sections': [
                {
                    'name': None,
                    'ingredients': [
                        serializers.RecipeIngredientSerializer(
                            self.recipe_ingredient1, order_parent_field="section").data,
                    ]
                },
                {
                    'name': "Ingredient Section 2",
                    'ingredients': [
                        serializers.RecipeIngredientSerializer(
                            self.recipe_ingredient2, order_parent_field="section").data
                    ]
                }
            ],
            'instruction_sections': [
                {
                    'name': None,
                    'instructions': ["Recipe 3 Instruction 1", "Recipe 3 Instruction 2"]
                },
                {
                    'name': "Instruction Section 3",
                    'instructions': ["Recipe 3 Instruction 3"]
                }
            ],
            'variations': [
                {
                    'name': "Var 1",
                    'variation': "Recipe 3 Variation 0"
                }
            ]
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        recipe2 = serializer.save()
        self.assertEqual(recipe2.name, "Recipe 3")
        self.assertEqual(recipe2.slug, "recipe-3")
        self.assertEqual(recipe2.category, self.category)
        self.assertEqual(recipe2.subcategory, self.subcategory)
        self.assertEqual(recipe2.source.name, "Test Source 2")
        self.assertEqual(recipe2.source.isbn, "1126222")
        self.assertEqual(recipe2.source.url, "http://www.testing.com")
        self.assertEqual(recipe2.source.notes, "source notes")
        self.assertEqual(len(recipe2.source.authors.all()), 1)
        self.assertEqual(recipe2.source.authors.all()[0], self.author)
        self.assertEqual(len(recipe2.yields.all()), 2)
        self.assertEqual(recipe2.yields.all()[0].approx, False)
        self.assertEqual(recipe2.yields.all()[0].min_quantity, Decimal('1.0000'))
        self.assertEqual(recipe2.yields.all()[0].max_quantity, None)
        self.assertEqual(recipe2.yields.all()[0].unit, self.unit_servings)
        self.assertEqual(recipe2.yields.all()[1].approx, False)
        self.assertEqual(recipe2.yields.all()[1].min_quantity, Decimal('2.0000'))
        self.assertEqual(recipe2.yields.all()[1].max_quantity, None)
        self.assertEqual(recipe2.yields.all()[1].unit, self.unit_cup)
        self.assertEqual(recipe2.prep_duration_min, 100)
        self.assertEqual(recipe2.prep_duration_max, 800)
        self.assertEqual(recipe2.cook_duration_min, 1200)
        self.assertEqual(recipe2.cook_duration_max, None)
        self.assertEqual(recipe2.description, "Recipe description 3")
        self.assertEqual(recipe2.notes, "Notes 3")
        self.assertEqual(len(recipe2.tags.all()), 1)
        self.assertEqual(recipe2.tags.all()[0], self.recipe_tag1)
        self.assertEqual(len(recipe2.ingredient_sections.all()), 2)
        temp_section = recipe2.ingredient_sections.all()[0]
        self.assertEqual(temp_section.name, None)
        self.assertEqual(len(temp_section.ingredients.all()), 1)
        self.assertEqual(
            serializers.RecipeIngredientSerializer(temp_section.ingredients.all()[0],
                                                   order_parent_field="section").data,
            serializers.RecipeIngredientSerializer(self.recipe_ingredient1,
                                                   order_parent_field="section").data)
        temp_section = recipe2.ingredient_sections.all()[1]
        self.assertEqual(temp_section.name, "Ingredient Section 2")
        self.assertEqual(len(temp_section.ingredients.all()), 1)
        self.assertEqual(
            serializers.RecipeIngredientSerializer(temp_section.ingredients.all()[0],
                                                   order_parent_field="section").data,
            serializers.RecipeIngredientSerializer(self.recipe_ingredient2,
                                                   order_parent_field="section").data)
        self.assertEqual(len(recipe2.instruction_sections.all()), 2)
        temp_section = recipe2.instruction_sections.all()[0]
        self.assertEqual(temp_section.name, None)
        self.assertEqual(temp_section.instructions.all()[0].instruction, "Recipe 3 Instruction 1")
        self.assertEqual(temp_section.instructions.all()[1].instruction, "Recipe 3 Instruction 2")
        temp_section = recipe2.instruction_sections.all()[1]
        self.assertEqual(temp_section.name, "Instruction Section 3")
        self.assertEqual(temp_section.instructions.all()[0].instruction, "Recipe 3 Instruction 3")
        self.assertEqual(len(recipe2.variations.all()), 1)
        self.assertEqual(recipe2.variations.all()[0].name, "Var 1")
        self.assertEqual(recipe2.variations.all()[0].variation, "Recipe 3 Variation 0")

        # Validate that if we don't specify a category, the category will be pulled from the
        # subcategory item, and we can create categories/subcategories as part of the request.
        serializer = serializers.RecipeSerializer(data={
            'name': "Recipe 2",
            'subcategory': {
                'name': "New subcategory",
                'slug': "new-subcategory",
                'category': {
                    'name': "New category",
                    'slug': "new-category"
                }
            },
            'source': {
                'name': "Test Source",
                'isbn': "112657",
                'url': "http://www.test.com",
                'notes': "source notes",
                'authors': [
                    serializers.SourceAuthorSerializer([self.author], many=True).data[0]
                ]
            },
            'yields': [],
            'ingredient_sections': [],
            'instruction_sections': [],
            'variations': []
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        new_recipe = serializer.save()
        self.assertEqual(new_recipe.slug, "recipe-2")
        self.assertIsNotNone(new_recipe.category)
        self.assertNotEqual(new_recipe.category, self.category)
        self.assertNotEqual(new_recipe.category, self.category2)
        self.assertEqual(new_recipe.category.name, "New category")
        self.assertEqual(new_recipe.category.slug, "new-category")
        self.assertIsNotNone(new_recipe.subcategory)
        self.assertNotEqual(new_recipe.subcategory, self.subcategory)
        self.assertNotEqual(new_recipe.subcategory, self.subcategory2)
        self.assertEqual(new_recipe.subcategory.name, "New subcategory")
        self.assertEqual(new_recipe.subcategory.slug, "new-subcategory")
        self.assertEqual(new_recipe.subcategory.category, new_recipe.category)

        # It should be valid to specify a category/subcategory with negative IDs and have them
        # auto-populated
        serializer = serializers.RecipeSerializer(data={
            'name': "Recipe 6",
            'category': {
                'id': -1,
                'name': "New category 3",
                'slug': "new-category-3"
            },
            'subcategory': {
                'id': -1,
                'name': "New subcategory 3",
                'slug': "new-subcategory-3",
            },
            'yields': [],
            'ingredient_sections': [],
            'instruction_sections': [],
            'variations': []
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        new_recipe = serializer.save()
        self.assertEqual(new_recipe.slug, "recipe-6")
        self.assertIsNotNone(new_recipe.category)
        self.assertNotEqual(new_recipe.category, self.category)
        self.assertNotEqual(new_recipe.category, self.category2)
        self.assertEqual(new_recipe.category.name, "New category 3")
        self.assertEqual(new_recipe.category.slug, "new-category-3")
        self.assertIsNotNone(new_recipe.subcategory)
        self.assertNotEqual(new_recipe.subcategory, self.subcategory)
        self.assertNotEqual(new_recipe.subcategory, self.subcategory2)
        self.assertEqual(new_recipe.subcategory.name, "New subcategory 3")
        self.assertEqual(new_recipe.subcategory.slug, "new-subcategory-3")
        self.assertEqual(new_recipe.subcategory.category, new_recipe.category)

        # Validate that if we specify an empty slug it gets populated correctly
        serializer = serializers.RecipeSerializer(data={
            'name': "Recipe 4",
            'slug': '',
            'category': serializers.CategorySerializer(self.category).data,
            'source': {
                'name': "Test Source 4",
            },
            'yields': [],
            'ingredient_sections': [],
            'instruction_sections': [],
            'variations': []
        })
        self.assertTrue(serializer.is_valid(), serializer.errors)
        new_recipe = serializer.save()
        self.assertEqual(new_recipe.slug, "recipe-4")

        # Validate category and subcategory consistency check when updating recipe
        serializer = serializers.RecipeSerializer(
            self.recipe1,
            data={
                'name': "Updated recipe name",
                'category': {'id': self.category.id},
                'subcategory': {'id': self.subcategory2.id},
                'source': {
                    'authors': [
                        {'name': "New test author", 'slug': "new-test-author"},
                        {'id': self.author.id}
                    ]
                },
                'yields': []
            },
            partial=True)
        self.assertFalse(serializer.is_valid(), serializer.errors)

        # Recipe with negative id field for created object
        serializer = serializers.RecipeSerializer(data={
            'name': "Recipe 5",
            'slug': "recipe-5",
            'category': serializers.CategorySerializer([self.category], many=True).data[0],
            'subcategory': None,
            'source': {
                'id': -1,
                'name': "Test Source",
                'authors': []
            },
            'yields': [],
            'ingredient_sections': [
                {
                    'name': None,
                    'ingredients': [
                        {
                            'quantities': [],
                            'ingredient': {'id': -1, 'name': 'new_ingredient'},
                            'substitutions': []
                        },
                        {
                            'quantities': [],
                            'ingredient': {'id': -2, 'name': 'new_ingredient'},
                            'substitutions': [[
                                {
                                    'quantities': [],
                                    'ingredient': {'id': -1, 'name': 'new_ingredient'},
                                    'substitutions': []
                                }
                            ]]
                        }
                    ]
                }
            ],
            'instruction_sections': [],
            'variations': []
        }, context={"a": 1234})
        self.assertTrue(serializer.is_valid(), serializer.errors)
        # Check that only two ingredients were created
        new_recipe = serializer.save()
        self.assertEqual(len(new_recipe.ingredient_sections.all()[0].ingredients.all()), 2)
        created_ingredients = new_recipe.ingredient_sections.all()[0].ingredients.all()
        self.assertTrue(created_ingredients[0].ingredient.id >= 0)
        self.assertTrue(created_ingredients[1].ingredient.id >= 0)
        self.assertNotEqual(created_ingredients[0].ingredient,
                            created_ingredients[1].ingredient)
        self.assertEqual(len(created_ingredients[1].substitutions.all()), 1)
        self.assertEqual(created_ingredients[0].ingredient,
                         created_ingredients[1].substitutions.all()[0].ingredients.
                         all()[0].ingredient)
        # Check that the created source has a positive ID
        self.assertTrue(new_recipe.source.id >= 0)

    def test_recipe_update(self):
        """Test updating a recipe."""

        recipe_yield_id = self.recipe1.yields.all()[0].id
        serializer = serializers.RecipeSerializer(
            self.recipe1,
            data={
                'name': "Updated recipe name",
                'subcategory': {
                    'name': "Updated subcategory",
                    'slug': "updated-subcategory",
                    'category': {'id': self.category.id}
                },
                'source': {
                    'id': self.source.id,
                    'authors': [
                        {'name': "New test author", 'slug': "new-test-author"},
                        {'id': self.author.id}
                    ]
                },
                'yields': []
            },
            partial=True)
        self.assertTrue(serializer.is_valid(), serializer.errors)
        serializer.save()

        # Recipe name was updated
        self.assertEqual(self.recipe1.name, "Updated recipe name")

        # The subcategory should have changed
        self.assertNotEqual(self.recipe1.subcategory, self.subcategory)
        self.assertEqual(self.recipe1.subcategory.name, "Updated subcategory")
        self.assertEqual(self.recipe1.subcategory.slug, "updated-subcategory")
        self.assertEqual(self.recipe1.subcategory.category, self.category)

        # Source authors should have been updated
        self.assertEqual(len(self.recipe1.source.authors.all()), 2)
        self.assertNotEqual(self.recipe1.source.authors.all()[0], self.author)
        self.assertEqual(self.recipe1.source.authors.all()[0].name, "New test author")
        self.assertEqual(self.recipe1.source.authors.all()[0].slug, "new-test-author")
        self.assertEqual(self.recipe1.source.authors.all()[1], self.author)

        # The yield entries should have been removed from the recipe, and also the yield item
        # removed from the database
        self.assertEqual(len(self.recipe1.yields.all()), 0)
        self.assertEqual(len(models.RecipeYield.objects.filter(id=recipe_yield_id)), 0)
