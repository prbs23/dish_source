"""
Unit Tests for RecipeIngredientSerializer
"""
from decimal import Decimal

from django.test import TestCase
from dish_source import models
from dish_source import serializers


class RecipeIngredientSerializerTests(TestCase):
    """
    RecipeIngredient Serializer class unit tests
    """

    def setUp(self):
        """
        Set up some test ingredients and other model items to define a recipe ingredient
        """
        self.category = models.Category.objects.create(name="Test Category", slug="test-category")
        self.subcategory = models.Subcategory.objects.create(
            name="Test Subcategory",
            slug="test-subcategory",
            category=self.category
        )
        self.author = models.SourceAuthor.objects.create(name="Test Author1", slug="test-author")
        self.source = models.Source.objects.create(name="Test Source")
        self.source.authors.add(self.author)
        self.ingredient_tag1 = models.IngredientTag.objects.create(
            name="Ing Tag 1", slug="ing-tag-1")
        self.recipe_tag1 = models.IngredientTag.objects.create(name="Rec Tag 1", slug="rec-tag-1")
        self.ingredient1 = models.Ingredient.objects.create(name="Ingredient1", slug="ingredient-1")
        self.ingredient1.tags.add(self.ingredient_tag1)
        self.ingredient2 = models.Ingredient.objects.create(name="Ingredient2", slug="ingredient-2")
        self.ingredient3 = models.Ingredient.objects.create(name="Ingredient3", slug="ingredient-3")
        self.ingredient4 = models.Ingredient.objects.create(name="Ingredient4", slug="ingredient-4")
        self.unit_cup = models.Unit.objects.create(name="cup", abbrev=None)
        self.unit_tbsp = models.Unit.objects.create(name="tablespoon", abbrev="tbsp")
        self.unit_servings = models.Unit.objects.create(name="serving")
        self.recipe1 = models.Recipe.objects.create(
            name="Recipe 1",
            slug="recipe-1",
            category=self.category,
            subcategory=self.subcategory,
            source=self.source,
            description="Recipe description 1",
            notes="Notes 1",
            prep_duration_min=600,
            prep_duration_max=None,
            cook_duration_min=1200,
            cook_duration_max=2000
        )
        models.RecipeYield.objects.create(
            recipe=self.recipe1,
            order=0,
            approx=True,
            min_quantity=5,
            max_quantity=None,
            unit=self.unit_servings
        )
        models.RecipeIngredientSection.objects.create(recipe=self.recipe1, order=0, name=None)
        self.recipe_ingredient1 = models.RecipeIngredient.objects.create(
            section=self.recipe1.ingredient_sections.all()[0],
            order=0,
            qualifier="qual",
            ingredient=self.ingredient1,
            processing="proc",
        )
        models.RecipeIngredientQuantity.objects.create(
            recipe_ingredient=self.recipe_ingredient1,
            order=0,
            approx=False,
            min_quantity=1,
            max_quantity=None,
            unit=self.unit_cup
        )
        self.recipe_ingredient2 = models.RecipeIngredient.objects.create(
            section=self.recipe1.ingredient_sections.all()[0],
            order=1,
            ingredient=self.ingredient2,
        )
        models.RecipeIngredientQuantity.objects.create(
            recipe_ingredient=self.recipe_ingredient2,
            order=0,
            approx=False,
            min_quantity=1,
            max_quantity=1.5,
            unit=self.unit_tbsp
        )
        models.RecipeIngredientQuantity.objects.create(
            recipe_ingredient=self.recipe_ingredient2,
            order=1,
            approx=True,
            min_quantity=10,
            max_quantity=None,
            unit=self.unit_cup
        )
        models.RecipeIngredientSubstitution.objects.create(
            substituted_ingredient=self.recipe_ingredient2,
            order=0,
        )
        models.RecipeIngredient.objects.create(
            substitution=self.recipe_ingredient2.substitutions.all()[0],
            order=0,
            qualifier="qualifier 2",
            ingredient=self.ingredient3,
        )
        models.RecipeIngredientQuantity.objects.create(
            recipe_ingredient=self.recipe_ingredient2.substitutions.all()[0].ingredients.all()[0],
            order=0,
            approx=True,
            min_quantity=4,
            unit=self.unit_tbsp
        )
        models.RecipeInstructionSection.objects.create(recipe=self.recipe1, order=0, name="None")
        models.RecipeInstruction.objects.create(
            section=self.recipe1.instruction_sections.all()[0],
            order=0,
            instruction="Recipe 1 Instruction 1"
        )
        models.RecipeInstruction.objects.create(
            section=self.recipe1.instruction_sections.all()[0],
            order=1,
            instruction="Recipe 1 Instruction 2"
        )
        models.RecipeVariation.objects.create(
            recipe=self.recipe1,
            order=0,
            name="Var 0",
            variation="Recipe 1 Variation 0"
        )
        self.recipe_ingredient3 = None
        self.recipe_ingredient4 = None

    def test_recipe_ingredient_serialize(self):
        """Test RecipeIngredient serialization"""

        # serialize a simple ingredient without substitutions
        self.assertEqual(serializers.RecipeIngredientSerializer(
            self.recipe_ingredient1,
            order_parent_field="section").data, {
                'quantities': [
                    {
                        'approx': False,
                        'min_quantity': '1.0000',
                        'max_quantity': None,
                        'unit': {
                            'id': self.unit_cup.id,
                            'name': 'cup',
                            'plural': 'cups',
                            'abbrev': None
                        }
                    }
                ],
                'qualifier': 'qual',
                'ingredient': serializers.IngredientSerializer([self.ingredient1],
                                                               many=True).data[0],
                'processing': 'proc',
                'substitutions': []
            })

        # serialize a more complicated ingredient with substitutions and multiple quantities
        self.assertEqual(serializers.RecipeIngredientSerializer(
            self.recipe_ingredient2,
            order_parent_field="section"
        ).data, {
            'quantities': [
                {
                    'approx': False,
                    'min_quantity': '1.0000',
                    'max_quantity': '1.5000',
                    'unit': {
                        'id': self.unit_tbsp.id,
                        'name': 'tablespoon',
                        'plural': 'tablespoons',
                        'abbrev': 'tbsp'
                    }
                },
                {
                    'approx': True,
                    'min_quantity': '10.0000',
                    'max_quantity': None,
                    'unit': {
                        'id': self.unit_cup.id,
                        'name': 'cup',
                        'plural': 'cups',
                        'abbrev': None
                    }
                }
            ],
            'qualifier': '',
            'ingredient': dict(serializers.IngredientSerializer(
                [self.ingredient2], many=True).data[0]),
            'processing': '',
            'substitutions': [
                [
                    {
                        'quantities': [
                            {
                                'approx': True,
                                'min_quantity': '4.0000',
                                'max_quantity': None,
                                'unit': {
                                    'id': self.unit_tbsp.id,
                                    'name': 'tablespoon',
                                    'plural': 'tablespoons',
                                    'abbrev': 'tbsp'
                                }
                            }
                        ],
                        'qualifier': 'qualifier 2',
                        'ingredient': dict(serializers.IngredientSerializer(
                            [self.ingredient3], many=True).data[0]),
                        'processing': '',
                        'substitutions': []
                    }
                ]
            ]
        })

    def test_recipe_ingredient_validation(self):
        """Test RecipeIngredient validation"""
        # Valid Recipe Ingredient with all fields
        serializer = serializers.RecipeIngredientSerializer(data={
            'quantities': [
                {
                    'approx': False,
                    'min_quantity': '1.0000',
                    'max_quantity': '1.5000',
                    'unit': {
                        'id': self.unit_tbsp.id,
                        'name': 'tablespoon',
                        'abbrev': 'tbsp'
                    }
                },
                {
                    'approx': True,
                    'min_quantity': '10.0000',
                    'unit': {
                        'id': self.unit_cup.id,
                        'name': 'cup',
                        'abbrev': None
                    }
                }
            ],
            'qualifier': '',
            'ingredient': dict(serializers.IngredientSerializer(
                [self.ingredient2], many=True).data[0]),
            'processing': '',
            'substitutions': [
                [
                    {
                        'quantities': [
                            {
                                'approx': True,
                                'min_quantity': '4.0000',
                                'max_quantity': None,
                                'unit': {
                                    'id': self.unit_tbsp.id,
                                    'name': 'tablespoon',
                                    'abbrev': 'tbsp'
                                }
                            }
                        ],
                        'qualifier': 'qualifier 2',
                        'ingredient': dict(serializers.IngredientSerializer(
                            [self.ingredient3], many=True).data[0]),
                        'processing': '',
                        'substitutions': []
                    }
                ]
            ]
        }, order_parent_field="section")
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # It's valid to not have any substitutions
        serializer = serializers.RecipeIngredientSerializer(data={
            'quantities': [
                {
                    'approx': False,
                    'min_quantity': '1.0000',
                    'max_quantity': '1.5000',
                    'unit': {
                        'id': self.unit_tbsp.id,
                        'name': 'tablespoon',
                        'abbrev': 'tbsp'
                    }
                },
                {
                    'approx': True,
                    'min_quantity': '10.0000',
                    'max_quantity': None,
                    'unit': {
                        'id': self.unit_cup.id,
                        'name': 'cup',
                        'abbrev': None
                    }
                }
            ],
            'qualifier': '',
            'ingredient': dict(serializers.IngredientSerializer(
                [self.ingredient2], many=True).data[0]),
            'processing': '',
            'substitutions': []
        }, order_parent_field="section")
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # It's even valid to not have any quantities
        serializer = serializers.RecipeIngredientSerializer(data={
            'quantities': [],
            'qualifier': '',
            'ingredient': dict(serializers.IngredientSerializer(
                [self.ingredient2], many=True).data[0]),
            'processing': '',
            'substitutions': []
        }, order_parent_field="section")
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # It's also valid for a quantity to not have a unit
        serializer = serializers.RecipeIngredientSerializer(data={
            'quantities': [
                {
                    'approx': True,
                    'min_quantity': '10.0000',
                    'max_quantity': None,
                    'unit': None
                }
            ],
            'qualifier': '',
            'ingredient': dict(serializers.IngredientSerializer(
                [self.ingredient2], many=True).data[0]),
            'processing': '',
            'substitutions': []
        }, order_parent_field="section")
        self.assertEqual(serializer.is_valid(), True, serializer.errors)

        # However, it's not valid to include a substitution that does not have any ingredients
        serializer = serializers.RecipeIngredientSerializer(data={
            'quantities': [],
            'qualifier': '',
            'ingredient': dict(serializers.IngredientSerializer(
                [self.ingredient2], many=True).data[0]),
            'processing': '',
            'substitutions': [[]]
        }, order_parent_field="section")
        self.assertEqual(serializer.is_valid(), False, serializer.errors)

    def test_recipe_ingredient_create(self):
        """Test creating a new recipe ingredient item"""

        # Test creating a simple recipe ingredient
        serializer = serializers.RecipeIngredientSerializer(data={
            'quantities': [
                {
                    'approx': False,
                    'min_quantity': '1.0000',
                    'max_quantity': None,
                    'unit': {
                        'id': self.unit_cup.id,
                        'name': 'cup',
                        'abbrev': None
                    }
                }
            ],
            'qualifier': 'test qual',
            'ingredient': serializers.IngredientSerializer([self.ingredient1], many=True).data[0],
            'processing': 'test processing instructions',
            'substitutions': []
        }, order_parent_field="section")
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        self.recipe_ingredient3 = serializer.save(
            order_position=5, parent_instance=self.recipe1.ingredient_sections.all()[0])


        # Check that the recipe item has been added to the ingredient section
        self.assertEqual(self.recipe_ingredient3,
                         self.recipe1.ingredient_sections.all()[0].ingredients.all()[2])

        # Validate the content of the recipe ingredient
        self.assertEqual(self.recipe_ingredient3.qualifier, 'test qual')
        self.assertEqual(self.recipe_ingredient3.processing, 'test processing instructions')
        self.assertEqual(self.recipe_ingredient3.ingredient, self.ingredient1)
        self.assertEqual(len(self.recipe_ingredient3.quantities.all()), 1)
        self.assertEqual(self.recipe_ingredient3.quantities.all()[0].approx, False)
        self.assertEqual(self.recipe_ingredient3.quantities.all()[0].min_quantity, 1.0)
        self.assertEqual(self.recipe_ingredient3.quantities.all()[0].max_quantity, None)
        self.assertEqual(self.recipe_ingredient3.quantities.all()[0].unit, self.unit_cup)
        self.assertEqual(len(self.recipe_ingredient3.substitutions.all()), 0)

        # Test creating a simple recipe ingredient with no quantity unit
        serializer = serializers.RecipeIngredientSerializer(data={
            'quantities': [
                {
                    'approx': False,
                    'min_quantity': '1.0000',
                    'max_quantity': None,
                    'unit': None
                }
            ],
            'qualifier': 'test qual',
            'ingredient': serializers.IngredientSerializer([self.ingredient1], many=True).data[0],
            'processing': 'test processing instructions',
            'substitutions': []
        }, order_parent_field="section")
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        ingredient = serializer.save(
            order_position=5, parent_instance=self.recipe1.ingredient_sections.all()[0])

        # Validate the content of the recipe ingredient
        self.assertEqual(ingredient.qualifier, 'test qual')
        self.assertEqual(ingredient.processing, 'test processing instructions')
        self.assertEqual(ingredient.ingredient, self.ingredient1)
        self.assertEqual(len(ingredient.quantities.all()), 1)
        self.assertEqual(ingredient.quantities.all()[0].approx, False)
        self.assertEqual(ingredient.quantities.all()[0].min_quantity, 1.0)
        self.assertEqual(ingredient.quantities.all()[0].max_quantity, None)
        self.assertEqual(ingredient.quantities.all()[0].unit, None)
        self.assertEqual(len(ingredient.substitutions.all()), 0)

        # Now try a more complicated recipe ingredient with substitutions
        serializer = serializers.RecipeIngredientSerializer(data={
            'quantities': [
                {
                    'approx': True,
                    'min_quantity': '4.0000',
                    'max_quantity': '4.5000',
                    'unit': {'id': self.unit_tbsp.id}
                },
                {
                    'approx': False,
                    'min_quantity': '0.2500',
                    'max_quantity': '0.2812',
                    'unit': {'id': self.unit_cup.id}
                }
            ],
            'qualifier': '',
            'ingredient': serializers.IngredientSerializer([self.ingredient2], many=True).data[0],
            'processing': 'test proc 2',
            'substitutions': [
                [
                    {
                        'quantities': [
                            {
                                'approx': True,
                                'min_quantity': '4.0000',
                                'max_quantity': '4.5000',
                                'unit': {'id': self.unit_tbsp.id}
                            },
                        ],
                        'qualifier': '',
                        'ingredient': serializers.IngredientSerializer([self.ingredient3],
                                                                       many=True).data[0],
                        'processing': 'test proc 3',
                        'substitutions': []
                    },
                    {
                        'quantities': [
                            {
                                'approx': False,
                                'min_quantity': '0.2500',
                                'max_quantity': '0.2812',
                                'unit': {'id': self.unit_cup.id}
                            }
                        ],
                        'qualifier': 'test qual 3',
                        'ingredient': serializers.IngredientSerializer([self.ingredient4],
                                                                       many=True).data[0],
                        'processing': '',
                        'substitutions': []
                    }
                ],
                [
                    {
                        'quantities': [
                            {
                                'approx': True,
                                'min_quantity': '4.0000',
                                'max_quantity': '4.5000',
                                'unit': {'id': self.unit_tbsp.id}
                            },
                        ],
                        'qualifier': 'another qualifier',
                        'ingredient': serializers.IngredientSerializer([self.ingredient1],
                                                                       many=True).data[0],
                        'processing': 'test proc 4',
                        'substitutions': []
                    },
                ]
            ]
        }, order_parent_field="section")
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        self.recipe_ingredient4 = serializer.save(
            order_position=4, parent_instance=self.recipe1.ingredient_sections.all()[0])

        # Check that the new ingredient was inserted in the correct place
        self.assertEqual(self.recipe_ingredient4,
                         self.recipe1.ingredient_sections.all()[0].ingredients.all()[2])

        # Also, the previously created ingredient should have changed positions
        self.assertEqual(self.recipe_ingredient3,
                         self.recipe1.ingredient_sections.all()[0].ingredients.all()[3])

        self.assertEqual(self.recipe_ingredient4.qualifier, '')
        self.assertEqual(self.recipe_ingredient4.processing, 'test proc 2')
        self.assertEqual(self.recipe_ingredient4.ingredient, self.ingredient2)
        self.assertEqual(len(self.recipe_ingredient4.quantities.all()), 2)
        self.assertEqual(self.recipe_ingredient4.quantities.all()[0].approx, True)
        self.assertEqual(self.recipe_ingredient4.quantities.all()[0].min_quantity, 4.0)
        self.assertEqual(self.recipe_ingredient4.quantities.all()[0].max_quantity, 4.5)
        self.assertEqual(self.recipe_ingredient4.quantities.all()[0].unit, self.unit_tbsp)
        self.assertEqual(self.recipe_ingredient4.quantities.all()[1].approx, False)
        self.assertEqual(self.recipe_ingredient4.quantities.all()[1].min_quantity, 0.25)
        self.assertEqual(self.recipe_ingredient4.quantities.all()[1].max_quantity,
                         Decimal('0.2812'))
        self.assertEqual(self.recipe_ingredient4.quantities.all()[1].unit, self.unit_cup)
        self.assertEqual(len(self.recipe_ingredient4.substitutions.all()), 2)
        temp_substitution = self.recipe_ingredient4.substitutions.all()[0].ingredients.all()
        self.assertEqual(len(temp_substitution), 2)
        self.assertEqual(temp_substitution[0].qualifier, '')
        self.assertEqual(temp_substitution[0].processing, 'test proc 3')
        self.assertEqual(temp_substitution[0].ingredient, self.ingredient3)
        self.assertEqual(len(temp_substitution[0].quantities.all()), 1)
        self.assertEqual(temp_substitution[0].quantities.all()[0].approx, True)
        self.assertEqual(temp_substitution[0].quantities.all()[0].min_quantity, 4.0)
        self.assertEqual(temp_substitution[0].quantities.all()[0].max_quantity, 4.5)
        self.assertEqual(temp_substitution[0].quantities.all()[0].unit, self.unit_tbsp)
        self.assertEqual(temp_substitution[1].qualifier, "test qual 3")
        self.assertEqual(temp_substitution[1].processing, '')
        self.assertEqual(temp_substitution[1].ingredient, self.ingredient4)
        self.assertEqual(len(temp_substitution[1].quantities.all()), 1)
        self.assertEqual(temp_substitution[1].quantities.all()[0].approx, False)
        self.assertEqual(temp_substitution[1].quantities.all()[0].min_quantity, 0.25)
        self.assertEqual(temp_substitution[1].quantities.all()[0].max_quantity, Decimal('0.2812'))
        self.assertEqual(temp_substitution[1].quantities.all()[0].unit, self.unit_cup)
        temp_substitution = self.recipe_ingredient4.substitutions.all()[1].ingredients.all()
        self.assertEqual(len(temp_substitution), 1)
        self.assertEqual(temp_substitution[0].qualifier, 'another qualifier')
        self.assertEqual(temp_substitution[0].processing, 'test proc 4')
        self.assertEqual(temp_substitution[0].ingredient, self.ingredient1)
        self.assertEqual(len(temp_substitution[0].quantities.all()), 1)
        self.assertEqual(temp_substitution[0].quantities.all()[0].approx, True)
        self.assertEqual(temp_substitution[0].quantities.all()[0].min_quantity, 4.0)
        self.assertEqual(temp_substitution[0].quantities.all()[0].max_quantity, 4.5)
        self.assertEqual(temp_substitution[0].quantities.all()[0].unit, self.unit_tbsp)

    def test_recipe_ingredient_update(self):
        """Test updating a recipe ingredient."""
        # Update quantities list and qualifier string
        serializer = serializers.RecipeIngredientSerializer(
            self.recipe_ingredient2,
            data={
                'quantities': [
                    {
                        'approx': True,
                        'min_quantity': '2.0000',
                        'max_quantity': '3.0000',
                        'unit': {
                            'id': self.unit_cup.id,
                            'name': 'cup',
                            'abbrev': None
                        }
                    }
                ],
                'qualifier': 'new qualifier',
            },
            order_parent_field="section",
            partial=True
        )
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        updated_ingredient = serializer.save(
            order_position=5, parent_instance=self.recipe1.ingredient_sections.all()[0])
        self.assertEqual(updated_ingredient.qualifier, 'new qualifier')
        self.assertEqual(updated_ingredient.processing, '')
        self.assertEqual(updated_ingredient.ingredient, self.ingredient2)
        self.assertEqual(len(updated_ingredient.quantities.all()), 1)
        self.assertEqual(updated_ingredient.quantities.all()[0].approx, True)
        self.assertEqual(updated_ingredient.quantities.all()[0].min_quantity,
                         Decimal('2.0000'))
        self.assertEqual(updated_ingredient.quantities.all()[0].max_quantity,
                         Decimal('3.0000'))
        self.assertEqual(updated_ingredient.quantities.all()[0].unit, self.unit_cup)
        self.assertEqual(len(updated_ingredient.substitutions.all()), 1)
        substitution_id = updated_ingredient.substitutions.all()[0].id
        sub_ingredient_id = updated_ingredient.substitutions.all()[0].ingredients.all()[0].id
        self.assertEqual(
            len(models.RecipeIngredientSubstitution.objects.filter(id=substitution_id)), 1)
        self.assertEqual(
            len(models.RecipeIngredient.objects.filter(id=sub_ingredient_id)), 1)

        # Update the substitution list
        serializer = serializers.RecipeIngredientSerializer(
            self.recipe_ingredient2,
            data={
                'substitutions': [
                    [
                        {
                            'quantities': [
                                {
                                    'approx': True,
                                    'min_quantity': '2.0000',
                                    'max_quantity': '2.5000',
                                    'unit': {'id': self.unit_tbsp.id}
                                },
                            ],
                            'qualifier': 'another qualifier',
                            'ingredient': serializers.IngredientSerializer([self.ingredient1],
                                                                           many=True).data[0],
                            'processing': 'test proc 5',
                            'substitutions': []
                        },
                    ]
                ]
            },
            order_parent_field="section",
            partial=True
        )
        self.assertEqual(serializer.is_valid(), True, serializer.errors)
        updated_ingredient = serializer.save(
            order_position=5, parent_instance=self.recipe1.ingredient_sections.all()[0])
        self.assertEqual(updated_ingredient.qualifier, 'new qualifier')
        self.assertEqual(updated_ingredient.processing, '')
        self.assertEqual(updated_ingredient.ingredient, self.ingredient2)
        self.assertEqual(len(updated_ingredient.quantities.all()), 1)
        self.assertEqual(updated_ingredient.quantities.all()[0].approx, True)
        self.assertEqual(updated_ingredient.quantities.all()[0].min_quantity,
                         Decimal('2.0000'))
        self.assertEqual(updated_ingredient.quantities.all()[0].max_quantity,
                         Decimal('3.0000'))
        self.assertEqual(updated_ingredient.quantities.all()[0].unit, self.unit_cup)
        self.assertEqual(len(updated_ingredient.substitutions.all()), 1)

        # the old substitution and substituted recipe ingredient entries have been removed
        self.assertEqual(
            len(models.RecipeIngredientSubstitution.objects.filter(id=substitution_id)), 0)
        self.assertEqual(
            len(models.RecipeIngredient.objects.filter(id=sub_ingredient_id)), 0)

        sub_ingredient = updated_ingredient.substitutions.all()[0].ingredients.all()[0]
        self.assertEqual(sub_ingredient.qualifier, 'another qualifier')
        self.assertEqual(sub_ingredient.processing, 'test proc 5')
        self.assertEqual(sub_ingredient.ingredient, self.ingredient1)
        self.assertEqual(len(sub_ingredient.quantities.all()), 1)
        self.assertEqual(sub_ingredient.quantities.all()[0].approx, True)
        self.assertEqual(sub_ingredient.quantities.all()[0].min_quantity, 2.0)
        self.assertEqual(sub_ingredient.quantities.all()[0].max_quantity, 2.5)
        self.assertEqual(sub_ingredient.quantities.all()[0].unit, self.unit_tbsp)
