"""
API routers for Dish Source app
"""

from rest_framework import routers
from dish_source import viewsets

router = routers.DefaultRouter()

# API View Sets
router.register(r'categories', viewsets.CategoryViewSet)
router.register(r'subcategories', viewsets.SubcategoryViewSet)
router.register(r'recipes', viewsets.RecipeViewSet)
router.register(r'ingredients', viewsets.IngredientViewSet)
router.register(r'tags/recipes', viewsets.RecipeTagViewSet)
router.register(r'tags/ingredients', viewsets.IngredientTagViewSet)
router.register(r'units', viewsets.UnitViewSet)
router.register(r'authors', viewsets.SourceAuthorViewSet)
router.register(r'unimported_recipes', viewsets.UnimportedRecipeViewSet)
