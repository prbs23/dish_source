"""
Dish source site form declarations
"""

from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UsernameField

UserModel = get_user_model()


class AccountSettingsForm(forms.ModelForm):
    """
    Form to user account settings.

    To start with this class just configures the default contrib.auth app user settings
    """

    class Meta:
        model = UserModel
        fields = ("username", "email", "first_name", "last_name")
        field_classes = {
            'username': UsernameField,
            'email': forms.EmailField,
            'first_name': forms.CharField,
            'last_name': forms.CharField,
        }
