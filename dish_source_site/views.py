"""
Dish Source Site Views
"""

from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.views.decorators.csrf import csrf_protect
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic.base import TemplateResponseMixin, View
from django.views.generic.edit import UpdateView
from django.urls.base import reverse
from django.utils.decorators import method_decorator
from dish_source_site.forms import AccountSettingsForm


class AccountSettingsView(UpdateView):
    """View to edit user account settings. Renders AccountSettingsForm"""

    template_name = "registration/account_settings.html"
    form_class = AccountSettingsForm
    title = "Account Settings"

    @method_decorator(sensitive_post_parameters())
    @method_decorator(csrf_protect)
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        """
        For all requests, set self.object pointer to the user instance
        from the request context.
        """
        # pylint: disable=attribute-defined-outside-init
        self.object = request.user
        return super().dispatch(request, *args, **kwargs)

    def get_object(self, queryset=None):
        """
        Override the default get_object implementation to just return self.object
        since we are already setting it in the dispatch method.
        """
        return self.object

    def get_success_url(self):
        return reverse("account_settings") + "?success"

    def get_context_data(self, **kwargs):
        """Add title field to the context dictionary"""
        context = super().get_context_data(**kwargs)
        context.update({
            'title': self.title
        })
        return context


class AccountDeleteConfirmationView(TemplateResponseMixin, View):
    """View for account deletion confirmation page"""

    template_name = "registration/account_delete_confirm.html"
    title = "Delete Account"
    success_url = "/"

    @method_decorator(csrf_protect)
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        """Decorate dispatch method with CSRF protection and login requirement"""
        return super().dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        """Handle get request. Just render the confirmation template page."""
        # pylint: disable=unused-argument
        return self.render_to_response(self.get_context_data())

    def post(self, request, *args, **kwargs):
        """Handle post request. Log out the current user, and then delete the account."""
        # pylint: disable=unused-argument
        user = request.user
        logout(request)
        user.delete()
        return HttpResponseRedirect(self.success_url)

    def get_context_data(self):
        """Add title field to the context dictionary"""
        return {
            'title': self.title
        }
